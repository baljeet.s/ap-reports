import { plainToClass } from "class-transformer";
import moment from "moment";
import { BSEOrder } from "../Classes/BSEOrder";
import { FolioModel } from "../Classes/Folio";
import { Holding } from "../Classes/Holding";
import { LatestNAVClass } from "../Classes/LatestNAV";
import { MFScheme } from "../Classes/MFScheme";
import { PortfolioClass } from "../Classes/Portfolio";
// import { getLockOutPeriod } from "../helpers/GetLockoutPeriod";
import AssetClass from '../Classes/AssetClass';
import OtherAsset from "../Classes/OtherAssets";

export default class Portfolio {
    static generate(orders: Array<BSEOrder>, schemesList: Array<MFScheme>, navs: { [code: string]: LatestNAVClass }, otherAssetsOrders: AssetClass[] = [], includeZeroFolios?: boolean, isAssetPlus?: boolean,) {
        console.log("schemeList: ", schemesList)
        //Bucket orders
        var portfolio: any = {
            holdings: {},
            otherAssets: otherAssetsOrders.map((item: AssetClass) => new OtherAsset(item)),
        };
        orders
            .filter(order => (order.orderStatus === 'VALID' && order.isProcessed))
            .forEach(order => {
                if (!portfolio.holdings[order.schemeCode]) {

                    var schemeInfo = MFScheme.findSchemeByCode(schemesList, order.schemeCode);
                    if (!schemeInfo) {
                        return console.log("Scheme Info Null", order.schemeCode)
                    }
                    console.log("got schemeInfo: ", schemeInfo.gfNAV, schemeInfo);

                    var nav = navs[order.schemeCode];
                    portfolio.holdings[order.schemeCode] = {
                        schemeCode: order.schemeCode,
                        schemeName: schemeInfo.schemeName,
                        nav: nav.nav,
                        navDate: ddmmmyyyytoyymmdd(nav.date),
                        gfNAV: parseFloat(schemeInfo.gfNAV),
                        navChange: nav.change,
                        lockInPeriod: schemeInfo.getLockOutPeriod,
                        amfiBroad: schemeInfo.amfiBroad,
                        amfiSub: schemeInfo.amfiSub,
                        legend: schemeInfo.legend || "Others",
                        amcCode: schemeInfo.amcCode,
                        folios: {},
                        isAssetPlus
                    };
                }
                if (!portfolio.holdings[order.schemeCode].folios[order.folioNo]) {
                    var holding = portfolio.holdings[order.schemeCode];
                    console.log("Finding holding: ", holding)
                    holding.folios[order.folioNo] = {
                        folioNo: order.folioNo,
                        schemeCode: order.schemeCode,
                        nav: holding.nav,
                        navDate: holding.navDate,
                        gfNAV: holding.gfNAV,
                        navChange: holding.navChange,
                        lockInPeriod: holding.lockInPeriod,
                        amfiBroad: holding.amfiBroad,
                        amfiSub: holding.amfiSub,
                        legend: holding.legend,
                        orders: [],
                    };
                }
                portfolio
                    .holdings[order.schemeCode]
                    .folios[order.folioNo]
                    .orders
                    .push({
                        transactionType: order.transactionType,
                        navDate: ddmmyyyytoyyyymmdd(order.bseOrderDate),
                        nav: parseFloat(order.processingNAV),
                        units: order.processingUnits,
                        amount: (order.processingAmount) || (parseFloat(order.processingNAV) * parseFloat(order.processingUnits)).toFixed(4)
                        // amount: (parseFloat(order.processingNAV) * parseFloat(order.processingUnits)).toFixed(4)
                    });
            });
        //Convert map to arrays
        portfolio.holdings = Object.keys(portfolio.holdings)
            .map(schemeCode => {
                var holding = portfolio.holdings[schemeCode];
                holding.folios = Object.keys(holding.folios)
                    .map(folioNo => {
                        var folio = portfolio.holdings[schemeCode].folios[folioNo];
                        folio.orders.sort((a: any, b: any) => {
                            if (a.navDate < b.navDate) {
                                return -1;
                            }
                            else if (a.navDate > b.navDate) {
                                return 1;
                            }
                            else {
                                return 0;
                            }
                        });
                        // return new Folio(folio);
                        var folioObject = plainToClass(FolioModel, folio);
                        folioObject.processOrders();
                        return folioObject;
                    })
                if (!includeZeroFolios) {
                    holding.folios = holding.folios.filter((folio: any) => folio.assetValue);
                }
                // .filter(folio => folio.assetValue);
                // return new Holding(holding);
                var holdingObject = plainToClass(Holding, holding);
                holdingObject.processFolios();
                return holdingObject;
            })
            // .filter(holding => holding.assetValue)
            .sort((a, b) => {
                if (a.schemeName < b.schemeName) {
                    return -1;
                }
                else if (a.schemeName > b.schemeName) {
                    return 1;
                }
                else {
                    return 0;
                }
            });

        if (!includeZeroFolios) {
            portfolio.holdings = portfolio.holdings.filter((holding: any) => holding.assetValue);
        }
        // portfolio = new Portfolio(portfolio);
        var portfolioObject = plainToClass(PortfolioClass, portfolio);
        portfolioObject.processHoldings();

        var unprocessedOrders = orders.filter(order => order.orderStatus !== 'INVALID' && !order.isProcessed);
        portfolioObject.unprocessedPurchases = unprocessedOrders
            .filter(order => order.transactionType === "P")
            .map(order => parseFloat(order.amount))
            .reduce((sum, current) => sum + current, 0);
        portfolioObject.unprocessedRedemptions = unprocessedOrders
            .filter(order => order.transactionType === "R")
            .map(order => parseFloat(order.amount))
            .reduce((sum, current) => sum + current, 0);
        portfolioObject.unprocessedSwitches = unprocessedOrders
            .filter(order => order.transactionType === "SI")
            .map(order => parseFloat(order.amount))
            .reduce((sum, current) => sum + current, 0);

        return portfolioObject;
    }

    // static generate(orders: Array<BSEOrder>, schemesList: Array<any>, navs: { [code: string]: LatestNAVClass }, isAssetPlus: boolean, otherAssetsOrders: any = [], includeZeroFolios?: boolean) {
    //     console.log("schemeList: ", schemesList)
    //     //Bucket orders
    //     var portfolio: any = {
    //         holdings: {}
    //     };
    //     orders
    //         .filter(order => (order.orderStatus === 'VALID' && order.isProcessed))
    //         .forEach(order => {
    //             if (!portfolio.holdings[order.schemeCode]) {

    //                 var schemeInfo = MFScheme.findSchemeByCode(schemesList, order.schemeCode);
    //                 if (!schemeInfo) {
    //                     return console.log("Scheme Info Null", order.schemeCode)
    //                 }
    //                 console.log("got schemeInfo: ", schemeInfo.gfNAV, schemeInfo);

    //                 var nav = navs[order.schemeCode];
    //                 portfolio.holdings[order.schemeCode] = {
    //                     schemeCode: order.schemeCode,
    //                     schemeName: schemeInfo.schemeName,
    //                     nav: nav.nav,
    //                     navDate: ddmmmyyyytoyymmdd(nav.date),
    //                     gfNAV: parseFloat(schemeInfo.gfNAV),
    //                     navChange: nav.change,
    //                     lockInPeriod: schemeInfo.getLockOutPeriod,
    //                     amfiBroad: schemeInfo.amfiBroad,
    //                     amfiSub: schemeInfo.amfiSub,
    //                     legend: schemeInfo.legend,
    //                     amcCode: schemeInfo.amcCode,
    //                     folios: {},
    //                     isAssetPlus
    //                 };
    //             }
    //             if (!portfolio.holdings[order.schemeCode].folios[order.folioNo]) {
    //                 var holding = portfolio.holdings[order.schemeCode];
    //                 console.log("Finding holding: ", holding)
    //                 holding.folios[order.folioNo] = {
    //                     folioNo: order.folioNo,
    //                     schemeCode: order.schemeCode,
    //                     nav: holding.nav,
    //                     navDate: holding.navDate,
    //                     gfNAV: holding.gfNAV,
    //                     navChange: holding.navChange,
    //                     lockInPeriod: holding.lockInPeriod,
    //                     amfiBroad: holding.amfiBroad,
    //                     amfiSub: holding.amfiSub,
    //                     legend: holding.legend,
    //                     orders: [],
    //                 };
    //             }
    //             portfolio
    //                 .holdings[order.schemeCode]
    //                 .folios[order.folioNo]
    //                 .orders
    //                 .push({
    //                     transactionType: order.transactionType,
    //                     navDate: ddmmyyyytoyyyymmdd(order.bseOrderDate),
    //                     nav: parseFloat(order.processingNAV),
    //                     units: order.processingUnits,
    //                     amount: (order.processingAmount) || (parseFloat(order.processingNAV) * parseFloat(order.processingUnits)).toFixed(4)
    //                     // amount: (parseFloat(order.processingNAV) * parseFloat(order.processingUnits)).toFixed(4)
    //                 });
    //         });
    //     //Convert map to arrays
    //     portfolio.holdings = Object.keys(portfolio.holdings)
    //         .map(schemeCode => {
    //             var holding = portfolio.holdings[schemeCode];
    //             holding.folios = Object.keys(holding.folios)
    //                 .map(folioNo => {
    //                     var folio = portfolio.holdings[schemeCode].folios[folioNo];
    //                     folio.orders.sort((a: any, b: any) => {
    //                         if (a.navDate < b.navDate) {
    //                             return -1;
    //                         }
    //                         else if (a.navDate > b.navDate) {
    //                             return 1;
    //                         }
    //                         else {
    //                             return 0;
    //                         }
    //                     });
    //                     // return new Folio(folio);
    //                     var folioObject = plainToClass(FolioModel, folio);
    //                     folioObject.processOrders();
    //                     return folioObject;
    //                 })
    //             // .filter(folio => folio.assetValue);
    //             // return new Holding(holding);
    //             var holdingObject = plainToClass(Holding, holding);
    //             holdingObject.processFolios();
    //             return holdingObject;
    //         })
    //         // .filter(holding => holding.assetValue)
    //         .sort((a, b) => {
    //             if (a.schemeName < b.schemeName) {
    //                 return -1;
    //             }
    //             else if (a.schemeName > b.schemeName) {
    //                 return 1;
    //             }
    //             else {
    //                 return 0;
    //             }
    //         });
    //     // portfolio = new Portfolio(portfolio);
    //     var portfolioObject = plainToClass(PortfolioClass, portfolio);
    //     portfolioObject.processHoldings();

    //     var unprocessedOrders = orders.filter(order => order.orderStatus !== 'INVALID' && !order.isProcessed);
    //     portfolioObject.unprocessedPurchases = unprocessedOrders
    //         .filter(order => order.transactionType === "P")
    //         .map(order => parseFloat(order.amount))
    //         .reduce((sum, current) => sum + current, 0);
    //     portfolioObject.unprocessedRedemptions = unprocessedOrders
    //         .filter(order => order.transactionType === "R")
    //         .map(order => parseFloat(order.amount))
    //         .reduce((sum, current) => sum + current, 0);
    //     portfolioObject.unprocessedSwitches = unprocessedOrders
    //         .filter(order => order.transactionType === "SI")
    //         .map(order => parseFloat(order.amount))
    //         .reduce((sum, current) => sum + current, 0);

    //     return portfolioObject;
    // }


    static combine(p1: PortfolioClass, p2: PortfolioClass) {
        // return new Portfolio({
        var combined = plainToClass(PortfolioClass, {
            holdings: [...p1.holdings, ...p2.holdings],
            unprocessedPurchases: [p1, p2].map(p => p.unprocessedPurchases).reduce((sum, current) => sum + current, 0),
            unprocessedRedemptions: [p1, p2].map(p => p.unprocessedRedemptions).reduce((sum, current) => sum + current, 0),
            unprocessedSwitches: [p1, p2].map(p => p.unprocessedSwitches).reduce((sum, current) => sum + current, 0),
        });

        combined.processHoldings()
        return combined;
    }
}


function ddmmyyyytoyyyymmdd(str: string) {
    var [dd, mm, yyyy] = str.split("/");
    return yyyy + "-" + mm + "-" + dd;
}
function ddmmmyyyytoyymmdd(str: string) {
    return moment(str, "DD-MMM-YYYY").format("YYYY-MM-DD");
}