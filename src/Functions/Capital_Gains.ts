import moment from "moment";
import fs from "fs";
import { PortfolioClass } from "../Classes/Portfolio";
import { PartnerModel, UserModel } from "../db/models";
import portfolioHelper from "../helpers/portfolioHelper";
import axios from 'axios';
var assetplusLogo = "https://assetplus-statics.s3.ap-south-1.amazonaws.com/report_assetplus_logo.png";
export default class CapitalGains {
    static async generate(userId: string, year: string, onlyInternal?: boolean) {
        var user = await UserModel.findById(userId);
        var advisorLogo = assetplusLogo;
        if (!user) throw "User not found";
        var portfolio: PortfolioClass = await portfolioHelper(userId);


        var partner = await PartnerModel.findOne({ adminId: user.subAdvisorId });
        if (partner && partner.image_logo) {
            let response = await axios.get(partner.image_logo.toString(), { responseType: 'arraybuffer' });
            if (response.status == 200) {
                advisorLogo = "data:" + response.headers["content-type"] + ";base64," + Buffer.from(response.data, 'binary').toString('base64');
            }
        }

        var startMoment = moment(year, "YYYY").set({ month: 3, date: 1 });
        var endMoment = moment(year, "YYYY").add({ years: 1 }).set({ month: 2, date: 31 });

        var startLabel = startMoment.format("YYYY-MM-DD");
        var endLabel = endMoment.format("YYYY-MM-DD");
        var hbsData: any = {
            advisorLogo,
            client: {
                name: user.firstName + " " + user.lastName,
                pan: user.panNumber,
                period: "FY " + startMoment.format("YYYY") + "-" + endMoment.format("YY"),
            },
            debt: {
                schemeNames: []
            },
            equity: {
                schemeNames: []
            }
        };


        portfolio.holdings.forEach((holding) => {
            console.log("checking isin: ", holding.isin, holding);
            var schemesArrayItem: any = {
                schemeName: holding.schemeName + " (ISIN: " + holding.isin + ")",
                folios: []
            }

            holding.folios.forEach((folio) => {

                var folioArrayItem: any = {
                    folio: folio.folioNo,
                    transactions: []
                }

                folio.sellQueue
                    .filter(sellItem => {
                        return sellItem.sellDate >= startLabel && sellItem.sellDate <= endLabel;
                    })
                    .forEach((sellItem) => {

                        sellItem.buyQueue.forEach((buyItem) => {

                            folioArrayItem.transactions.push({
                                Units: buyItem.sellUnits,
                                PDate: yyyymmddtoddmmyy(buyItem.buyDate),
                                PValue: buyItem.sellUnits * buyItem.originalNav,
                                Av: (buyItem.sellUnits * buyItem.buyNav),
                                // buynav: buyItem.buyNav,
                                RDate: yyyymmddtoddmmyy(sellItem.sellDate),
                                RValue: (buyItem.sellUnits * sellItem.sellNav),
                                stcg: buyItem.stcg,
                                ltcg: buyItem.ltcg
                            });

                        })

                    });


                if (folioArrayItem.transactions.length > 0)
                    schemesArrayItem.folios.push(folioArrayItem);

            })

            if (schemesArrayItem.folios.length > 0) {
                if (!["Equity", "Balanced"].includes(holding.legend)) {
                    hbsData.debt.schemeNames.push(schemesArrayItem);
                } else {
                    hbsData.equity.schemeNames.push(schemesArrayItem);
                }
            }
        });
        fs.writeFile("portfolio_1.json", JSON.stringify(portfolio, null, 4), (err: any) => { }); //Note: For saving the output in json file.

        return hbsData;
    }
}

function yyyymmddtoddmmyy(str: string) {
    var splits = str.split("-");
    return splits[2] + "/" + splits[1] + "/" + splits[0];
}
