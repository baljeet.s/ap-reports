import fs from 'fs';
import { describe, expect, test } from '@jest/globals';
import Portfolio from '../Functions/Portfolio';

var orders: any = [
    {
        "_id": "609f67d9bfc09b607271023d",
        "schemeCode": "100377",
        "bseOrderDate": "21/12/2017",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2017-12-21",
        "processingNAV": "1183.8740",
        "processingUnits": "0.845",
        "processingAmount": "1000.00",
        "createdAt": "2017-12-21",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271023e",
        "schemeCode": "100377",
        "bseOrderDate": "18/01/2018",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2018-01-18",
        "processingNAV": "1195.4158",
        "processingUnits": "0.837",
        "processingAmount": "1000.00",
        "createdAt": "2018-01-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271023f",
        "schemeCode": "100377",
        "bseOrderDate": "19/02/2018",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2018-02-19",
        "processingNAV": "1124.0935",
        "processingUnits": "0.890",
        "processingAmount": "1000.00",
        "createdAt": "2018-02-19",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710240",
        "schemeCode": "100377",
        "bseOrderDate": "19/03/2018",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2018-03-19",
        "processingNAV": "1084.7599",
        "processingUnits": "0.922",
        "processingAmount": "1000.00",
        "createdAt": "2018-03-19",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710241",
        "schemeCode": "100377",
        "bseOrderDate": "18/04/2018",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2018-04-18",
        "processingNAV": "1134.8436",
        "processingUnits": "0.881",
        "processingAmount": "1000.00",
        "createdAt": "2018-04-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710242",
        "schemeCode": "100377",
        "bseOrderDate": "18/05/2018",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2018-05-18",
        "processingNAV": "1101.6595",
        "processingUnits": "0.908",
        "processingAmount": "1000.00",
        "createdAt": "2018-05-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710243",
        "schemeCode": "100377",
        "bseOrderDate": "18/06/2018",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2018-06-18",
        "processingNAV": "1101.8661",
        "processingUnits": "0.908",
        "processingAmount": "1000.00",
        "createdAt": "2018-06-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710244",
        "schemeCode": "100377",
        "bseOrderDate": "18/07/2018",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2018-07-18",
        "processingNAV": "1055.4870",
        "processingUnits": "0.947",
        "processingAmount": "1000.00",
        "createdAt": "2018-07-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710245",
        "schemeCode": "100377",
        "bseOrderDate": "20/08/2018",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2018-08-20",
        "processingNAV": "1124.1176",
        "processingUnits": "0.890",
        "processingAmount": "1000.00",
        "createdAt": "2018-08-20",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710246",
        "schemeCode": "100377",
        "bseOrderDate": "18/09/2018",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2018-09-18",
        "processingNAV": "1099.6928",
        "processingUnits": "0.909",
        "processingAmount": "1000.00",
        "createdAt": "2018-09-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710247",
        "schemeCode": "100377",
        "bseOrderDate": "19/10/2018",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2018-10-19",
        "processingNAV": "994.1896",
        "processingUnits": "1.006",
        "processingAmount": "1000.00",
        "createdAt": "2018-10-19",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710248",
        "schemeCode": "100377",
        "bseOrderDate": "19/11/2018",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2018-11-19",
        "processingNAV": "1041.1885",
        "processingUnits": "0.960",
        "processingAmount": "1000.00",
        "createdAt": "2018-11-19",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710249",
        "schemeCode": "100377",
        "bseOrderDate": "18/12/2018",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2018-12-18",
        "processingNAV": "1062.9288",
        "processingUnits": "0.941",
        "processingAmount": "1000.00",
        "createdAt": "2018-12-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271024a",
        "schemeCode": "100377",
        "bseOrderDate": "31/12/2018",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "SO",
        "amount": "12679.56",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2018-12-31",
        "processingNAV": "1070.5582",
        "processingUnits": "11.844",
        "processingAmount": "12679.56",
        "createdAt": "2018-12-31",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271024b",
        "schemeCode": "100377",
        "bseOrderDate": "18/01/2019",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2019-01-18",
        "processingNAV": "1064.9694",
        "processingUnits": "0.939",
        "processingAmount": "1000.00",
        "createdAt": "2019-01-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271024c",
        "schemeCode": "100377",
        "bseOrderDate": "18/02/2019",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2019-02-18",
        "processingNAV": "996.1433",
        "processingUnits": "1.004",
        "processingAmount": "1000.00",
        "createdAt": "2019-02-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271024d",
        "schemeCode": "100377",
        "bseOrderDate": "18/03/2019",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2019-03-18",
        "processingNAV": "1094.4731",
        "processingUnits": "0.914",
        "processingAmount": "1000.00",
        "createdAt": "2019-03-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271024e",
        "schemeCode": "100377",
        "bseOrderDate": "18/04/2019",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2019-04-18",
        "processingNAV": "1126.1574",
        "processingUnits": "0.888",
        "processingAmount": "1000.00",
        "createdAt": "2019-04-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271024f",
        "schemeCode": "100377",
        "bseOrderDate": "20/05/2019",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2019-05-20",
        "processingNAV": "1113.6909",
        "processingUnits": "0.898",
        "processingAmount": "1000.00",
        "createdAt": "2019-05-20",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710250",
        "schemeCode": "100377",
        "bseOrderDate": "18/06/2019",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2019-06-18",
        "processingNAV": "1104.6010",
        "processingUnits": "0.905",
        "processingAmount": "1000.00",
        "createdAt": "2019-06-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710251",
        "schemeCode": "100377",
        "bseOrderDate": "18/07/2019",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2019-07-18",
        "processingNAV": "1092.2942",
        "processingUnits": "0.916",
        "processingAmount": "1000.00",
        "createdAt": "2019-07-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710252",
        "schemeCode": "100377",
        "bseOrderDate": "19/08/2019",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2019-08-19",
        "processingNAV": "1052.0783",
        "processingUnits": "0.950",
        "processingAmount": "1000.00",
        "createdAt": "2019-08-19",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710253",
        "schemeCode": "100377",
        "bseOrderDate": "18/09/2019",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2019-09-18",
        "processingNAV": "1036.4353",
        "processingUnits": "0.965",
        "processingAmount": "1000.00",
        "createdAt": "2019-09-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710254",
        "schemeCode": "100377",
        "bseOrderDate": "18/10/2019",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2019-10-18",
        "processingNAV": "1086.6634",
        "processingUnits": "0.920",
        "processingAmount": "1000.00",
        "createdAt": "2019-10-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710255",
        "schemeCode": "100377",
        "bseOrderDate": "18/11/2019",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2019-11-18",
        "processingNAV": "1114.1146",
        "processingUnits": "0.898",
        "processingAmount": "1000.00",
        "createdAt": "2019-11-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710256",
        "schemeCode": "100377",
        "bseOrderDate": "03/12/2019",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "R",
        "amount": "11430.91",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2019-12-03",
        "processingNAV": "1121.0177",
        "processingUnits": "10.197",
        "processingAmount": "11430.91",
        "createdAt": "2019-12-03",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710257",
        "schemeCode": "100377",
        "bseOrderDate": "18/12/2019",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "transactionType": "P",
        "amount": "1000.00",
        "folioNo": "401185465892",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2019-12-18",
        "processingNAV": "1127.0096",
        "processingUnits": "0.887",
        "processingAmount": "1000.00",
        "createdAt": "2019-12-18",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710228",
        "schemeCode": "105669",
        "bseOrderDate": "10/08/2020",
        "schemeName": "DSP Strategic Bond Fund - Regular Plan - Growth",
        "transactionType": "P",
        "amount": "999.95",
        "folioNo": "6247700/16",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2020-08-10",
        "processingNAV": "2574.2126",
        "processingUnits": "0.388",
        "processingAmount": "999.95",
        "createdAt": "2020-08-10",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271022e",
        "schemeCode": "115833",
        "bseOrderDate": "11/08/2020",
        "schemeName": "ICICI Prudential Regular Gold Savings Fund (FOF) - Growth",
        "transactionType": "P",
        "amount": "1999.90",
        "folioNo": "14587144/21",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2020-08-11",
        "processingNAV": "17.7987",
        "processingUnits": "112.362",
        "processingAmount": "1999.90",
        "createdAt": "2020-08-11",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710229",
        "schemeCode": "105669",
        "bseOrderDate": "10/09/2020",
        "schemeName": "DSP Strategic Bond Fund - Regular Plan - Growth",
        "transactionType": "P",
        "amount": "999.95",
        "folioNo": "6247700/16",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2020-09-10",
        "processingNAV": "2553.6487",
        "processingUnits": "0.392",
        "processingAmount": "999.95",
        "createdAt": "2020-09-10",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271022f",
        "schemeCode": "115833",
        "bseOrderDate": "11/09/2020",
        "schemeName": "ICICI Prudential Regular Gold Savings Fund (FOF) - Growth",
        "transactionType": "P",
        "amount": "1999.90",
        "folioNo": "14587144/21",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2020-09-11",
        "processingNAV": "17.0788",
        "processingUnits": "117.098",
        "processingAmount": "1999.90",
        "createdAt": "2020-09-11",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271022a",
        "schemeCode": "105669",
        "bseOrderDate": "12/10/2020",
        "schemeName": "DSP Strategic Bond Fund - Regular Plan - Growth",
        "transactionType": "P",
        "amount": "999.95",
        "folioNo": "6247700/16",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2020-10-12",
        "processingNAV": "2594.5854",
        "processingUnits": "0.385",
        "processingAmount": "999.95",
        "createdAt": "2020-10-12",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710230",
        "schemeCode": "115833",
        "bseOrderDate": "12/10/2020",
        "schemeName": "ICICI Prudential Regular Gold Savings Fund (FOF) - Growth",
        "transactionType": "P",
        "amount": "1999.90",
        "folioNo": "14587144/21",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2020-10-12",
        "processingNAV": "16.8361",
        "processingUnits": "118.786",
        "processingAmount": "1999.90",
        "createdAt": "2020-10-12",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271022b",
        "schemeCode": "105669",
        "bseOrderDate": "10/11/2020",
        "schemeName": "DSP Strategic Bond Fund - Regular Plan - Growth",
        "transactionType": "P",
        "amount": "999.95",
        "folioNo": "6247700/16",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2020-11-10",
        "processingNAV": "2607.8755",
        "processingUnits": "0.383",
        "processingAmount": "999.95",
        "createdAt": "2020-11-10",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710231",
        "schemeCode": "115833",
        "bseOrderDate": "11/11/2020",
        "schemeName": "ICICI Prudential Regular Gold Savings Fund (FOF) - Growth",
        "transactionType": "P",
        "amount": "1999.90",
        "folioNo": "14587144/21",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2020-11-11",
        "processingNAV": "16.6690",
        "processingUnits": "119.977",
        "processingAmount": "1999.90",
        "createdAt": "2020-11-11",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271022c",
        "schemeCode": "105669",
        "bseOrderDate": "10/12/2020",
        "schemeName": "DSP Strategic Bond Fund - Regular Plan - Growth",
        "transactionType": "P",
        "amount": "999.95",
        "folioNo": "6247700/16",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2020-12-10",
        "processingNAV": "2626.4926",
        "processingUnits": "0.381",
        "processingAmount": "999.95",
        "createdAt": "2020-12-10",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710232",
        "schemeCode": "115833",
        "bseOrderDate": "11/12/2020",
        "schemeName": "ICICI Prudential Regular Gold Savings Fund (FOF) - Growth",
        "transactionType": "P",
        "amount": "1999.90",
        "folioNo": "14587144/21",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2020-12-11",
        "processingNAV": "16.1911",
        "processingUnits": "123.518",
        "processingAmount": "1999.90",
        "createdAt": "2020-12-11",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271022d",
        "schemeCode": "105669",
        "bseOrderDate": "11/01/2021",
        "schemeName": "DSP Strategic Bond Fund - Regular Plan - Growth",
        "transactionType": "P",
        "amount": "999.95",
        "folioNo": "6247700/16",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2021-01-11",
        "processingNAV": "2633.5635",
        "processingUnits": "0.380",
        "processingAmount": "999.95",
        "createdAt": "2021-01-11",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710233",
        "schemeCode": "115833",
        "bseOrderDate": "11/01/2021",
        "schemeName": "ICICI Prudential Regular Gold Savings Fund (FOF) - Growth",
        "transactionType": "P",
        "amount": "1999.90",
        "folioNo": "14587144/21",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2021-01-11",
        "processingNAV": "16.3023",
        "processingUnits": "122.676",
        "processingAmount": "1999.90",
        "createdAt": "2021-01-11",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710237",
        "schemeCode": "102875",
        "bseOrderDate": "22/02/2021",
        "schemeName": "Kotak - Small Cap Fund - Growth",
        "transactionType": "P",
        "amount": "999.95",
        "folioNo": "8016916/14",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2021-02-22",
        "processingNAV": "112.748",
        "processingUnits": "8.869",
        "processingAmount": "999.95",
        "createdAt": "2021-02-22",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271023a",
        "schemeCode": "112932",
        "bseOrderDate": "22/02/2021",
        "schemeName": "Mirae Asset Emerging Bluechip Fund - Regular Plan - Growth Option",
        "transactionType": "P",
        "amount": "999.95",
        "folioNo": "79936310696",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2021-02-22",
        "processingNAV": "77.157",
        "processingUnits": "12.960",
        "processingAmount": "999.95",
        "createdAt": "2021-02-22",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710234",
        "schemeCode": "105503",
        "bseOrderDate": "25/02/2021",
        "schemeName": "Invesco India Midcap Fund - Growth Option",
        "transactionType": "P",
        "amount": "999.95",
        "folioNo": "3108417116",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2021-02-25",
        "processingNAV": "69.88",
        "processingUnits": "14.310",
        "processingAmount": "999.95",
        "createdAt": "2021-02-25",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710238",
        "schemeCode": "102875",
        "bseOrderDate": "22/03/2021",
        "schemeName": "Kotak - Small Cap Fund - Growth",
        "transactionType": "P",
        "amount": "999.95",
        "folioNo": "8016916/14",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2021-03-22",
        "processingNAV": "117.913",
        "processingUnits": "8.480",
        "processingAmount": "999.95",
        "createdAt": "2021-03-22",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271023b",
        "schemeCode": "112932",
        "bseOrderDate": "22/03/2021",
        "schemeName": "Mirae Asset Emerging Bluechip Fund - Regular Plan - Growth Option",
        "transactionType": "P",
        "amount": "999.95",
        "folioNo": "79936310696",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2021-03-22",
        "processingNAV": "77.949",
        "processingUnits": "12.828",
        "processingAmount": "999.95",
        "createdAt": "2021-03-22",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710235",
        "schemeCode": "105503",
        "bseOrderDate": "25/03/2021",
        "schemeName": "Invesco India Midcap Fund - Growth Option",
        "transactionType": "P",
        "amount": "999.95",
        "folioNo": "3108417116",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2021-03-25",
        "processingNAV": "67.83",
        "processingUnits": "14.742",
        "processingAmount": "999.95",
        "createdAt": "2021-03-25",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b607271023c",
        "schemeCode": "112932",
        "bseOrderDate": "20/04/2021",
        "schemeName": "Mirae Asset Emerging Bluechip Fund - Regular Plan - Growth Option",
        "transactionType": "P",
        "amount": "999.95",
        "folioNo": "79936310696",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2021-04-20",
        "processingNAV": "77.374",
        "processingUnits": "12.924",
        "processingAmount": "999.95",
        "createdAt": "2021-04-20",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710239",
        "schemeCode": "102875",
        "bseOrderDate": "22/04/2021",
        "schemeName": "Kotak - Small Cap Fund - Growth",
        "transactionType": "P",
        "amount": "999.95",
        "folioNo": "8016916/14",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2021-04-22",
        "processingNAV": "118.552",
        "processingUnits": "8.435",
        "processingAmount": "999.95",
        "createdAt": "2021-04-22",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    },
    {
        "_id": "609f67d9bfc09b6072710236",
        "schemeCode": "105503",
        "bseOrderDate": "26/04/2021",
        "schemeName": "Invesco India Midcap Fund - Growth Option",
        "transactionType": "P",
        "amount": "999.95",
        "folioNo": "3108417116",
        "orderStatus": "VALID",
        "isProcessed": true,
        "processingDate": "2021-04-26",
        "processingNAV": "68.22",
        "processingUnits": "14.658",
        "processingAmount": "999.95",
        "createdAt": "2021-04-26",
        "orderId": "",
        "userId": "5ff31b0f7a36e024929d0438",
        "mode": "priyashah03081991@gmail.com"
    }
];

var schemes: any = [
    {
        "schemeCode": "100377",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "aumInLakhs": 0,
        "amcCode": "Nippon India Mutual Fund",
        "amfiBroad": "Equity",
        "amfiSub": "Mid Cap",
        "riskLevel": 0,
        "legend": "Equity",
        "isRecommended": false,
        "purchaseAllowed": false,
        "sipAllowed": false,
        "lockOutPeriod": 1,
        "gfNAV": "1177.8459"
    },
    {
        "schemeCode": "102875",
        "schemeName": "Kotak - Small Cap Fund - Growth",
        "aumInLakhs": 0,
        "amcCode": "Kotak Mahindra Mutual Fund",
        "amfiBroad": "Equity",
        "amfiSub": "Small Cap",
        "riskLevel": 0,
        "legend": "Equity",
        "isRecommended": false,
        "purchaseAllowed": false,
        "sipAllowed": false,
        "lockOutPeriod": 1,
        "gfNAV": "81.696"
    },
    {
        "schemeCode": "105503",
        "schemeName": "Invesco India Midcap Fund - Growth Option",
        "aumInLakhs": 0,
        "amcCode": "Invesco Mutual Fund",
        "amfiBroad": "Equity",
        "amfiSub": "Mid Cap",
        "riskLevel": 0,
        "legend": "Equity",
        "isRecommended": false,
        "purchaseAllowed": false,
        "sipAllowed": false,
        "lockOutPeriod": 1,
        "gfNAV": "49.71"
    },
    {
        "schemeCode": "105669",
        "schemeName": "DSP Strategic Bond Fund - Regular Plan - Growth",
        "aumInLakhs": 0,
        "amcCode": "DSP Mutual Fund",
        "amfiBroad": "Debt",
        "amfiSub": "Dynamic Bond",
        "riskLevel": 0,
        "legend": "Debt",
        "isRecommended": false,
        "purchaseAllowed": false,
        "sipAllowed": false,
        "lockOutPeriod": 0,
        "gfNAV": "1988.3432"
    },
    {
        "schemeCode": "112932",
        "schemeName": "Mirae Asset Emerging Bluechip Fund - Regular Plan - Growth Option",
        "aumInLakhs": 0,
        "amcCode": "Mirae Asset Mutual Fund",
        "amfiBroad": "Equity",
        "amfiSub": "Large & Mid Cap",
        "riskLevel": 0,
        "legend": "Equity",
        "isRecommended": false,
        "purchaseAllowed": false,
        "sipAllowed": false,
        "lockOutPeriod": 1,
        "gfNAV": "52.168"
    },
    {
        "schemeCode": "115833",
        "schemeName": "ICICI Prudential Regular Gold Savings Fund (FOF) - Growth",
        "aumInLakhs": 0,
        "amcCode": "ICICI Prudential Mutual Fund",
        "amfiBroad": "Other",
        "amfiSub": "FoF Domestic",
        "riskLevel": 0,
        "legend": "Debt",
        "isRecommended": false,
        "purchaseAllowed": false,
        "sipAllowed": false,
        "lockOutPeriod": 0,
        "gfNAV": "10.2807"
    }
];

var nav: any = {
    "100377": {
        "nav": "2217.2532",
        "date": "30-Nov-2022",
        "change": 28.158600000000206,
        "isEquity": true,
        "legend": "Equity",
        "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
        "lockOutPeriod": 1,
        "amfiBroad": "Equity",
        "amfiSub": "Mid Cap",
        "gfNAV": "1177.8459",
        "amcName": "Nippon India Mutual Fund",
        "isin": "INF204K01323",
        "amcId": "21",
        "amfiType": "Open Ended Schemes(Equity Scheme - Mid Cap Fund)"
    },
    "102875": {
        "nav": "165.063",
        "date": "30-Nov-2022",
        "change": 0.42499999999998295,
        "isEquity": true,
        "legend": "Equity",
        "schemeName": "Kotak - Small Cap Fund - Growth",
        "lockOutPeriod": 1,
        "amfiBroad": "Equity",
        "amfiSub": "Small Cap",
        "gfNAV": "81.696",
        "amcName": "Kotak Mahindra Mutual Fund",
        "isin": "INF174K01211",
        "amcId": "17",
        "amfiType": "Open Ended Schemes(Equity Scheme - Small Cap Fund)"
    },
    "105503": {
        "nav": "91",
        "date": "30-Nov-2022",
        "change": 0.5699999999999932,
        "isEquity": true,
        "legend": "Equity",
        "schemeName": "Invesco India Midcap Fund - Growth Option",
        "lockOutPeriod": 1,
        "amfiBroad": "Equity",
        "amfiSub": "Mid Cap",
        "gfNAV": "49.71",
        "amcName": "Invesco Mutual Fund",
        "isin": "INF205K01BC9",
        "amcId": "42",
        "amfiType": "Open Ended Schemes(Equity Scheme - Mid Cap Fund)"
    },
    "105669": {
        "nav": "2727.2571",
        "date": "30-Nov-2022",
        "change": 0.6027999999996609,
        "isEquity": false,
        "legend": "Debt",
        "schemeName": "DSP Strategic Bond Fund - Regular Plan - Growth",
        "lockOutPeriod": 0,
        "amfiBroad": "Debt",
        "amfiSub": "Dynamic Bond",
        "gfNAV": "1988.3432",
        "amcName": "DSP Mutual Fund",
        "isin": "INF740K01GK7",
        "amcId": "6",
        "amfiType": "Open Ended Schemes(Debt Scheme - Dynamic Bond)"
    },
    "112932": {
        "nav": "99.569",
        "date": "30-Nov-2022",
        "change": 0.4759999999999991,
        "isEquity": true,
        "legend": "Equity",
        "schemeName": "Mirae Asset Emerging Bluechip Fund - Regular Plan - Growth Option",
        "lockOutPeriod": 1,
        "amfiBroad": "Equity",
        "amfiSub": "Large & Mid Cap",
        "gfNAV": "52.168",
        "amcName": "Mirae Asset Mutual Fund",
        "isin": "INF769K01101",
        "amcId": "45",
        "amfiType": "Open Ended Schemes(Equity Scheme - Large & Mid Cap Fund)"
    },
    "115833": {
        "nav": "16.9317",
        "date": "30-Nov-2022",
        "change": 0.0033999999999991815,
        "isEquity": false,
        "legend": "Debt",
        "schemeName": "ICICI Prudential Regular Gold Savings Fund (FOF) - Growth",
        "lockOutPeriod": 0,
        "amfiBroad": "Other",
        "amfiSub": "FoF Domestic",
        "gfNAV": "10.2807",
        "amcName": "ICICI Prudential Mutual Fund",
        "isin": "INF109K01TK8",
        "amcId": "20",
        "amfiType": "Open Ended Schemes(Other Scheme - FoF Domestic)"
    }
};

var generateOutput = {
    "totalDebtInvestment": 17998.3779693,
    "totalDebtValue": 18393.5309628,
    "debtGrowth": 395.15299349999987,
    "totalEquityInvestment": 9999.275375200003,
    "totalEquityValue": 14054.8131084,
    "equityGrowth": 4055.537733199997,
    "xirrReturns": 0.07662353714113207,
    "totalUnits": 825.819,
    "totalInvestment": 27997.6533445,
    "assetValue": 32448.344071199997,
    "flowTotal": 4449.6940712,
    "growth": 4450.6907267,
    "unrealizedCg": 0,
    "divPaid": 0,
    "divReinvested": 0,
    "dailyChange": 0,
    "lockFreeUnits": 825.819,
    "lockFreeValue": 32448.344071199997,
    "unrealizedCG": 4450.6907267,
    "stcg": 395.15299349999924,
    "stcgTax": 118.54589804999976,
    "absoluteReturns": 0.15896656308784235,
    "debtRatio": 57,
    "unprocessedPurchases": 0,
    "unprocessedRedemptions": 0,
    "unprocessedSwitches": 0,
    "holdings": [
        {
            "isAssetPlus": true,
            "schemeCode": "105669",
            "schemeName": "DSP Strategic Bond Fund - Regular Plan - Growth",
            "nav": "2727.2571",
            "navDate": "2022-11-30",
            "navChange": 0.6027999999996609,
            "lockInPeriod": 0,
            "amfiBroad": "Debt",
            "amfiSub": "Dynamic Bond",
            "legend": "Debt",
            "amcCode": "DSP Mutual Fund",
            "folios": [
                {
                    "folioNo": "6247700/16",
                    "schemeCode": "105669",
                    "schemeName": "",
                    "nav": "2727.2571",
                    "navDate": "2022-11-30",
                    "navChange": 0.6027999999996609,
                    "lockInPeriod": 0,
                    "amfiBroad": "Debt",
                    "amfiSub": "Dynamic Bond",
                    "legend": "Debt",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2020-08-10",
                            "nav": 2574.2126,
                            "units": "0.388",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-09-10",
                            "nav": 2553.6487,
                            "units": "0.392",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-10-12",
                            "nav": 2594.5854,
                            "units": "0.385",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-11-10",
                            "nav": 2607.8755,
                            "units": "0.383",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-12-10",
                            "nav": 2626.4926,
                            "units": "0.381",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-01-11",
                            "nav": 2633.5635,
                            "units": "0.380",
                            "amount": "999.95",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 3880,
                            "nav": 2574.2126,
                            "date": "2020-08-10",
                            "type": "P"
                        },
                        {
                            "units_10000": 3920,
                            "nav": 2553.6487,
                            "date": "2020-09-10",
                            "type": "P"
                        },
                        {
                            "units_10000": 3850,
                            "nav": 2594.5854,
                            "date": "2020-10-12",
                            "type": "P"
                        },
                        {
                            "units_10000": 3830,
                            "nav": 2607.8755,
                            "date": "2020-11-10",
                            "type": "P"
                        },
                        {
                            "units_10000": 3810,
                            "nav": 2626.4926,
                            "date": "2020-12-10",
                            "type": "P"
                        },
                        {
                            "units_10000": 3800,
                            "nav": 2633.5635,
                            "date": "2021-01-11",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [],
                    "cashFlows": [
                        {
                            "amount": -999.95,
                            "when": "2020-08-10T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2020-09-10T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2020-10-12T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2020-11-10T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2020-12-10T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-01-11T00:00:00.000Z"
                        },
                        {
                            "amount": 6297.2366439,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 2.309,
                    "totalInvestment": 5999.0042853,
                    "divReinvested": 0,
                    "assetValue": 6297.2366439,
                    "unrealizedCG": 298.2323585999993,
                    "unrealizedSTCG": 298.23235859999943,
                    "unrealizedLTCG": 0,
                    "growth": 298.2323585999993,
                    "flowTotal": 297.53664389999994,
                    "dailyChange": 0,
                    "lockFreeUnits": 2.309,
                    "lockFreeValue": 6297.2366439,
                    "unrealizedCGIndexed": 298.23235859999943,
                    "stcg": 298.23235859999943,
                    "stcgTax": 89.46970757999982,
                    "gfNAV": 1988.3432,
                    "xirrReturns": 0.023359543575196564,
                    "absoluteReturns": 0.049713643200887496
                }
            ],
            "totalUnits": 2.309,
            "totalInvestment": 5999.0042853,
            "assetValue": 6297.2366439,
            "flowTotal": 297.53664389999994,
            "growth": 298.2323585999993,
            "unrealizedCG": 298.2323585999993,
            "unrealizedCGIndexed": 298.23235859999943,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 298.23235859999943,
            "unrealizedLTCG": 0,
            "dailyChange": 0,
            "lockFreeUnits": 2.309,
            "lockFreeValue": 6297.2366439,
            "stcg": 298.23235859999943,
            "stcgTax": 89.46970757999982,
            "cashFlows": [
                {
                    "amount": -999.95,
                    "when": "2020-08-10T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2020-09-10T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2020-10-12T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2020-11-10T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2020-12-10T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-01-11T00:00:00.000Z"
                },
                {
                    "amount": 6297.2366439,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.023359543575196564,
            "absoluteReturns": 0.049713643200887496,
            "gfNAV": 1988.3432
        },
        {
            "isAssetPlus": true,
            "schemeCode": "115833",
            "schemeName": "ICICI Prudential Regular Gold Savings Fund (FOF) - Growth",
            "nav": "16.9317",
            "navDate": "2022-11-30",
            "navChange": 0.0033999999999991815,
            "lockInPeriod": 0,
            "amfiBroad": "Other",
            "amfiSub": "FoF Domestic",
            "legend": "Debt",
            "amcCode": "ICICI Prudential Mutual Fund",
            "folios": [
                {
                    "folioNo": "14587144/21",
                    "schemeCode": "115833",
                    "schemeName": "",
                    "nav": "16.9317",
                    "navDate": "2022-11-30",
                    "navChange": 0.0033999999999991815,
                    "lockInPeriod": 0,
                    "amfiBroad": "Other",
                    "amfiSub": "FoF Domestic",
                    "legend": "Debt",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2020-08-11",
                            "nav": 17.7987,
                            "units": "112.362",
                            "amount": "1999.90",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-09-11",
                            "nav": 17.0788,
                            "units": "117.098",
                            "amount": "1999.90",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-10-12",
                            "nav": 16.8361,
                            "units": "118.786",
                            "amount": "1999.90",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-11-11",
                            "nav": 16.669,
                            "units": "119.977",
                            "amount": "1999.90",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-12-11",
                            "nav": 16.1911,
                            "units": "123.518",
                            "amount": "1999.90",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-01-11",
                            "nav": 16.3023,
                            "units": "122.676",
                            "amount": "1999.90",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 1123620,
                            "nav": 17.7987,
                            "date": "2020-08-11",
                            "type": "P"
                        },
                        {
                            "units_10000": 1170980,
                            "nav": 17.0788,
                            "date": "2020-09-11",
                            "type": "P"
                        },
                        {
                            "units_10000": 1187860,
                            "nav": 16.8361,
                            "date": "2020-10-12",
                            "type": "P"
                        },
                        {
                            "units_10000": 1199770,
                            "nav": 16.669,
                            "date": "2020-11-11",
                            "type": "P"
                        },
                        {
                            "units_10000": 1235180,
                            "nav": 16.1911,
                            "date": "2020-12-11",
                            "type": "P"
                        },
                        {
                            "units_10000": 1226760,
                            "nav": 16.3023,
                            "date": "2021-01-11",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [],
                    "cashFlows": [
                        {
                            "amount": -1999.9,
                            "when": "2020-08-11T00:00:00.000Z"
                        },
                        {
                            "amount": -1999.9,
                            "when": "2020-09-11T00:00:00.000Z"
                        },
                        {
                            "amount": -1999.9,
                            "when": "2020-10-12T00:00:00.000Z"
                        },
                        {
                            "amount": -1999.9,
                            "when": "2020-11-11T00:00:00.000Z"
                        },
                        {
                            "amount": -1999.9,
                            "when": "2020-12-11T00:00:00.000Z"
                        },
                        {
                            "amount": -1999.9,
                            "when": "2021-01-11T00:00:00.000Z"
                        },
                        {
                            "amount": 12096.2943189,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 714.417,
                    "totalInvestment": 11999.373683999998,
                    "divReinvested": 0,
                    "assetValue": 12096.2943189,
                    "unrealizedCG": 96.92063490000146,
                    "unrealizedSTCG": 96.92063489999981,
                    "unrealizedLTCG": 0,
                    "growth": 96.92063490000146,
                    "flowTotal": 96.89431890000014,
                    "dailyChange": 0,
                    "lockFreeUnits": 714.417,
                    "lockFreeValue": 12096.2943189,
                    "unrealizedCGIndexed": 96.92063489999981,
                    "stcg": 96.92063489999981,
                    "stcgTax": 29.076190469999943,
                    "gfNAV": 10.2807,
                    "xirrReturns": 0.0038479319488984353,
                    "absoluteReturns": 0.008077141145227916
                }
            ],
            "totalUnits": 714.417,
            "totalInvestment": 11999.373683999998,
            "assetValue": 12096.2943189,
            "flowTotal": 96.89431890000014,
            "growth": 96.92063490000146,
            "unrealizedCG": 96.92063490000146,
            "unrealizedCGIndexed": 96.92063489999981,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 96.92063489999981,
            "unrealizedLTCG": 0,
            "dailyChange": 0,
            "lockFreeUnits": 714.417,
            "lockFreeValue": 12096.2943189,
            "stcg": 96.92063489999981,
            "stcgTax": 29.076190469999943,
            "cashFlows": [
                {
                    "amount": -1999.9,
                    "when": "2020-08-11T00:00:00.000Z"
                },
                {
                    "amount": -1999.9,
                    "when": "2020-09-11T00:00:00.000Z"
                },
                {
                    "amount": -1999.9,
                    "when": "2020-10-12T00:00:00.000Z"
                },
                {
                    "amount": -1999.9,
                    "when": "2020-11-11T00:00:00.000Z"
                },
                {
                    "amount": -1999.9,
                    "when": "2020-12-11T00:00:00.000Z"
                },
                {
                    "amount": -1999.9,
                    "when": "2021-01-11T00:00:00.000Z"
                },
                {
                    "amount": 12096.2943189,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.0038479319488984353,
            "absoluteReturns": 0.008077141145227916,
            "gfNAV": 10.2807
        },
        {
            "isAssetPlus": true,
            "schemeCode": "105503",
            "schemeName": "Invesco India Midcap Fund - Growth Option",
            "nav": "91",
            "navDate": "2022-11-30",
            "navChange": 0.5699999999999932,
            "lockInPeriod": 0,
            "amfiBroad": "Equity",
            "amfiSub": "Mid Cap",
            "legend": "Equity",
            "amcCode": "Invesco Mutual Fund",
            "folios": [
                {
                    "folioNo": "3108417116",
                    "schemeCode": "105503",
                    "schemeName": "",
                    "nav": "91",
                    "navDate": "2022-11-30",
                    "navChange": 0.5699999999999932,
                    "lockInPeriod": 0,
                    "amfiBroad": "Equity",
                    "amfiSub": "Mid Cap",
                    "legend": "Equity",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2021-02-25",
                            "nav": 69.88,
                            "units": "14.310",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-03-25",
                            "nav": 67.83,
                            "units": "14.742",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-04-26",
                            "nav": 68.22,
                            "units": "14.658",
                            "amount": "999.95",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 143100,
                            "nav": 69.88,
                            "date": "2021-02-25",
                            "type": "P"
                        },
                        {
                            "units_10000": 147420,
                            "nav": 67.83,
                            "date": "2021-03-25",
                            "type": "P"
                        },
                        {
                            "units_10000": 146580,
                            "nav": 68.22,
                            "date": "2021-04-26",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [],
                    "cashFlows": [
                        {
                            "amount": -999.95,
                            "when": "2021-02-25T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-03-25T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-04-26T00:00:00.000Z"
                        },
                        {
                            "amount": 3977.61,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 43.71,
                    "totalInvestment": 2999.90142,
                    "divReinvested": 0,
                    "assetValue": 3977.61,
                    "unrealizedCG": 977.70858,
                    "unrealizedSTCG": 0,
                    "unrealizedLTCG": 977.70858,
                    "growth": 977.70858,
                    "flowTotal": 977.7599999999998,
                    "dailyChange": 0,
                    "lockFreeUnits": 43.71,
                    "lockFreeValue": 3977.61,
                    "unrealizedCGIndexed": 977.70858,
                    "stcg": 0,
                    "stcgTax": 0,
                    "gfNAV": 49.71,
                    "xirrReturns": 0.18265563228420076,
                    "absoluteReturns": 0.3259135695198944
                }
            ],
            "totalUnits": 43.71,
            "totalInvestment": 2999.90142,
            "assetValue": 3977.61,
            "flowTotal": 977.7599999999998,
            "growth": 977.70858,
            "unrealizedCG": 977.70858,
            "unrealizedCGIndexed": 977.70858,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 0,
            "unrealizedLTCG": 977.70858,
            "dailyChange": 0,
            "lockFreeUnits": 43.71,
            "lockFreeValue": 3977.61,
            "stcg": 0,
            "stcgTax": 0,
            "cashFlows": [
                {
                    "amount": -999.95,
                    "when": "2021-02-25T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-03-25T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-04-26T00:00:00.000Z"
                },
                {
                    "amount": 3977.61,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.18265563228420076,
            "absoluteReturns": 0.3259135695198944,
            "gfNAV": 49.71
        },
        {
            "isAssetPlus": true,
            "schemeCode": "102875",
            "schemeName": "Kotak - Small Cap Fund - Growth",
            "nav": "165.063",
            "navDate": "2022-11-30",
            "navChange": 0.42499999999998295,
            "lockInPeriod": 0,
            "amfiBroad": "Equity",
            "amfiSub": "Small Cap",
            "legend": "Equity",
            "amcCode": "Kotak Mahindra Mutual Fund",
            "folios": [
                {
                    "folioNo": "8016916/14",
                    "schemeCode": "102875",
                    "schemeName": "",
                    "nav": "165.063",
                    "navDate": "2022-11-30",
                    "navChange": 0.42499999999998295,
                    "lockInPeriod": 0,
                    "amfiBroad": "Equity",
                    "amfiSub": "Small Cap",
                    "legend": "Equity",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2021-02-22",
                            "nav": 112.748,
                            "units": "8.869",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-03-22",
                            "nav": 117.913,
                            "units": "8.480",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-04-22",
                            "nav": 118.552,
                            "units": "8.435",
                            "amount": "999.95",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 88690,
                            "nav": 112.748,
                            "date": "2021-02-22",
                            "type": "P"
                        },
                        {
                            "units_10000": 84800,
                            "nav": 117.913,
                            "date": "2021-03-22",
                            "type": "P"
                        },
                        {
                            "units_10000": 84350,
                            "nav": 118.552,
                            "date": "2021-04-22",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [],
                    "cashFlows": [
                        {
                            "amount": -999.95,
                            "when": "2021-02-22T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-03-22T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-04-22T00:00:00.000Z"
                        },
                        {
                            "amount": 4255.984391999999,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 25.784,
                    "totalInvestment": 2999.8503720000003,
                    "divReinvested": 0,
                    "assetValue": 4255.984391999999,
                    "unrealizedCG": 1256.134019999999,
                    "unrealizedSTCG": 0,
                    "unrealizedLTCG": 1256.1340199999995,
                    "growth": 1256.134019999999,
                    "flowTotal": 1256.134391999999,
                    "dailyChange": 0,
                    "lockFreeUnits": 25.784,
                    "lockFreeValue": 4255.984391999999,
                    "unrealizedCGIndexed": 1256.1340199999995,
                    "stcg": 0,
                    "stcgTax": 0,
                    "gfNAV": 81.696,
                    "xirrReturns": 0.22980468674174015,
                    "absoluteReturns": 0.4187322246884382
                }
            ],
            "totalUnits": 25.784,
            "totalInvestment": 2999.8503720000003,
            "assetValue": 4255.984391999999,
            "flowTotal": 1256.134391999999,
            "growth": 1256.134019999999,
            "unrealizedCG": 1256.134019999999,
            "unrealizedCGIndexed": 1256.1340199999995,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 0,
            "unrealizedLTCG": 1256.1340199999995,
            "dailyChange": 0,
            "lockFreeUnits": 25.784,
            "lockFreeValue": 4255.984391999999,
            "stcg": 0,
            "stcgTax": 0,
            "cashFlows": [
                {
                    "amount": -999.95,
                    "when": "2021-02-22T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-03-22T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-04-22T00:00:00.000Z"
                },
                {
                    "amount": 4255.984391999999,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.22980468674174015,
            "absoluteReturns": 0.4187322246884382,
            "gfNAV": 81.696
        },
        {
            "isAssetPlus": true,
            "schemeCode": "112932",
            "schemeName": "Mirae Asset Emerging Bluechip Fund - Regular Plan - Growth Option",
            "nav": "99.569",
            "navDate": "2022-11-30",
            "navChange": 0.4759999999999991,
            "lockInPeriod": 0,
            "amfiBroad": "Equity",
            "amfiSub": "Large & Mid Cap",
            "legend": "Equity",
            "amcCode": "Mirae Asset Mutual Fund",
            "folios": [
                {
                    "folioNo": "79936310696",
                    "schemeCode": "112932",
                    "schemeName": "",
                    "nav": "99.569",
                    "navDate": "2022-11-30",
                    "navChange": 0.4759999999999991,
                    "lockInPeriod": 0,
                    "amfiBroad": "Equity",
                    "amfiSub": "Large & Mid Cap",
                    "legend": "Equity",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2021-02-22",
                            "nav": 77.157,
                            "units": "12.960",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-03-22",
                            "nav": 77.949,
                            "units": "12.828",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-04-20",
                            "nav": 77.374,
                            "units": "12.924",
                            "amount": "999.95",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 129600,
                            "nav": 77.157,
                            "date": "2021-02-22",
                            "type": "P"
                        },
                        {
                            "units_10000": 128280,
                            "nav": 77.949,
                            "date": "2021-03-22",
                            "type": "P"
                        },
                        {
                            "units_10000": 129240,
                            "nav": 77.374,
                            "date": "2021-04-20",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [],
                    "cashFlows": [
                        {
                            "amount": -999.95,
                            "when": "2021-02-22T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-03-22T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-04-20T00:00:00.000Z"
                        },
                        {
                            "amount": 3854.5151280000005,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 38.712,
                    "totalInvestment": 2999.8660680000003,
                    "divReinvested": 0,
                    "assetValue": 3854.5151280000005,
                    "unrealizedCG": 854.6490600000002,
                    "unrealizedSTCG": 0,
                    "unrealizedLTCG": 854.6490600000002,
                    "growth": 854.6490600000002,
                    "flowTotal": 854.6651280000001,
                    "dailyChange": 0,
                    "lockFreeUnits": 38.712,
                    "lockFreeValue": 3854.5151280000005,
                    "unrealizedCGIndexed": 854.6490600000002,
                    "stcg": 0,
                    "stcgTax": 0,
                    "gfNAV": 52.168,
                    "xirrReturns": 0.15964117890369958,
                    "absoluteReturns": 0.28489573888536684
                }
            ],
            "totalUnits": 38.712,
            "totalInvestment": 2999.8660680000003,
            "assetValue": 3854.5151280000005,
            "flowTotal": 854.6651280000001,
            "growth": 854.6490600000002,
            "unrealizedCG": 854.6490600000002,
            "unrealizedCGIndexed": 854.6490600000002,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 0,
            "unrealizedLTCG": 854.6490600000002,
            "dailyChange": 0,
            "lockFreeUnits": 38.712,
            "lockFreeValue": 3854.5151280000005,
            "stcg": 0,
            "stcgTax": 0,
            "cashFlows": [
                {
                    "amount": -999.95,
                    "when": "2021-02-22T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-03-22T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-04-20T00:00:00.000Z"
                },
                {
                    "amount": 3854.5151280000005,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.15964117890369958,
            "absoluteReturns": 0.28489573888536684,
            "gfNAV": 52.168
        },
        {
            "isAssetPlus": true,
            "schemeCode": "100377",
            "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
            "nav": "2217.2532",
            "navDate": "2022-11-30",
            "navChange": 28.158600000000206,
            "lockInPeriod": 0,
            "amfiBroad": "Equity",
            "amfiSub": "Mid Cap",
            "legend": "Equity",
            "amcCode": "Nippon India Mutual Fund",
            "folios": [
                {
                    "folioNo": "401185465892",
                    "schemeCode": "100377",
                    "schemeName": "",
                    "nav": "2217.2532",
                    "navDate": "2022-11-30",
                    "navChange": 28.158600000000206,
                    "lockInPeriod": 0,
                    "amfiBroad": "Equity",
                    "amfiSub": "Mid Cap",
                    "legend": "Equity",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2017-12-21",
                            "nav": 1183.874,
                            "units": "0.845",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-01-18",
                            "nav": 1195.4158,
                            "units": "0.837",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-02-19",
                            "nav": 1124.0935,
                            "units": "0.890",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-03-19",
                            "nav": 1084.7599,
                            "units": "0.922",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-04-18",
                            "nav": 1134.8436,
                            "units": "0.881",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-05-18",
                            "nav": 1101.6595,
                            "units": "0.908",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-06-18",
                            "nav": 1101.8661,
                            "units": "0.908",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-07-18",
                            "nav": 1055.487,
                            "units": "0.947",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-08-20",
                            "nav": 1124.1176,
                            "units": "0.890",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-09-18",
                            "nav": 1099.6928,
                            "units": "0.909",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-10-19",
                            "nav": 994.1896,
                            "units": "1.006",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-11-19",
                            "nav": 1041.1885,
                            "units": "0.960",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-12-18",
                            "nav": 1062.9288,
                            "units": "0.941",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "SO",
                            "navDate": "2018-12-31",
                            "nav": 1070.5582,
                            "units": "11.844",
                            "amount": "12679.56",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-01-18",
                            "nav": 1064.9694,
                            "units": "0.939",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-02-18",
                            "nav": 996.1433,
                            "units": "1.004",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-03-18",
                            "nav": 1094.4731,
                            "units": "0.914",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-04-18",
                            "nav": 1126.1574,
                            "units": "0.888",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-05-20",
                            "nav": 1113.6909,
                            "units": "0.898",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-06-18",
                            "nav": 1104.601,
                            "units": "0.905",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-07-18",
                            "nav": 1092.2942,
                            "units": "0.916",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-08-19",
                            "nav": 1052.0783,
                            "units": "0.950",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-09-18",
                            "nav": 1036.4353,
                            "units": "0.965",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-10-18",
                            "nav": 1086.6634,
                            "units": "0.920",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-11-18",
                            "nav": 1114.1146,
                            "units": "0.898",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "R",
                            "navDate": "2019-12-03",
                            "nav": 1121.0177,
                            "units": "10.197",
                            "amount": "11430.91",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-12-18",
                            "nav": 1127.0096,
                            "units": "0.887",
                            "amount": "1000.00",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 8870,
                            "nav": 1127.0096,
                            "date": "2019-12-18",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [
                        {
                            "sellAmount": "12679.56",
                            "sellDate": "2018-12-31",
                            "sellUnits": 11.844,
                            "sellNav": 1070.5582,
                            "buyQueue": [
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.845,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1183.874,
                                    "buyDate": "2017-12-21",
                                    "originalNav": 1183.874,
                                    "age": 1.0268817204301075,
                                    "gain": -95.75185100000006,
                                    "stcg": 0,
                                    "ltcg": -95.75185100000006,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.837,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1195.4158,
                                    "buyDate": "2018-01-18",
                                    "originalNav": 1195.4158,
                                    "age": 0.9516129032258065,
                                    "gain": -104.50581120000004,
                                    "stcg": -104.50581120000004,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.89,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1124.0935,
                                    "buyDate": "2018-02-19",
                                    "originalNav": 1124.0935,
                                    "age": 0.8601190476190476,
                                    "gain": -47.64641700000001,
                                    "stcg": -47.64641700000001,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.922,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1084.7599,
                                    "buyDate": "2018-03-19",
                                    "originalNav": 1084.7599,
                                    "age": 0.782258064516129,
                                    "gain": -13.093967400000068,
                                    "stcg": -13.093967400000068,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.881,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1134.8436,
                                    "buyDate": "2018-04-18",
                                    "originalNav": 1134.8436,
                                    "age": 0.7000000000000001,
                                    "gain": -56.63543739999999,
                                    "stcg": -56.63543739999999,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.908,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1101.6595,
                                    "buyDate": "2018-05-18",
                                    "originalNav": 1101.6595,
                                    "age": 0.6182795698924731,
                                    "gain": -28.239980400000036,
                                    "stcg": -28.239980400000036,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.908,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1101.8661,
                                    "buyDate": "2018-06-18",
                                    "originalNav": 1101.8661,
                                    "age": 0.5333333333333333,
                                    "gain": -28.427573200000015,
                                    "stcg": -28.427573200000015,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.947,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1055.487,
                                    "buyDate": "2018-07-18",
                                    "originalNav": 1055.487,
                                    "age": 0.45161290322580644,
                                    "gain": 14.272426399999869,
                                    "stcg": 14.272426399999869,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.89,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1124.1176,
                                    "buyDate": "2018-08-20",
                                    "originalNav": 1124.1176,
                                    "age": 0.36290322580645157,
                                    "gain": -47.66786600000009,
                                    "stcg": -47.66786600000009,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.909,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1099.6928,
                                    "buyDate": "2018-09-18",
                                    "originalNav": 1099.6928,
                                    "age": 0.2833333333333333,
                                    "gain": -26.483351400000085,
                                    "stcg": -26.483351400000085,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 1.006,
                                    "sellNav": 1070.5582,
                                    "buyNav": 994.1896,
                                    "buyDate": "2018-10-19",
                                    "originalNav": 994.1896,
                                    "age": 0.19892473118279572,
                                    "gain": 76.8268115999999,
                                    "stcg": 76.8268115999999,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.96,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1041.1885,
                                    "buyDate": "2018-11-19",
                                    "originalNav": 1041.1885,
                                    "age": 0.11388888888888889,
                                    "gain": 28.194911999999967,
                                    "stcg": 28.194911999999967,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.941,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1062.9288,
                                    "buyDate": "2018-12-18",
                                    "originalNav": 1062.9288,
                                    "age": 0.03494623655913979,
                                    "gain": 7.17926540000003,
                                    "stcg": 7.17926540000003,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                }
                            ]
                        },
                        {
                            "sellAmount": "11430.91",
                            "sellDate": "2019-12-03",
                            "sellUnits": 10.197,
                            "sellNav": 1121.0177,
                            "buyQueue": [
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.939,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1064.9694,
                                    "buyDate": "2019-01-18",
                                    "originalNav": 1064.9694,
                                    "age": 0.875,
                                    "gain": 52.629353700000145,
                                    "stcg": 52.629353700000145,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 1.004,
                                    "sellNav": 1121.0177,
                                    "buyNav": 996.1433,
                                    "buyDate": "2019-02-18",
                                    "originalNav": 996.1433,
                                    "age": 0.7916666666666666,
                                    "gain": 125.37389760000015,
                                    "stcg": 125.37389760000015,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.914,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1094.4731,
                                    "buyDate": "2019-03-18",
                                    "originalNav": 1094.4731,
                                    "age": 0.7083333333333334,
                                    "gain": 24.26176440000016,
                                    "stcg": 24.26176440000016,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.888,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1126.1574,
                                    "buyDate": "2019-04-18",
                                    "originalNav": 1126.1574,
                                    "age": 0.625,
                                    "gain": -4.564053599999954,
                                    "stcg": -4.564053599999954,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.898,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1113.6909,
                                    "buyDate": "2019-05-20",
                                    "originalNav": 1113.6909,
                                    "age": 0.5361111111111111,
                                    "gain": 6.579466400000044,
                                    "stcg": 6.579466400000044,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.905,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1104.601,
                                    "buyDate": "2019-06-18",
                                    "originalNav": 1104.601,
                                    "age": 0.4583333333333333,
                                    "gain": 14.857113499999993,
                                    "stcg": 14.857113499999993,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.916,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1092.2942,
                                    "buyDate": "2019-07-18",
                                    "originalNav": 1092.2942,
                                    "age": 0.375,
                                    "gain": 26.310726000000056,
                                    "stcg": 26.310726000000056,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.95,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1052.0783,
                                    "buyDate": "2019-08-19",
                                    "originalNav": 1052.0783,
                                    "age": 0.2888888888888889,
                                    "gain": 65.4924300000002,
                                    "stcg": 65.4924300000002,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.965,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1036.4353,
                                    "buyDate": "2019-09-18",
                                    "originalNav": 1036.4353,
                                    "age": 0.20833333333333334,
                                    "gain": 81.622016,
                                    "stcg": 81.622016,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.92,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1086.6634,
                                    "buyDate": "2019-10-18",
                                    "originalNav": 1086.6634,
                                    "age": 0.125,
                                    "gain": 31.60595600000018,
                                    "stcg": 31.60595600000018,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.898,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1114.1146,
                                    "buyDate": "2019-11-18",
                                    "originalNav": 1114.1146,
                                    "age": 0.041666666666666664,
                                    "gain": 6.198983799999995,
                                    "stcg": 6.198983799999995,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                }
                            ]
                        }
                    ],
                    "cashFlows": [
                        {
                            "amount": -1000,
                            "when": "2019-12-18T00:00:00.000Z"
                        },
                        {
                            "amount": 1966.7035884000002,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 0.887,
                    "totalInvestment": 999.6575152000001,
                    "divReinvested": 0,
                    "assetValue": 1966.7035884000002,
                    "unrealizedCG": 967.0460732,
                    "unrealizedSTCG": 0,
                    "unrealizedLTCG": 967.0460732,
                    "growth": 967.0460732,
                    "flowTotal": 966.7035884000002,
                    "dailyChange": 0,
                    "lockFreeUnits": 0.887,
                    "lockFreeValue": 1966.7035884000002,
                    "unrealizedCGIndexed": 967.0460732,
                    "stcg": 0,
                    "stcgTax": 0,
                    "gfNAV": 1177.8459,
                    "xirrReturns": 0.25735250727496894,
                    "absoluteReturns": 0.9673773852503118
                }
            ],
            "totalUnits": 0.887,
            "totalInvestment": 999.6575152000001,
            "assetValue": 1966.7035884000002,
            "flowTotal": 966.7035884000002,
            "growth": 967.0460732,
            "unrealizedCG": 967.0460732,
            "unrealizedCGIndexed": 967.0460732,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 0,
            "unrealizedLTCG": 967.0460732,
            "dailyChange": 0,
            "lockFreeUnits": 0.887,
            "lockFreeValue": 1966.7035884000002,
            "stcg": 0,
            "stcgTax": 0,
            "cashFlows": [
                {
                    "amount": -1000,
                    "when": "2019-12-18T00:00:00.000Z"
                },
                {
                    "amount": 1966.7035884000002,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.25735250727496894,
            "absoluteReturns": 0.9673773852503118,
            "gfNAV": 1177.8459
        }
    ],
    "cashFlows": [
        {
            "amount": -1000,
            "when": "2019-12-18T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2020-08-10T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2020-08-11T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2020-09-10T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2020-09-11T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2020-10-12T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2020-10-12T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2020-11-10T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2020-11-11T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2020-12-10T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2020-12-11T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-01-11T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2021-01-11T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-02-22T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-02-22T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-02-25T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-03-22T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-03-22T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-03-25T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-04-20T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-04-22T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-04-26T00:00:00.000Z"
        },
        {
            "amount": 6297.2366439,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 12096.2943189,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 3977.61,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 4255.984391999999,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 3854.5151280000005,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 1966.7035884000002,
            "when": "2022-11-30T00:00:00.000Z"
        }
    ],
    "unrealizedSTCG": 395.15299349999924,
    "unrealizedLTCG": 4055.5377332,
    "isXIRRMode": true,
    "unrealizedCGIndexed": 4450.690726699999
}

var combineOutput = {
    "totalDebtInvestment": 35996.7559386,
    "totalDebtValue": 36787.0619256,
    "debtGrowth": 790.3059869999997,
    "totalEquityInvestment": 19998.550750400005,
    "totalEquityValue": 28109.626216799996,
    "equityGrowth": 8111.075466399991,
    "xirrReturns": 0.07662353714113221,
    "totalUnits": 1651.6380000000001,
    "totalInvestment": 55995.306689000005,
    "assetValue": 64896.688142399995,
    "flowTotal": 8899.388142399997,
    "growth": 8901.3814534,
    "unrealizedCg": 0,
    "divPaid": 0,
    "divReinvested": 0,
    "dailyChange": 0,
    "lockFreeUnits": 1651.6380000000001,
    "lockFreeValue": 64896.688142399995,
    "unrealizedCG": 8901.3814534,
    "stcg": 790.3059869999986,
    "stcgTax": 237.09179609999953,
    "absoluteReturns": 0.15896656308784235,
    "debtRatio": 57,
    "unprocessedPurchases": 0,
    "unprocessedRedemptions": 0,
    "unprocessedSwitches": 0,
    "holdings": [
        {
            "isAssetPlus": true,
            "schemeCode": "105669",
            "schemeName": "DSP Strategic Bond Fund - Regular Plan - Growth",
            "nav": "2727.2571",
            "navDate": "2022-11-30",
            "navChange": 0.6027999999996609,
            "lockInPeriod": 0,
            "amfiBroad": "Debt",
            "amfiSub": "Dynamic Bond",
            "legend": "Debt",
            "amcCode": "DSP Mutual Fund",
            "folios": [
                {
                    "folioNo": "6247700/16",
                    "schemeCode": "105669",
                    "schemeName": "",
                    "nav": "2727.2571",
                    "navDate": "2022-11-30",
                    "navChange": 0.6027999999996609,
                    "lockInPeriod": 0,
                    "amfiBroad": "Debt",
                    "amfiSub": "Dynamic Bond",
                    "legend": "Debt",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2020-08-10",
                            "nav": 2574.2126,
                            "units": "0.388",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-09-10",
                            "nav": 2553.6487,
                            "units": "0.392",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-10-12",
                            "nav": 2594.5854,
                            "units": "0.385",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-11-10",
                            "nav": 2607.8755,
                            "units": "0.383",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-12-10",
                            "nav": 2626.4926,
                            "units": "0.381",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-01-11",
                            "nav": 2633.5635,
                            "units": "0.380",
                            "amount": "999.95",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 3880,
                            "nav": 2574.2126,
                            "date": "2020-08-10",
                            "type": "P"
                        },
                        {
                            "units_10000": 3920,
                            "nav": 2553.6487,
                            "date": "2020-09-10",
                            "type": "P"
                        },
                        {
                            "units_10000": 3850,
                            "nav": 2594.5854,
                            "date": "2020-10-12",
                            "type": "P"
                        },
                        {
                            "units_10000": 3830,
                            "nav": 2607.8755,
                            "date": "2020-11-10",
                            "type": "P"
                        },
                        {
                            "units_10000": 3810,
                            "nav": 2626.4926,
                            "date": "2020-12-10",
                            "type": "P"
                        },
                        {
                            "units_10000": 3800,
                            "nav": 2633.5635,
                            "date": "2021-01-11",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [],
                    "cashFlows": [
                        {
                            "amount": -999.95,
                            "when": "2020-08-10T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2020-09-10T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2020-10-12T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2020-11-10T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2020-12-10T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-01-11T00:00:00.000Z"
                        },
                        {
                            "amount": 6297.2366439,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 2.309,
                    "totalInvestment": 5999.0042853,
                    "divReinvested": 0,
                    "assetValue": 6297.2366439,
                    "unrealizedCG": 298.2323585999993,
                    "unrealizedSTCG": 298.23235859999943,
                    "unrealizedLTCG": 0,
                    "growth": 298.2323585999993,
                    "flowTotal": 297.53664389999994,
                    "dailyChange": 0,
                    "lockFreeUnits": 2.309,
                    "lockFreeValue": 6297.2366439,
                    "unrealizedCGIndexed": 298.23235859999943,
                    "stcg": 298.23235859999943,
                    "stcgTax": 89.46970757999982,
                    "gfNAV": 1988.3432,
                    "xirrReturns": 0.023359543575196564,
                    "absoluteReturns": 0.049713643200887496
                }
            ],
            "totalUnits": 2.309,
            "totalInvestment": 5999.0042853,
            "assetValue": 6297.2366439,
            "flowTotal": 297.53664389999994,
            "growth": 298.2323585999993,
            "unrealizedCG": 298.2323585999993,
            "unrealizedCGIndexed": 298.23235859999943,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 298.23235859999943,
            "unrealizedLTCG": 0,
            "dailyChange": 0,
            "lockFreeUnits": 2.309,
            "lockFreeValue": 6297.2366439,
            "stcg": 298.23235859999943,
            "stcgTax": 89.46970757999982,
            "cashFlows": [
                {
                    "amount": -999.95,
                    "when": "2020-08-10T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2020-09-10T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2020-10-12T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2020-11-10T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2020-12-10T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-01-11T00:00:00.000Z"
                },
                {
                    "amount": 6297.2366439,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.023359543575196564,
            "absoluteReturns": 0.049713643200887496,
            "gfNAV": 1988.3432
        },
        {
            "isAssetPlus": true,
            "schemeCode": "115833",
            "schemeName": "ICICI Prudential Regular Gold Savings Fund (FOF) - Growth",
            "nav": "16.9317",
            "navDate": "2022-11-30",
            "navChange": 0.0033999999999991815,
            "lockInPeriod": 0,
            "amfiBroad": "Other",
            "amfiSub": "FoF Domestic",
            "legend": "Debt",
            "amcCode": "ICICI Prudential Mutual Fund",
            "folios": [
                {
                    "folioNo": "14587144/21",
                    "schemeCode": "115833",
                    "schemeName": "",
                    "nav": "16.9317",
                    "navDate": "2022-11-30",
                    "navChange": 0.0033999999999991815,
                    "lockInPeriod": 0,
                    "amfiBroad": "Other",
                    "amfiSub": "FoF Domestic",
                    "legend": "Debt",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2020-08-11",
                            "nav": 17.7987,
                            "units": "112.362",
                            "amount": "1999.90",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-09-11",
                            "nav": 17.0788,
                            "units": "117.098",
                            "amount": "1999.90",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-10-12",
                            "nav": 16.8361,
                            "units": "118.786",
                            "amount": "1999.90",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-11-11",
                            "nav": 16.669,
                            "units": "119.977",
                            "amount": "1999.90",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-12-11",
                            "nav": 16.1911,
                            "units": "123.518",
                            "amount": "1999.90",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-01-11",
                            "nav": 16.3023,
                            "units": "122.676",
                            "amount": "1999.90",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 1123620,
                            "nav": 17.7987,
                            "date": "2020-08-11",
                            "type": "P"
                        },
                        {
                            "units_10000": 1170980,
                            "nav": 17.0788,
                            "date": "2020-09-11",
                            "type": "P"
                        },
                        {
                            "units_10000": 1187860,
                            "nav": 16.8361,
                            "date": "2020-10-12",
                            "type": "P"
                        },
                        {
                            "units_10000": 1199770,
                            "nav": 16.669,
                            "date": "2020-11-11",
                            "type": "P"
                        },
                        {
                            "units_10000": 1235180,
                            "nav": 16.1911,
                            "date": "2020-12-11",
                            "type": "P"
                        },
                        {
                            "units_10000": 1226760,
                            "nav": 16.3023,
                            "date": "2021-01-11",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [],
                    "cashFlows": [
                        {
                            "amount": -1999.9,
                            "when": "2020-08-11T00:00:00.000Z"
                        },
                        {
                            "amount": -1999.9,
                            "when": "2020-09-11T00:00:00.000Z"
                        },
                        {
                            "amount": -1999.9,
                            "when": "2020-10-12T00:00:00.000Z"
                        },
                        {
                            "amount": -1999.9,
                            "when": "2020-11-11T00:00:00.000Z"
                        },
                        {
                            "amount": -1999.9,
                            "when": "2020-12-11T00:00:00.000Z"
                        },
                        {
                            "amount": -1999.9,
                            "when": "2021-01-11T00:00:00.000Z"
                        },
                        {
                            "amount": 12096.2943189,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 714.417,
                    "totalInvestment": 11999.373683999998,
                    "divReinvested": 0,
                    "assetValue": 12096.2943189,
                    "unrealizedCG": 96.92063490000146,
                    "unrealizedSTCG": 96.92063489999981,
                    "unrealizedLTCG": 0,
                    "growth": 96.92063490000146,
                    "flowTotal": 96.89431890000014,
                    "dailyChange": 0,
                    "lockFreeUnits": 714.417,
                    "lockFreeValue": 12096.2943189,
                    "unrealizedCGIndexed": 96.92063489999981,
                    "stcg": 96.92063489999981,
                    "stcgTax": 29.076190469999943,
                    "gfNAV": 10.2807,
                    "xirrReturns": 0.0038479319488984353,
                    "absoluteReturns": 0.008077141145227916
                }
            ],
            "totalUnits": 714.417,
            "totalInvestment": 11999.373683999998,
            "assetValue": 12096.2943189,
            "flowTotal": 96.89431890000014,
            "growth": 96.92063490000146,
            "unrealizedCG": 96.92063490000146,
            "unrealizedCGIndexed": 96.92063489999981,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 96.92063489999981,
            "unrealizedLTCG": 0,
            "dailyChange": 0,
            "lockFreeUnits": 714.417,
            "lockFreeValue": 12096.2943189,
            "stcg": 96.92063489999981,
            "stcgTax": 29.076190469999943,
            "cashFlows": [
                {
                    "amount": -1999.9,
                    "when": "2020-08-11T00:00:00.000Z"
                },
                {
                    "amount": -1999.9,
                    "when": "2020-09-11T00:00:00.000Z"
                },
                {
                    "amount": -1999.9,
                    "when": "2020-10-12T00:00:00.000Z"
                },
                {
                    "amount": -1999.9,
                    "when": "2020-11-11T00:00:00.000Z"
                },
                {
                    "amount": -1999.9,
                    "when": "2020-12-11T00:00:00.000Z"
                },
                {
                    "amount": -1999.9,
                    "when": "2021-01-11T00:00:00.000Z"
                },
                {
                    "amount": 12096.2943189,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.0038479319488984353,
            "absoluteReturns": 0.008077141145227916,
            "gfNAV": 10.2807
        },
        {
            "isAssetPlus": true,
            "schemeCode": "105503",
            "schemeName": "Invesco India Midcap Fund - Growth Option",
            "nav": "91",
            "navDate": "2022-11-30",
            "navChange": 0.5699999999999932,
            "lockInPeriod": 0,
            "amfiBroad": "Equity",
            "amfiSub": "Mid Cap",
            "legend": "Equity",
            "amcCode": "Invesco Mutual Fund",
            "folios": [
                {
                    "folioNo": "3108417116",
                    "schemeCode": "105503",
                    "schemeName": "",
                    "nav": "91",
                    "navDate": "2022-11-30",
                    "navChange": 0.5699999999999932,
                    "lockInPeriod": 0,
                    "amfiBroad": "Equity",
                    "amfiSub": "Mid Cap",
                    "legend": "Equity",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2021-02-25",
                            "nav": 69.88,
                            "units": "14.310",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-03-25",
                            "nav": 67.83,
                            "units": "14.742",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-04-26",
                            "nav": 68.22,
                            "units": "14.658",
                            "amount": "999.95",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 143100,
                            "nav": 69.88,
                            "date": "2021-02-25",
                            "type": "P"
                        },
                        {
                            "units_10000": 147420,
                            "nav": 67.83,
                            "date": "2021-03-25",
                            "type": "P"
                        },
                        {
                            "units_10000": 146580,
                            "nav": 68.22,
                            "date": "2021-04-26",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [],
                    "cashFlows": [
                        {
                            "amount": -999.95,
                            "when": "2021-02-25T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-03-25T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-04-26T00:00:00.000Z"
                        },
                        {
                            "amount": 3977.61,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 43.71,
                    "totalInvestment": 2999.90142,
                    "divReinvested": 0,
                    "assetValue": 3977.61,
                    "unrealizedCG": 977.70858,
                    "unrealizedSTCG": 0,
                    "unrealizedLTCG": 977.70858,
                    "growth": 977.70858,
                    "flowTotal": 977.7599999999998,
                    "dailyChange": 0,
                    "lockFreeUnits": 43.71,
                    "lockFreeValue": 3977.61,
                    "unrealizedCGIndexed": 977.70858,
                    "stcg": 0,
                    "stcgTax": 0,
                    "gfNAV": 49.71,
                    "xirrReturns": 0.18265563228420076,
                    "absoluteReturns": 0.3259135695198944
                }
            ],
            "totalUnits": 43.71,
            "totalInvestment": 2999.90142,
            "assetValue": 3977.61,
            "flowTotal": 977.7599999999998,
            "growth": 977.70858,
            "unrealizedCG": 977.70858,
            "unrealizedCGIndexed": 977.70858,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 0,
            "unrealizedLTCG": 977.70858,
            "dailyChange": 0,
            "lockFreeUnits": 43.71,
            "lockFreeValue": 3977.61,
            "stcg": 0,
            "stcgTax": 0,
            "cashFlows": [
                {
                    "amount": -999.95,
                    "when": "2021-02-25T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-03-25T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-04-26T00:00:00.000Z"
                },
                {
                    "amount": 3977.61,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.18265563228420076,
            "absoluteReturns": 0.3259135695198944,
            "gfNAV": 49.71
        },
        {
            "isAssetPlus": true,
            "schemeCode": "102875",
            "schemeName": "Kotak - Small Cap Fund - Growth",
            "nav": "165.063",
            "navDate": "2022-11-30",
            "navChange": 0.42499999999998295,
            "lockInPeriod": 0,
            "amfiBroad": "Equity",
            "amfiSub": "Small Cap",
            "legend": "Equity",
            "amcCode": "Kotak Mahindra Mutual Fund",
            "folios": [
                {
                    "folioNo": "8016916/14",
                    "schemeCode": "102875",
                    "schemeName": "",
                    "nav": "165.063",
                    "navDate": "2022-11-30",
                    "navChange": 0.42499999999998295,
                    "lockInPeriod": 0,
                    "amfiBroad": "Equity",
                    "amfiSub": "Small Cap",
                    "legend": "Equity",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2021-02-22",
                            "nav": 112.748,
                            "units": "8.869",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-03-22",
                            "nav": 117.913,
                            "units": "8.480",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-04-22",
                            "nav": 118.552,
                            "units": "8.435",
                            "amount": "999.95",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 88690,
                            "nav": 112.748,
                            "date": "2021-02-22",
                            "type": "P"
                        },
                        {
                            "units_10000": 84800,
                            "nav": 117.913,
                            "date": "2021-03-22",
                            "type": "P"
                        },
                        {
                            "units_10000": 84350,
                            "nav": 118.552,
                            "date": "2021-04-22",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [],
                    "cashFlows": [
                        {
                            "amount": -999.95,
                            "when": "2021-02-22T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-03-22T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-04-22T00:00:00.000Z"
                        },
                        {
                            "amount": 4255.984391999999,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 25.784,
                    "totalInvestment": 2999.8503720000003,
                    "divReinvested": 0,
                    "assetValue": 4255.984391999999,
                    "unrealizedCG": 1256.134019999999,
                    "unrealizedSTCG": 0,
                    "unrealizedLTCG": 1256.1340199999995,
                    "growth": 1256.134019999999,
                    "flowTotal": 1256.134391999999,
                    "dailyChange": 0,
                    "lockFreeUnits": 25.784,
                    "lockFreeValue": 4255.984391999999,
                    "unrealizedCGIndexed": 1256.1340199999995,
                    "stcg": 0,
                    "stcgTax": 0,
                    "gfNAV": 81.696,
                    "xirrReturns": 0.22980468674174015,
                    "absoluteReturns": 0.4187322246884382
                }
            ],
            "totalUnits": 25.784,
            "totalInvestment": 2999.8503720000003,
            "assetValue": 4255.984391999999,
            "flowTotal": 1256.134391999999,
            "growth": 1256.134019999999,
            "unrealizedCG": 1256.134019999999,
            "unrealizedCGIndexed": 1256.1340199999995,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 0,
            "unrealizedLTCG": 1256.1340199999995,
            "dailyChange": 0,
            "lockFreeUnits": 25.784,
            "lockFreeValue": 4255.984391999999,
            "stcg": 0,
            "stcgTax": 0,
            "cashFlows": [
                {
                    "amount": -999.95,
                    "when": "2021-02-22T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-03-22T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-04-22T00:00:00.000Z"
                },
                {
                    "amount": 4255.984391999999,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.22980468674174015,
            "absoluteReturns": 0.4187322246884382,
            "gfNAV": 81.696
        },
        {
            "isAssetPlus": true,
            "schemeCode": "112932",
            "schemeName": "Mirae Asset Emerging Bluechip Fund - Regular Plan - Growth Option",
            "nav": "99.569",
            "navDate": "2022-11-30",
            "navChange": 0.4759999999999991,
            "lockInPeriod": 0,
            "amfiBroad": "Equity",
            "amfiSub": "Large & Mid Cap",
            "legend": "Equity",
            "amcCode": "Mirae Asset Mutual Fund",
            "folios": [
                {
                    "folioNo": "79936310696",
                    "schemeCode": "112932",
                    "schemeName": "",
                    "nav": "99.569",
                    "navDate": "2022-11-30",
                    "navChange": 0.4759999999999991,
                    "lockInPeriod": 0,
                    "amfiBroad": "Equity",
                    "amfiSub": "Large & Mid Cap",
                    "legend": "Equity",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2021-02-22",
                            "nav": 77.157,
                            "units": "12.960",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-03-22",
                            "nav": 77.949,
                            "units": "12.828",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-04-20",
                            "nav": 77.374,
                            "units": "12.924",
                            "amount": "999.95",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 129600,
                            "nav": 77.157,
                            "date": "2021-02-22",
                            "type": "P"
                        },
                        {
                            "units_10000": 128280,
                            "nav": 77.949,
                            "date": "2021-03-22",
                            "type": "P"
                        },
                        {
                            "units_10000": 129240,
                            "nav": 77.374,
                            "date": "2021-04-20",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [],
                    "cashFlows": [
                        {
                            "amount": -999.95,
                            "when": "2021-02-22T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-03-22T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-04-20T00:00:00.000Z"
                        },
                        {
                            "amount": 3854.5151280000005,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 38.712,
                    "totalInvestment": 2999.8660680000003,
                    "divReinvested": 0,
                    "assetValue": 3854.5151280000005,
                    "unrealizedCG": 854.6490600000002,
                    "unrealizedSTCG": 0,
                    "unrealizedLTCG": 854.6490600000002,
                    "growth": 854.6490600000002,
                    "flowTotal": 854.6651280000001,
                    "dailyChange": 0,
                    "lockFreeUnits": 38.712,
                    "lockFreeValue": 3854.5151280000005,
                    "unrealizedCGIndexed": 854.6490600000002,
                    "stcg": 0,
                    "stcgTax": 0,
                    "gfNAV": 52.168,
                    "xirrReturns": 0.15964117890369958,
                    "absoluteReturns": 0.28489573888536684
                }
            ],
            "totalUnits": 38.712,
            "totalInvestment": 2999.8660680000003,
            "assetValue": 3854.5151280000005,
            "flowTotal": 854.6651280000001,
            "growth": 854.6490600000002,
            "unrealizedCG": 854.6490600000002,
            "unrealizedCGIndexed": 854.6490600000002,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 0,
            "unrealizedLTCG": 854.6490600000002,
            "dailyChange": 0,
            "lockFreeUnits": 38.712,
            "lockFreeValue": 3854.5151280000005,
            "stcg": 0,
            "stcgTax": 0,
            "cashFlows": [
                {
                    "amount": -999.95,
                    "when": "2021-02-22T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-03-22T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-04-20T00:00:00.000Z"
                },
                {
                    "amount": 3854.5151280000005,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.15964117890369958,
            "absoluteReturns": 0.28489573888536684,
            "gfNAV": 52.168
        },
        {
            "isAssetPlus": true,
            "schemeCode": "100377",
            "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
            "nav": "2217.2532",
            "navDate": "2022-11-30",
            "navChange": 28.158600000000206,
            "lockInPeriod": 0,
            "amfiBroad": "Equity",
            "amfiSub": "Mid Cap",
            "legend": "Equity",
            "amcCode": "Nippon India Mutual Fund",
            "folios": [
                {
                    "folioNo": "401185465892",
                    "schemeCode": "100377",
                    "schemeName": "",
                    "nav": "2217.2532",
                    "navDate": "2022-11-30",
                    "navChange": 28.158600000000206,
                    "lockInPeriod": 0,
                    "amfiBroad": "Equity",
                    "amfiSub": "Mid Cap",
                    "legend": "Equity",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2017-12-21",
                            "nav": 1183.874,
                            "units": "0.845",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-01-18",
                            "nav": 1195.4158,
                            "units": "0.837",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-02-19",
                            "nav": 1124.0935,
                            "units": "0.890",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-03-19",
                            "nav": 1084.7599,
                            "units": "0.922",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-04-18",
                            "nav": 1134.8436,
                            "units": "0.881",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-05-18",
                            "nav": 1101.6595,
                            "units": "0.908",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-06-18",
                            "nav": 1101.8661,
                            "units": "0.908",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-07-18",
                            "nav": 1055.487,
                            "units": "0.947",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-08-20",
                            "nav": 1124.1176,
                            "units": "0.890",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-09-18",
                            "nav": 1099.6928,
                            "units": "0.909",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-10-19",
                            "nav": 994.1896,
                            "units": "1.006",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-11-19",
                            "nav": 1041.1885,
                            "units": "0.960",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-12-18",
                            "nav": 1062.9288,
                            "units": "0.941",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "SO",
                            "navDate": "2018-12-31",
                            "nav": 1070.5582,
                            "units": "11.844",
                            "amount": "12679.56",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-01-18",
                            "nav": 1064.9694,
                            "units": "0.939",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-02-18",
                            "nav": 996.1433,
                            "units": "1.004",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-03-18",
                            "nav": 1094.4731,
                            "units": "0.914",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-04-18",
                            "nav": 1126.1574,
                            "units": "0.888",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-05-20",
                            "nav": 1113.6909,
                            "units": "0.898",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-06-18",
                            "nav": 1104.601,
                            "units": "0.905",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-07-18",
                            "nav": 1092.2942,
                            "units": "0.916",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-08-19",
                            "nav": 1052.0783,
                            "units": "0.950",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-09-18",
                            "nav": 1036.4353,
                            "units": "0.965",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-10-18",
                            "nav": 1086.6634,
                            "units": "0.920",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-11-18",
                            "nav": 1114.1146,
                            "units": "0.898",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "R",
                            "navDate": "2019-12-03",
                            "nav": 1121.0177,
                            "units": "10.197",
                            "amount": "11430.91",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-12-18",
                            "nav": 1127.0096,
                            "units": "0.887",
                            "amount": "1000.00",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 8870,
                            "nav": 1127.0096,
                            "date": "2019-12-18",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [
                        {
                            "sellAmount": "12679.56",
                            "sellDate": "2018-12-31",
                            "sellUnits": 11.844,
                            "sellNav": 1070.5582,
                            "buyQueue": [
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.845,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1183.874,
                                    "buyDate": "2017-12-21",
                                    "originalNav": 1183.874,
                                    "age": 1.0268817204301075,
                                    "gain": -95.75185100000006,
                                    "stcg": 0,
                                    "ltcg": -95.75185100000006,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.837,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1195.4158,
                                    "buyDate": "2018-01-18",
                                    "originalNav": 1195.4158,
                                    "age": 0.9516129032258065,
                                    "gain": -104.50581120000004,
                                    "stcg": -104.50581120000004,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.89,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1124.0935,
                                    "buyDate": "2018-02-19",
                                    "originalNav": 1124.0935,
                                    "age": 0.8601190476190476,
                                    "gain": -47.64641700000001,
                                    "stcg": -47.64641700000001,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.922,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1084.7599,
                                    "buyDate": "2018-03-19",
                                    "originalNav": 1084.7599,
                                    "age": 0.782258064516129,
                                    "gain": -13.093967400000068,
                                    "stcg": -13.093967400000068,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.881,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1134.8436,
                                    "buyDate": "2018-04-18",
                                    "originalNav": 1134.8436,
                                    "age": 0.7000000000000001,
                                    "gain": -56.63543739999999,
                                    "stcg": -56.63543739999999,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.908,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1101.6595,
                                    "buyDate": "2018-05-18",
                                    "originalNav": 1101.6595,
                                    "age": 0.6182795698924731,
                                    "gain": -28.239980400000036,
                                    "stcg": -28.239980400000036,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.908,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1101.8661,
                                    "buyDate": "2018-06-18",
                                    "originalNav": 1101.8661,
                                    "age": 0.5333333333333333,
                                    "gain": -28.427573200000015,
                                    "stcg": -28.427573200000015,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.947,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1055.487,
                                    "buyDate": "2018-07-18",
                                    "originalNav": 1055.487,
                                    "age": 0.45161290322580644,
                                    "gain": 14.272426399999869,
                                    "stcg": 14.272426399999869,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.89,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1124.1176,
                                    "buyDate": "2018-08-20",
                                    "originalNav": 1124.1176,
                                    "age": 0.36290322580645157,
                                    "gain": -47.66786600000009,
                                    "stcg": -47.66786600000009,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.909,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1099.6928,
                                    "buyDate": "2018-09-18",
                                    "originalNav": 1099.6928,
                                    "age": 0.2833333333333333,
                                    "gain": -26.483351400000085,
                                    "stcg": -26.483351400000085,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 1.006,
                                    "sellNav": 1070.5582,
                                    "buyNav": 994.1896,
                                    "buyDate": "2018-10-19",
                                    "originalNav": 994.1896,
                                    "age": 0.19892473118279572,
                                    "gain": 76.8268115999999,
                                    "stcg": 76.8268115999999,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.96,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1041.1885,
                                    "buyDate": "2018-11-19",
                                    "originalNav": 1041.1885,
                                    "age": 0.11388888888888889,
                                    "gain": 28.194911999999967,
                                    "stcg": 28.194911999999967,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.941,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1062.9288,
                                    "buyDate": "2018-12-18",
                                    "originalNav": 1062.9288,
                                    "age": 0.03494623655913979,
                                    "gain": 7.17926540000003,
                                    "stcg": 7.17926540000003,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                }
                            ]
                        },
                        {
                            "sellAmount": "11430.91",
                            "sellDate": "2019-12-03",
                            "sellUnits": 10.197,
                            "sellNav": 1121.0177,
                            "buyQueue": [
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.939,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1064.9694,
                                    "buyDate": "2019-01-18",
                                    "originalNav": 1064.9694,
                                    "age": 0.875,
                                    "gain": 52.629353700000145,
                                    "stcg": 52.629353700000145,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 1.004,
                                    "sellNav": 1121.0177,
                                    "buyNav": 996.1433,
                                    "buyDate": "2019-02-18",
                                    "originalNav": 996.1433,
                                    "age": 0.7916666666666666,
                                    "gain": 125.37389760000015,
                                    "stcg": 125.37389760000015,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.914,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1094.4731,
                                    "buyDate": "2019-03-18",
                                    "originalNav": 1094.4731,
                                    "age": 0.7083333333333334,
                                    "gain": 24.26176440000016,
                                    "stcg": 24.26176440000016,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.888,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1126.1574,
                                    "buyDate": "2019-04-18",
                                    "originalNav": 1126.1574,
                                    "age": 0.625,
                                    "gain": -4.564053599999954,
                                    "stcg": -4.564053599999954,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.898,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1113.6909,
                                    "buyDate": "2019-05-20",
                                    "originalNav": 1113.6909,
                                    "age": 0.5361111111111111,
                                    "gain": 6.579466400000044,
                                    "stcg": 6.579466400000044,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.905,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1104.601,
                                    "buyDate": "2019-06-18",
                                    "originalNav": 1104.601,
                                    "age": 0.4583333333333333,
                                    "gain": 14.857113499999993,
                                    "stcg": 14.857113499999993,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.916,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1092.2942,
                                    "buyDate": "2019-07-18",
                                    "originalNav": 1092.2942,
                                    "age": 0.375,
                                    "gain": 26.310726000000056,
                                    "stcg": 26.310726000000056,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.95,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1052.0783,
                                    "buyDate": "2019-08-19",
                                    "originalNav": 1052.0783,
                                    "age": 0.2888888888888889,
                                    "gain": 65.4924300000002,
                                    "stcg": 65.4924300000002,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.965,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1036.4353,
                                    "buyDate": "2019-09-18",
                                    "originalNav": 1036.4353,
                                    "age": 0.20833333333333334,
                                    "gain": 81.622016,
                                    "stcg": 81.622016,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.92,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1086.6634,
                                    "buyDate": "2019-10-18",
                                    "originalNav": 1086.6634,
                                    "age": 0.125,
                                    "gain": 31.60595600000018,
                                    "stcg": 31.60595600000018,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.898,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1114.1146,
                                    "buyDate": "2019-11-18",
                                    "originalNav": 1114.1146,
                                    "age": 0.041666666666666664,
                                    "gain": 6.198983799999995,
                                    "stcg": 6.198983799999995,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                }
                            ]
                        }
                    ],
                    "cashFlows": [
                        {
                            "amount": -1000,
                            "when": "2019-12-18T00:00:00.000Z"
                        },
                        {
                            "amount": 1966.7035884000002,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 0.887,
                    "totalInvestment": 999.6575152000001,
                    "divReinvested": 0,
                    "assetValue": 1966.7035884000002,
                    "unrealizedCG": 967.0460732,
                    "unrealizedSTCG": 0,
                    "unrealizedLTCG": 967.0460732,
                    "growth": 967.0460732,
                    "flowTotal": 966.7035884000002,
                    "dailyChange": 0,
                    "lockFreeUnits": 0.887,
                    "lockFreeValue": 1966.7035884000002,
                    "unrealizedCGIndexed": 967.0460732,
                    "stcg": 0,
                    "stcgTax": 0,
                    "gfNAV": 1177.8459,
                    "xirrReturns": 0.25735250727496894,
                    "absoluteReturns": 0.9673773852503118
                }
            ],
            "totalUnits": 0.887,
            "totalInvestment": 999.6575152000001,
            "assetValue": 1966.7035884000002,
            "flowTotal": 966.7035884000002,
            "growth": 967.0460732,
            "unrealizedCG": 967.0460732,
            "unrealizedCGIndexed": 967.0460732,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 0,
            "unrealizedLTCG": 967.0460732,
            "dailyChange": 0,
            "lockFreeUnits": 0.887,
            "lockFreeValue": 1966.7035884000002,
            "stcg": 0,
            "stcgTax": 0,
            "cashFlows": [
                {
                    "amount": -1000,
                    "when": "2019-12-18T00:00:00.000Z"
                },
                {
                    "amount": 1966.7035884000002,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.25735250727496894,
            "absoluteReturns": 0.9673773852503118,
            "gfNAV": 1177.8459
        },
        {
            "isAssetPlus": true,
            "schemeCode": "105669",
            "schemeName": "DSP Strategic Bond Fund - Regular Plan - Growth",
            "nav": "2727.2571",
            "navDate": "2022-11-30",
            "navChange": 0.6027999999996609,
            "lockInPeriod": 0,
            "amfiBroad": "Debt",
            "amfiSub": "Dynamic Bond",
            "legend": "Debt",
            "amcCode": "DSP Mutual Fund",
            "folios": [
                {
                    "folioNo": "6247700/16",
                    "schemeCode": "105669",
                    "schemeName": "",
                    "nav": "2727.2571",
                    "navDate": "2022-11-30",
                    "navChange": 0.6027999999996609,
                    "lockInPeriod": 0,
                    "amfiBroad": "Debt",
                    "amfiSub": "Dynamic Bond",
                    "legend": "Debt",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2020-08-10",
                            "nav": 2574.2126,
                            "units": "0.388",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-09-10",
                            "nav": 2553.6487,
                            "units": "0.392",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-10-12",
                            "nav": 2594.5854,
                            "units": "0.385",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-11-10",
                            "nav": 2607.8755,
                            "units": "0.383",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-12-10",
                            "nav": 2626.4926,
                            "units": "0.381",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-01-11",
                            "nav": 2633.5635,
                            "units": "0.380",
                            "amount": "999.95",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 3880,
                            "nav": 2574.2126,
                            "date": "2020-08-10",
                            "type": "P"
                        },
                        {
                            "units_10000": 3920,
                            "nav": 2553.6487,
                            "date": "2020-09-10",
                            "type": "P"
                        },
                        {
                            "units_10000": 3850,
                            "nav": 2594.5854,
                            "date": "2020-10-12",
                            "type": "P"
                        },
                        {
                            "units_10000": 3830,
                            "nav": 2607.8755,
                            "date": "2020-11-10",
                            "type": "P"
                        },
                        {
                            "units_10000": 3810,
                            "nav": 2626.4926,
                            "date": "2020-12-10",
                            "type": "P"
                        },
                        {
                            "units_10000": 3800,
                            "nav": 2633.5635,
                            "date": "2021-01-11",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [],
                    "cashFlows": [
                        {
                            "amount": -999.95,
                            "when": "2020-08-10T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2020-09-10T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2020-10-12T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2020-11-10T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2020-12-10T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-01-11T00:00:00.000Z"
                        },
                        {
                            "amount": 6297.2366439,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 2.309,
                    "totalInvestment": 5999.0042853,
                    "divReinvested": 0,
                    "assetValue": 6297.2366439,
                    "unrealizedCG": 298.2323585999993,
                    "unrealizedSTCG": 298.23235859999943,
                    "unrealizedLTCG": 0,
                    "growth": 298.2323585999993,
                    "flowTotal": 297.53664389999994,
                    "dailyChange": 0,
                    "lockFreeUnits": 2.309,
                    "lockFreeValue": 6297.2366439,
                    "unrealizedCGIndexed": 298.23235859999943,
                    "stcg": 298.23235859999943,
                    "stcgTax": 89.46970757999982,
                    "gfNAV": 1988.3432,
                    "xirrReturns": 0.023359543575196564,
                    "absoluteReturns": 0.049713643200887496
                }
            ],
            "totalUnits": 2.309,
            "totalInvestment": 5999.0042853,
            "assetValue": 6297.2366439,
            "flowTotal": 297.53664389999994,
            "growth": 298.2323585999993,
            "unrealizedCG": 298.2323585999993,
            "unrealizedCGIndexed": 298.23235859999943,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 298.23235859999943,
            "unrealizedLTCG": 0,
            "dailyChange": 0,
            "lockFreeUnits": 2.309,
            "lockFreeValue": 6297.2366439,
            "stcg": 298.23235859999943,
            "stcgTax": 89.46970757999982,
            "cashFlows": [
                {
                    "amount": -999.95,
                    "when": "2020-08-10T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2020-09-10T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2020-10-12T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2020-11-10T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2020-12-10T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-01-11T00:00:00.000Z"
                },
                {
                    "amount": 6297.2366439,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.023359543575196564,
            "absoluteReturns": 0.049713643200887496,
            "gfNAV": 1988.3432
        },
        {
            "isAssetPlus": true,
            "schemeCode": "115833",
            "schemeName": "ICICI Prudential Regular Gold Savings Fund (FOF) - Growth",
            "nav": "16.9317",
            "navDate": "2022-11-30",
            "navChange": 0.0033999999999991815,
            "lockInPeriod": 0,
            "amfiBroad": "Other",
            "amfiSub": "FoF Domestic",
            "legend": "Debt",
            "amcCode": "ICICI Prudential Mutual Fund",
            "folios": [
                {
                    "folioNo": "14587144/21",
                    "schemeCode": "115833",
                    "schemeName": "",
                    "nav": "16.9317",
                    "navDate": "2022-11-30",
                    "navChange": 0.0033999999999991815,
                    "lockInPeriod": 0,
                    "amfiBroad": "Other",
                    "amfiSub": "FoF Domestic",
                    "legend": "Debt",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2020-08-11",
                            "nav": 17.7987,
                            "units": "112.362",
                            "amount": "1999.90",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-09-11",
                            "nav": 17.0788,
                            "units": "117.098",
                            "amount": "1999.90",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-10-12",
                            "nav": 16.8361,
                            "units": "118.786",
                            "amount": "1999.90",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-11-11",
                            "nav": 16.669,
                            "units": "119.977",
                            "amount": "1999.90",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2020-12-11",
                            "nav": 16.1911,
                            "units": "123.518",
                            "amount": "1999.90",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-01-11",
                            "nav": 16.3023,
                            "units": "122.676",
                            "amount": "1999.90",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 1123620,
                            "nav": 17.7987,
                            "date": "2020-08-11",
                            "type": "P"
                        },
                        {
                            "units_10000": 1170980,
                            "nav": 17.0788,
                            "date": "2020-09-11",
                            "type": "P"
                        },
                        {
                            "units_10000": 1187860,
                            "nav": 16.8361,
                            "date": "2020-10-12",
                            "type": "P"
                        },
                        {
                            "units_10000": 1199770,
                            "nav": 16.669,
                            "date": "2020-11-11",
                            "type": "P"
                        },
                        {
                            "units_10000": 1235180,
                            "nav": 16.1911,
                            "date": "2020-12-11",
                            "type": "P"
                        },
                        {
                            "units_10000": 1226760,
                            "nav": 16.3023,
                            "date": "2021-01-11",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [],
                    "cashFlows": [
                        {
                            "amount": -1999.9,
                            "when": "2020-08-11T00:00:00.000Z"
                        },
                        {
                            "amount": -1999.9,
                            "when": "2020-09-11T00:00:00.000Z"
                        },
                        {
                            "amount": -1999.9,
                            "when": "2020-10-12T00:00:00.000Z"
                        },
                        {
                            "amount": -1999.9,
                            "when": "2020-11-11T00:00:00.000Z"
                        },
                        {
                            "amount": -1999.9,
                            "when": "2020-12-11T00:00:00.000Z"
                        },
                        {
                            "amount": -1999.9,
                            "when": "2021-01-11T00:00:00.000Z"
                        },
                        {
                            "amount": 12096.2943189,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 714.417,
                    "totalInvestment": 11999.373683999998,
                    "divReinvested": 0,
                    "assetValue": 12096.2943189,
                    "unrealizedCG": 96.92063490000146,
                    "unrealizedSTCG": 96.92063489999981,
                    "unrealizedLTCG": 0,
                    "growth": 96.92063490000146,
                    "flowTotal": 96.89431890000014,
                    "dailyChange": 0,
                    "lockFreeUnits": 714.417,
                    "lockFreeValue": 12096.2943189,
                    "unrealizedCGIndexed": 96.92063489999981,
                    "stcg": 96.92063489999981,
                    "stcgTax": 29.076190469999943,
                    "gfNAV": 10.2807,
                    "xirrReturns": 0.0038479319488984353,
                    "absoluteReturns": 0.008077141145227916
                }
            ],
            "totalUnits": 714.417,
            "totalInvestment": 11999.373683999998,
            "assetValue": 12096.2943189,
            "flowTotal": 96.89431890000014,
            "growth": 96.92063490000146,
            "unrealizedCG": 96.92063490000146,
            "unrealizedCGIndexed": 96.92063489999981,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 96.92063489999981,
            "unrealizedLTCG": 0,
            "dailyChange": 0,
            "lockFreeUnits": 714.417,
            "lockFreeValue": 12096.2943189,
            "stcg": 96.92063489999981,
            "stcgTax": 29.076190469999943,
            "cashFlows": [
                {
                    "amount": -1999.9,
                    "when": "2020-08-11T00:00:00.000Z"
                },
                {
                    "amount": -1999.9,
                    "when": "2020-09-11T00:00:00.000Z"
                },
                {
                    "amount": -1999.9,
                    "when": "2020-10-12T00:00:00.000Z"
                },
                {
                    "amount": -1999.9,
                    "when": "2020-11-11T00:00:00.000Z"
                },
                {
                    "amount": -1999.9,
                    "when": "2020-12-11T00:00:00.000Z"
                },
                {
                    "amount": -1999.9,
                    "when": "2021-01-11T00:00:00.000Z"
                },
                {
                    "amount": 12096.2943189,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.0038479319488984353,
            "absoluteReturns": 0.008077141145227916,
            "gfNAV": 10.2807
        },
        {
            "isAssetPlus": true,
            "schemeCode": "105503",
            "schemeName": "Invesco India Midcap Fund - Growth Option",
            "nav": "91",
            "navDate": "2022-11-30",
            "navChange": 0.5699999999999932,
            "lockInPeriod": 0,
            "amfiBroad": "Equity",
            "amfiSub": "Mid Cap",
            "legend": "Equity",
            "amcCode": "Invesco Mutual Fund",
            "folios": [
                {
                    "folioNo": "3108417116",
                    "schemeCode": "105503",
                    "schemeName": "",
                    "nav": "91",
                    "navDate": "2022-11-30",
                    "navChange": 0.5699999999999932,
                    "lockInPeriod": 0,
                    "amfiBroad": "Equity",
                    "amfiSub": "Mid Cap",
                    "legend": "Equity",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2021-02-25",
                            "nav": 69.88,
                            "units": "14.310",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-03-25",
                            "nav": 67.83,
                            "units": "14.742",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-04-26",
                            "nav": 68.22,
                            "units": "14.658",
                            "amount": "999.95",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 143100,
                            "nav": 69.88,
                            "date": "2021-02-25",
                            "type": "P"
                        },
                        {
                            "units_10000": 147420,
                            "nav": 67.83,
                            "date": "2021-03-25",
                            "type": "P"
                        },
                        {
                            "units_10000": 146580,
                            "nav": 68.22,
                            "date": "2021-04-26",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [],
                    "cashFlows": [
                        {
                            "amount": -999.95,
                            "when": "2021-02-25T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-03-25T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-04-26T00:00:00.000Z"
                        },
                        {
                            "amount": 3977.61,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 43.71,
                    "totalInvestment": 2999.90142,
                    "divReinvested": 0,
                    "assetValue": 3977.61,
                    "unrealizedCG": 977.70858,
                    "unrealizedSTCG": 0,
                    "unrealizedLTCG": 977.70858,
                    "growth": 977.70858,
                    "flowTotal": 977.7599999999998,
                    "dailyChange": 0,
                    "lockFreeUnits": 43.71,
                    "lockFreeValue": 3977.61,
                    "unrealizedCGIndexed": 977.70858,
                    "stcg": 0,
                    "stcgTax": 0,
                    "gfNAV": 49.71,
                    "xirrReturns": 0.18265563228420076,
                    "absoluteReturns": 0.3259135695198944
                }
            ],
            "totalUnits": 43.71,
            "totalInvestment": 2999.90142,
            "assetValue": 3977.61,
            "flowTotal": 977.7599999999998,
            "growth": 977.70858,
            "unrealizedCG": 977.70858,
            "unrealizedCGIndexed": 977.70858,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 0,
            "unrealizedLTCG": 977.70858,
            "dailyChange": 0,
            "lockFreeUnits": 43.71,
            "lockFreeValue": 3977.61,
            "stcg": 0,
            "stcgTax": 0,
            "cashFlows": [
                {
                    "amount": -999.95,
                    "when": "2021-02-25T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-03-25T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-04-26T00:00:00.000Z"
                },
                {
                    "amount": 3977.61,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.18265563228420076,
            "absoluteReturns": 0.3259135695198944,
            "gfNAV": 49.71
        },
        {
            "isAssetPlus": true,
            "schemeCode": "102875",
            "schemeName": "Kotak - Small Cap Fund - Growth",
            "nav": "165.063",
            "navDate": "2022-11-30",
            "navChange": 0.42499999999998295,
            "lockInPeriod": 0,
            "amfiBroad": "Equity",
            "amfiSub": "Small Cap",
            "legend": "Equity",
            "amcCode": "Kotak Mahindra Mutual Fund",
            "folios": [
                {
                    "folioNo": "8016916/14",
                    "schemeCode": "102875",
                    "schemeName": "",
                    "nav": "165.063",
                    "navDate": "2022-11-30",
                    "navChange": 0.42499999999998295,
                    "lockInPeriod": 0,
                    "amfiBroad": "Equity",
                    "amfiSub": "Small Cap",
                    "legend": "Equity",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2021-02-22",
                            "nav": 112.748,
                            "units": "8.869",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-03-22",
                            "nav": 117.913,
                            "units": "8.480",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-04-22",
                            "nav": 118.552,
                            "units": "8.435",
                            "amount": "999.95",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 88690,
                            "nav": 112.748,
                            "date": "2021-02-22",
                            "type": "P"
                        },
                        {
                            "units_10000": 84800,
                            "nav": 117.913,
                            "date": "2021-03-22",
                            "type": "P"
                        },
                        {
                            "units_10000": 84350,
                            "nav": 118.552,
                            "date": "2021-04-22",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [],
                    "cashFlows": [
                        {
                            "amount": -999.95,
                            "when": "2021-02-22T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-03-22T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-04-22T00:00:00.000Z"
                        },
                        {
                            "amount": 4255.984391999999,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 25.784,
                    "totalInvestment": 2999.8503720000003,
                    "divReinvested": 0,
                    "assetValue": 4255.984391999999,
                    "unrealizedCG": 1256.134019999999,
                    "unrealizedSTCG": 0,
                    "unrealizedLTCG": 1256.1340199999995,
                    "growth": 1256.134019999999,
                    "flowTotal": 1256.134391999999,
                    "dailyChange": 0,
                    "lockFreeUnits": 25.784,
                    "lockFreeValue": 4255.984391999999,
                    "unrealizedCGIndexed": 1256.1340199999995,
                    "stcg": 0,
                    "stcgTax": 0,
                    "gfNAV": 81.696,
                    "xirrReturns": 0.22980468674174015,
                    "absoluteReturns": 0.4187322246884382
                }
            ],
            "totalUnits": 25.784,
            "totalInvestment": 2999.8503720000003,
            "assetValue": 4255.984391999999,
            "flowTotal": 1256.134391999999,
            "growth": 1256.134019999999,
            "unrealizedCG": 1256.134019999999,
            "unrealizedCGIndexed": 1256.1340199999995,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 0,
            "unrealizedLTCG": 1256.1340199999995,
            "dailyChange": 0,
            "lockFreeUnits": 25.784,
            "lockFreeValue": 4255.984391999999,
            "stcg": 0,
            "stcgTax": 0,
            "cashFlows": [
                {
                    "amount": -999.95,
                    "when": "2021-02-22T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-03-22T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-04-22T00:00:00.000Z"
                },
                {
                    "amount": 4255.984391999999,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.22980468674174015,
            "absoluteReturns": 0.4187322246884382,
            "gfNAV": 81.696
        },
        {
            "isAssetPlus": true,
            "schemeCode": "112932",
            "schemeName": "Mirae Asset Emerging Bluechip Fund - Regular Plan - Growth Option",
            "nav": "99.569",
            "navDate": "2022-11-30",
            "navChange": 0.4759999999999991,
            "lockInPeriod": 0,
            "amfiBroad": "Equity",
            "amfiSub": "Large & Mid Cap",
            "legend": "Equity",
            "amcCode": "Mirae Asset Mutual Fund",
            "folios": [
                {
                    "folioNo": "79936310696",
                    "schemeCode": "112932",
                    "schemeName": "",
                    "nav": "99.569",
                    "navDate": "2022-11-30",
                    "navChange": 0.4759999999999991,
                    "lockInPeriod": 0,
                    "amfiBroad": "Equity",
                    "amfiSub": "Large & Mid Cap",
                    "legend": "Equity",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2021-02-22",
                            "nav": 77.157,
                            "units": "12.960",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-03-22",
                            "nav": 77.949,
                            "units": "12.828",
                            "amount": "999.95",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2021-04-20",
                            "nav": 77.374,
                            "units": "12.924",
                            "amount": "999.95",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 129600,
                            "nav": 77.157,
                            "date": "2021-02-22",
                            "type": "P"
                        },
                        {
                            "units_10000": 128280,
                            "nav": 77.949,
                            "date": "2021-03-22",
                            "type": "P"
                        },
                        {
                            "units_10000": 129240,
                            "nav": 77.374,
                            "date": "2021-04-20",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [],
                    "cashFlows": [
                        {
                            "amount": -999.95,
                            "when": "2021-02-22T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-03-22T00:00:00.000Z"
                        },
                        {
                            "amount": -999.95,
                            "when": "2021-04-20T00:00:00.000Z"
                        },
                        {
                            "amount": 3854.5151280000005,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 38.712,
                    "totalInvestment": 2999.8660680000003,
                    "divReinvested": 0,
                    "assetValue": 3854.5151280000005,
                    "unrealizedCG": 854.6490600000002,
                    "unrealizedSTCG": 0,
                    "unrealizedLTCG": 854.6490600000002,
                    "growth": 854.6490600000002,
                    "flowTotal": 854.6651280000001,
                    "dailyChange": 0,
                    "lockFreeUnits": 38.712,
                    "lockFreeValue": 3854.5151280000005,
                    "unrealizedCGIndexed": 854.6490600000002,
                    "stcg": 0,
                    "stcgTax": 0,
                    "gfNAV": 52.168,
                    "xirrReturns": 0.15964117890369958,
                    "absoluteReturns": 0.28489573888536684
                }
            ],
            "totalUnits": 38.712,
            "totalInvestment": 2999.8660680000003,
            "assetValue": 3854.5151280000005,
            "flowTotal": 854.6651280000001,
            "growth": 854.6490600000002,
            "unrealizedCG": 854.6490600000002,
            "unrealizedCGIndexed": 854.6490600000002,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 0,
            "unrealizedLTCG": 854.6490600000002,
            "dailyChange": 0,
            "lockFreeUnits": 38.712,
            "lockFreeValue": 3854.5151280000005,
            "stcg": 0,
            "stcgTax": 0,
            "cashFlows": [
                {
                    "amount": -999.95,
                    "when": "2021-02-22T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-03-22T00:00:00.000Z"
                },
                {
                    "amount": -999.95,
                    "when": "2021-04-20T00:00:00.000Z"
                },
                {
                    "amount": 3854.5151280000005,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.15964117890369958,
            "absoluteReturns": 0.28489573888536684,
            "gfNAV": 52.168
        },
        {
            "isAssetPlus": true,
            "schemeCode": "100377",
            "schemeName": "Nippon India Growth Fund - Growth Plan - Growth Option",
            "nav": "2217.2532",
            "navDate": "2022-11-30",
            "navChange": 28.158600000000206,
            "lockInPeriod": 0,
            "amfiBroad": "Equity",
            "amfiSub": "Mid Cap",
            "legend": "Equity",
            "amcCode": "Nippon India Mutual Fund",
            "folios": [
                {
                    "folioNo": "401185465892",
                    "schemeCode": "100377",
                    "schemeName": "",
                    "nav": "2217.2532",
                    "navDate": "2022-11-30",
                    "navChange": 28.158600000000206,
                    "lockInPeriod": 0,
                    "amfiBroad": "Equity",
                    "amfiSub": "Mid Cap",
                    "legend": "Equity",
                    "orders": [
                        {
                            "transactionType": "P",
                            "navDate": "2017-12-21",
                            "nav": 1183.874,
                            "units": "0.845",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-01-18",
                            "nav": 1195.4158,
                            "units": "0.837",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-02-19",
                            "nav": 1124.0935,
                            "units": "0.890",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-03-19",
                            "nav": 1084.7599,
                            "units": "0.922",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-04-18",
                            "nav": 1134.8436,
                            "units": "0.881",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-05-18",
                            "nav": 1101.6595,
                            "units": "0.908",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-06-18",
                            "nav": 1101.8661,
                            "units": "0.908",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-07-18",
                            "nav": 1055.487,
                            "units": "0.947",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-08-20",
                            "nav": 1124.1176,
                            "units": "0.890",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-09-18",
                            "nav": 1099.6928,
                            "units": "0.909",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-10-19",
                            "nav": 994.1896,
                            "units": "1.006",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-11-19",
                            "nav": 1041.1885,
                            "units": "0.960",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2018-12-18",
                            "nav": 1062.9288,
                            "units": "0.941",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "SO",
                            "navDate": "2018-12-31",
                            "nav": 1070.5582,
                            "units": "11.844",
                            "amount": "12679.56",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-01-18",
                            "nav": 1064.9694,
                            "units": "0.939",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-02-18",
                            "nav": 996.1433,
                            "units": "1.004",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-03-18",
                            "nav": 1094.4731,
                            "units": "0.914",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-04-18",
                            "nav": 1126.1574,
                            "units": "0.888",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-05-20",
                            "nav": 1113.6909,
                            "units": "0.898",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-06-18",
                            "nav": 1104.601,
                            "units": "0.905",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-07-18",
                            "nav": 1092.2942,
                            "units": "0.916",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-08-19",
                            "nav": 1052.0783,
                            "units": "0.950",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-09-18",
                            "nav": 1036.4353,
                            "units": "0.965",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-10-18",
                            "nav": 1086.6634,
                            "units": "0.920",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-11-18",
                            "nav": 1114.1146,
                            "units": "0.898",
                            "amount": "1000.00",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "R",
                            "navDate": "2019-12-03",
                            "nav": 1121.0177,
                            "units": "10.197",
                            "amount": "11430.91",
                            "folioNo": ""
                        },
                        {
                            "transactionType": "P",
                            "navDate": "2019-12-18",
                            "nav": 1127.0096,
                            "units": "0.887",
                            "amount": "1000.00",
                            "folioNo": ""
                        }
                    ],
                    "unitsQueue": [
                        {
                            "units_10000": 8870,
                            "nav": 1127.0096,
                            "date": "2019-12-18",
                            "type": "P"
                        }
                    ],
                    "sellQueue": [
                        {
                            "sellAmount": "12679.56",
                            "sellDate": "2018-12-31",
                            "sellUnits": 11.844,
                            "sellNav": 1070.5582,
                            "buyQueue": [
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.845,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1183.874,
                                    "buyDate": "2017-12-21",
                                    "originalNav": 1183.874,
                                    "age": 1.0268817204301075,
                                    "gain": -95.75185100000006,
                                    "stcg": 0,
                                    "ltcg": -95.75185100000006,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.837,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1195.4158,
                                    "buyDate": "2018-01-18",
                                    "originalNav": 1195.4158,
                                    "age": 0.9516129032258065,
                                    "gain": -104.50581120000004,
                                    "stcg": -104.50581120000004,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.89,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1124.0935,
                                    "buyDate": "2018-02-19",
                                    "originalNav": 1124.0935,
                                    "age": 0.8601190476190476,
                                    "gain": -47.64641700000001,
                                    "stcg": -47.64641700000001,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.922,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1084.7599,
                                    "buyDate": "2018-03-19",
                                    "originalNav": 1084.7599,
                                    "age": 0.782258064516129,
                                    "gain": -13.093967400000068,
                                    "stcg": -13.093967400000068,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.881,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1134.8436,
                                    "buyDate": "2018-04-18",
                                    "originalNav": 1134.8436,
                                    "age": 0.7000000000000001,
                                    "gain": -56.63543739999999,
                                    "stcg": -56.63543739999999,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.908,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1101.6595,
                                    "buyDate": "2018-05-18",
                                    "originalNav": 1101.6595,
                                    "age": 0.6182795698924731,
                                    "gain": -28.239980400000036,
                                    "stcg": -28.239980400000036,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.908,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1101.8661,
                                    "buyDate": "2018-06-18",
                                    "originalNav": 1101.8661,
                                    "age": 0.5333333333333333,
                                    "gain": -28.427573200000015,
                                    "stcg": -28.427573200000015,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.947,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1055.487,
                                    "buyDate": "2018-07-18",
                                    "originalNav": 1055.487,
                                    "age": 0.45161290322580644,
                                    "gain": 14.272426399999869,
                                    "stcg": 14.272426399999869,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.89,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1124.1176,
                                    "buyDate": "2018-08-20",
                                    "originalNav": 1124.1176,
                                    "age": 0.36290322580645157,
                                    "gain": -47.66786600000009,
                                    "stcg": -47.66786600000009,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.909,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1099.6928,
                                    "buyDate": "2018-09-18",
                                    "originalNav": 1099.6928,
                                    "age": 0.2833333333333333,
                                    "gain": -26.483351400000085,
                                    "stcg": -26.483351400000085,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 1.006,
                                    "sellNav": 1070.5582,
                                    "buyNav": 994.1896,
                                    "buyDate": "2018-10-19",
                                    "originalNav": 994.1896,
                                    "age": 0.19892473118279572,
                                    "gain": 76.8268115999999,
                                    "stcg": 76.8268115999999,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.96,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1041.1885,
                                    "buyDate": "2018-11-19",
                                    "originalNav": 1041.1885,
                                    "age": 0.11388888888888889,
                                    "gain": 28.194911999999967,
                                    "stcg": 28.194911999999967,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "12679.56",
                                    "sellDate": "2018-12-31",
                                    "sellUnits": 0.941,
                                    "sellNav": 1070.5582,
                                    "buyNav": 1062.9288,
                                    "buyDate": "2018-12-18",
                                    "originalNav": 1062.9288,
                                    "age": 0.03494623655913979,
                                    "gain": 7.17926540000003,
                                    "stcg": 7.17926540000003,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                }
                            ]
                        },
                        {
                            "sellAmount": "11430.91",
                            "sellDate": "2019-12-03",
                            "sellUnits": 10.197,
                            "sellNav": 1121.0177,
                            "buyQueue": [
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.939,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1064.9694,
                                    "buyDate": "2019-01-18",
                                    "originalNav": 1064.9694,
                                    "age": 0.875,
                                    "gain": 52.629353700000145,
                                    "stcg": 52.629353700000145,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 1.004,
                                    "sellNav": 1121.0177,
                                    "buyNav": 996.1433,
                                    "buyDate": "2019-02-18",
                                    "originalNav": 996.1433,
                                    "age": 0.7916666666666666,
                                    "gain": 125.37389760000015,
                                    "stcg": 125.37389760000015,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.914,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1094.4731,
                                    "buyDate": "2019-03-18",
                                    "originalNav": 1094.4731,
                                    "age": 0.7083333333333334,
                                    "gain": 24.26176440000016,
                                    "stcg": 24.26176440000016,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.888,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1126.1574,
                                    "buyDate": "2019-04-18",
                                    "originalNav": 1126.1574,
                                    "age": 0.625,
                                    "gain": -4.564053599999954,
                                    "stcg": -4.564053599999954,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.898,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1113.6909,
                                    "buyDate": "2019-05-20",
                                    "originalNav": 1113.6909,
                                    "age": 0.5361111111111111,
                                    "gain": 6.579466400000044,
                                    "stcg": 6.579466400000044,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.905,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1104.601,
                                    "buyDate": "2019-06-18",
                                    "originalNav": 1104.601,
                                    "age": 0.4583333333333333,
                                    "gain": 14.857113499999993,
                                    "stcg": 14.857113499999993,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.916,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1092.2942,
                                    "buyDate": "2019-07-18",
                                    "originalNav": 1092.2942,
                                    "age": 0.375,
                                    "gain": 26.310726000000056,
                                    "stcg": 26.310726000000056,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.95,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1052.0783,
                                    "buyDate": "2019-08-19",
                                    "originalNav": 1052.0783,
                                    "age": 0.2888888888888889,
                                    "gain": 65.4924300000002,
                                    "stcg": 65.4924300000002,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.965,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1036.4353,
                                    "buyDate": "2019-09-18",
                                    "originalNav": 1036.4353,
                                    "age": 0.20833333333333334,
                                    "gain": 81.622016,
                                    "stcg": 81.622016,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.92,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1086.6634,
                                    "buyDate": "2019-10-18",
                                    "originalNav": 1086.6634,
                                    "age": 0.125,
                                    "gain": 31.60595600000018,
                                    "stcg": 31.60595600000018,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                },
                                {
                                    "sellAmount": "11430.91",
                                    "sellDate": "2019-12-03",
                                    "sellUnits": 0.898,
                                    "sellNav": 1121.0177,
                                    "buyNav": 1114.1146,
                                    "buyDate": "2019-11-18",
                                    "originalNav": 1114.1146,
                                    "age": 0.041666666666666664,
                                    "gain": 6.198983799999995,
                                    "stcg": 6.198983799999995,
                                    "ltcg": 0,
                                    "taxation": "EQUITY"
                                }
                            ]
                        }
                    ],
                    "cashFlows": [
                        {
                            "amount": -1000,
                            "when": "2019-12-18T00:00:00.000Z"
                        },
                        {
                            "amount": 1966.7035884000002,
                            "when": "2022-11-30T00:00:00.000Z"
                        }
                    ],
                    "xirr": 0,
                    "divPaid": 0,
                    "totalUnits": 0.887,
                    "totalInvestment": 999.6575152000001,
                    "divReinvested": 0,
                    "assetValue": 1966.7035884000002,
                    "unrealizedCG": 967.0460732,
                    "unrealizedSTCG": 0,
                    "unrealizedLTCG": 967.0460732,
                    "growth": 967.0460732,
                    "flowTotal": 966.7035884000002,
                    "dailyChange": 0,
                    "lockFreeUnits": 0.887,
                    "lockFreeValue": 1966.7035884000002,
                    "unrealizedCGIndexed": 967.0460732,
                    "stcg": 0,
                    "stcgTax": 0,
                    "gfNAV": 1177.8459,
                    "xirrReturns": 0.25735250727496894,
                    "absoluteReturns": 0.9673773852503118
                }
            ],
            "totalUnits": 0.887,
            "totalInvestment": 999.6575152000001,
            "assetValue": 1966.7035884000002,
            "flowTotal": 966.7035884000002,
            "growth": 967.0460732,
            "unrealizedCG": 967.0460732,
            "unrealizedCGIndexed": 967.0460732,
            "divPaid": 0,
            "divReinvested": 0,
            "unrealizedSTCG": 0,
            "unrealizedLTCG": 967.0460732,
            "dailyChange": 0,
            "lockFreeUnits": 0.887,
            "lockFreeValue": 1966.7035884000002,
            "stcg": 0,
            "stcgTax": 0,
            "cashFlows": [
                {
                    "amount": -1000,
                    "when": "2019-12-18T00:00:00.000Z"
                },
                {
                    "amount": 1966.7035884000002,
                    "when": "2022-11-30T00:00:00.000Z"
                }
            ],
            "xirrReturns": 0.25735250727496894,
            "absoluteReturns": 0.9673773852503118,
            "gfNAV": 1177.8459
        }
    ],
    "cashFlows": [
        {
            "amount": -1000,
            "when": "2019-12-18T00:00:00.000Z"
        },
        {
            "amount": -1000,
            "when": "2019-12-18T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2020-08-10T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2020-08-10T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2020-08-11T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2020-08-11T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2020-09-10T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2020-09-10T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2020-09-11T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2020-09-11T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2020-10-12T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2020-10-12T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2020-10-12T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2020-10-12T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2020-11-10T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2020-11-10T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2020-11-11T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2020-11-11T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2020-12-10T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2020-12-10T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2020-12-11T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2020-12-11T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-01-11T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2021-01-11T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-01-11T00:00:00.000Z"
        },
        {
            "amount": -1999.9,
            "when": "2021-01-11T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-02-22T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-02-22T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-02-22T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-02-22T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-02-25T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-02-25T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-03-22T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-03-22T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-03-22T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-03-22T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-03-25T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-03-25T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-04-20T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-04-20T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-04-22T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-04-22T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-04-26T00:00:00.000Z"
        },
        {
            "amount": -999.95,
            "when": "2021-04-26T00:00:00.000Z"
        },
        {
            "amount": 6297.2366439,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 12096.2943189,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 3977.61,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 4255.984391999999,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 3854.5151280000005,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 1966.7035884000002,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 6297.2366439,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 12096.2943189,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 3977.61,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 4255.984391999999,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 3854.5151280000005,
            "when": "2022-11-30T00:00:00.000Z"
        },
        {
            "amount": 1966.7035884000002,
            "when": "2022-11-30T00:00:00.000Z"
        }
    ],
    "unrealizedSTCG": 790.3059869999986,
    "unrealizedLTCG": 8111.075466399999,
    "isXIRRMode": true,
    "unrealizedCGIndexed": 8901.381453399996
};

// fs.writeFile("output.json", JSON.stringify(response), (err) => { }); //Note: For saving the output in json file.

describe('Portfolio Tests', () => {
    test('Generate function test', () => {
        let response = Portfolio.generate(orders, schemes, nav, true)
        expect(JSON.parse(JSON.stringify(response))).toEqual(generateOutput);
    });

    test('combine function test', () => {
        let portfolio = Portfolio.generate(orders, schemes, nav, true);
        let response = Portfolio.combine(portfolio, portfolio)
        expect(JSON.parse(JSON.stringify(response))).toEqual(combineOutput);
    });
});