import fs from "fs";
import { BSEOrdersModel, PartnerModel, PastOrdersModel } from "../db/models";
import CapitalGains from "../Functions/Capital_Gains";
import axios from 'axios';
import PortfolioHelper from "../helpers/portfolioHelper";

var request = require('request').defaults({ encoding: null });
export async function test() {
    // var response = await CapitalGains.generate("5ff31b0f7a36e024929d0438", "2022");
    var response = await PortfolioHelper("5ff31b0f7a36e024929d0438", false, false, undefined, false, false, true);
    // var response = await PastOrdersModel.find({ userId: "5ff31b0f7a36e024929d0438" });
    // var response = await BSEOrdersModel.find({ userId: "57f51aaee0bb123a1b7924df", isProcessed: true }, {}, { lean: true }).limit(600);
    // let response = await axios.get("https://s3.ap-south-1.amazonaws.com/assetplus-statics/partners/1555411416968_ValueX%20Logo.jpg", { responseType: 'arraybuffer' });
    console.log("Final Output: ", response);
    // advisorLogo = "data:" + response.headers["content-type"] + ";base64," + new Buffer(body).toString('base64');

    fs.writeFile("test.json", JSON.stringify(response, null, 4), (err: any) => { }); //Note: For saving the output in json file.


    // var partner = await PartnerModel.findOne({ adminId: "5badcc07feea3061cd4463cb" });
    // if (partner) {
    //     // let response = request.get("https://s3.ap-south-1.amazonaws.com/assetplus-statics/partners/1555411416968_ValueX%20Logo.jpg");
    //     let response = await axios.get("https://s3.ap-south-1.amazonaws.com/assetplus-statics/partners/1555411416968_ValueX%20Logo.jpg")

    //     console.log("Final Output: ", response.headers["content-type"], new Buffer(response.data).toString('base64'));
    //     fs.writeFile("output.json", JSON.stringify(new Buffer(response.data).toString('base64')), (err: any) => { }); //Note: For saving the output in json file.
    // }
    // request.get("https://s3.ap-south-1.amazonaws.com/assetplus-statics/partners/1555411416968_ValueX%20Logo.jpg", function (error: any, response: any, body: any) {
    //     // if (error) return callback();
    //     if (!error && response.statusCode == 200) {
    //         var advisorLogo = "data:" + response.headers["content-type"] + ";base64," + new Buffer(body).toString('base64');
    //         console.log("advisorLogo: ", advisorLogo);

    //     }
    //     // return callback();
    // })


    return;

}
test();