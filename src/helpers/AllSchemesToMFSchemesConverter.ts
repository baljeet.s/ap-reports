import { MFScheme } from "../Classes/MFScheme";

function convertToMFScheme(schemeJSON: any): MFScheme {
    var scheme: MFScheme = new MFScheme();
    Object.assign(scheme, schemeJSON);
    return scheme;
}
export function AllSchemesToMFSchemesConverter(schemesArray: any[]) {
    var schemes: MFScheme[] = [];
    schemesArray.forEach(function (schemeJSON) {
        schemes.push(convertToMFScheme(schemeJSON));
    });
    return schemes;
}