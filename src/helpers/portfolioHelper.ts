import { AllSchemes, AssetModel, BSEOrdersModel, LatestNAV, PastOrdersModel } from "../db/models";
import { MFScheme } from "../Classes/MFScheme";
import navHistoryHelper from './navHistory/index';
import { PortfolioClass } from "../Classes/Portfolio";
import { LatestNAVClass } from "../Classes/LatestNAV";
import { BSEOrder } from "../Classes/BSEOrder";
import { PastOrders } from '../Classes/PastOrders';
import moment from "moment";
import navHelper from "../helpers/navHistory/fetch"
import Portfolio from "../Functions/Portfolio";
import AssetClass, { Dividend } from "../Classes/AssetClass";
import AllSchemesClass from "../Classes/AllSchemesClass";

async function fetchPastOrders(userId: string, ignoreExternal?: boolean) {
    if (ignoreExternal) {
        return {
            orders: [],
            schemes: {}
        };
    }
    let pastOrders = await PastOrdersModel.find({ userId }, {}, { lean: true });
    console.log("Past Orders: ", pastOrders);
    if (!pastOrders || pastOrders.length == 0) {
        return {
            orders: [],
            schemes: {}
        }
    }
    var schemes: any = {};
    var availableSchemeCodes: any = {};

    for (let order of pastOrders) {
        if (!schemes[order.schemeCode]) {
            schemes[order.schemeCode] = await LatestNAV.findOne({ schemeCode: { $in: [order.schemeCode, order.schemeCode.replace(/DRI|DR|I/, "")] } })
            if (schemes[order.schemeCode]) {
                availableSchemeCodes[order.schemeCode] = true;
            }
        } else {
            availableSchemeCodes[order.schemeCode] = true;
        }
    };
    pastOrders = pastOrders.filter((order) => {
        if (availableSchemeCodes[order.schemeCode]) {
            return true;
        }
    });
    return {
        orders: pastOrders,
        schemes
    };
}

export default async function PortfolioHelper(userId: string, ignoreInternal?: boolean, ignoreExternal?: boolean, valuationDate?: string, withTrend?: boolean, includeOtherAssets?: boolean, includeZeroFolios?: boolean): Promise<any> { //PortfolioClass
    var bseOrders: BSEOrder[] = [];
    if (!ignoreInternal) {
        bseOrders = await BSEOrdersModel.find({ userId, isProcessed: true }, { bseOrderId: 0, schemeName: 0, bseSchemeCode: 0, payoutDate: 0, bseTransactionId: 0, orderRemark: 0, isAdditional: 0, processingRemarks: 0, paymentSuccess: 0, sipId: 0, cancellationReason: 0 }, { lean: true }).exec();

        bseOrders = bseOrders.sort((a, b) => {
            var aDate = a.bseOrderDate.split("/").reverse().join("-");
            var bDate = b.bseOrderDate.split("/").reverse().join("-");
            if (aDate < bDate) return -1;
            if (aDate > bDate) return 1;

            var multA = ["P", "DR", "SI"].includes(a.transactionType) ? 1 : -1;
            var multB = ["P", "DR", "SI"].includes(b.transactionType) ? 1 : -1;

            if (multA > multB) return -1;
            if (multA < multB) return 1;

            if (a.createdAt < b.createdAt) return -1;
            if (a.createdAt > b.createdAt) return 1;
            return 0;
        });
    }

    var pastOrders: PastOrders[] = [];
    if (!ignoreExternal) {
        pastOrders = await PastOrdersModel.find({ userId }, { mode: 0, orderId: 0 }, { lean: true }).exec();
        pastOrders.forEach((order: PastOrders) => {
            order.amount = order.processingAmount;
            order.createdAt = order.processingDate;
            order.orderStatus = "VALID";
            order.isProcessed = true;
            order.bseOrderDate = yyyymmddtoddmmyy(order.processingDate);
        });
    }

    var allOrders: (BSEOrder | PastOrders | any)[] = [...bseOrders, ...pastOrders];

    if (valuationDate) {
        allOrders.forEach(order => order.dateIso = order.bseOrderDate!.split("/").reverse().join("-"));
        allOrders = allOrders.filter(order => {
            return order.dateIso <= valuationDate;
        });
    }
    var schemeCodes = [...new Set(allOrders.map(order => order.schemeCode))];
    // var schemeCodeSet = new Set(bseOrders.map(order => order.schemeCode));

    var schemes: any[] = await AllSchemes.find({ schemeCode: { $in: Array.from(schemeCodes) } });
    // var schemes: LatestNAVClass[] = await LatestNAV.find({ schemeCode: { $in: Array.from(schemeCodes) } });
    // for (let scheme of schemes) {
    //     let allScheme: AllSchemesClass | null = await AllSchemes.findOne({ schemeCode: scheme.schemeCode });
    //     scheme.isin = allScheme?.isin;
    // }
    var navs: any = {}
    // schemeCode: string, startDate: string, endDate: string, lookBackward?: boolean
    if (valuationDate) {
        var startDate: string = moment(valuationDate, "YYYY-MM-DD").format("YYYYMMDD");
        for (let i = 0; i < schemeCodes.length; i++) {
            var schemeCode: string = schemeCodes[i];
            var nav: any = await navHelper({ schemeCode, startDate, lookBackward: true });
            navs[schemeCode] = { "nav": nav.n, "date": moment(nav.d, "YYYYMMDD").format("DD-MMM-YYYY"), "change": 0 };
        }
    } else {
        schemes.forEach(scheme => {
            scheme.
                navs[scheme.schemeCode] = {
                "nav": parseFloat(scheme?.nav),
                "date": scheme?.date,
                "change": scheme?.change
            }
        })
    }

    allOrders = allOrders.filter(order => navs[order.schemeCode]);

    var allAssets: AssetClass[] = [];
    if (includeOtherAssets) {
        allAssets = await AssetModel.find({ userId }, {}, { lean: true }).exec();

        if (valuationDate) {
            allAssets = allAssets.filter(asset => {
                asset.dividends = asset.dividends.filter((div: Dividend) => {
                    return moment(div.date).format("YYYY-MM-DD") <= valuationDate;
                });
                return moment(asset.purchaseDate).format("YYYY-MM-DD") <= valuationDate;
            });
        }

    }
    // schemes = schemes.map((scheme) => {
    //     let mfScheme = new MFScheme();
    //     Object.assign(mfScheme, scheme);
    //     return mfScheme;
    // })
    var portfolio = Portfolio.generate(allOrders, schemes, navs, allAssets, includeZeroFolios);

    // TODO: Add ActiveSIPs Data
    // var activeSIPs = await models.BSEISIP.find({ userId, isCancelled: false, isPendingCancellation: false }, { schemeCode: 1, schemeName: 1, installmentAmount: 1, bseStartDate: 1, installmentCount: 1, bseSIPId: 1 });
    // activeSIPs = activeSIPs.filter(a => !a.bseSIPId.startsWith("TEMP"));
    // activeSIPs = activeSIPs.sort((a, b) => a.schemeName.localeCompare(b.schemeName));
    // portfolio.isips = activeSIPs;

    return portfolio;

    // var latestNAV: any = {};
    // var allSchemes: any[] = await AllSchemes.find({ schemeCode: { $in: Array.from(schemeCodeSet) } });
    // var apSchemes: any[] = allSchemes;
    // apSchemes.filter(scheme => Array.from(schemeCodeSet).includes(scheme.schemeCode))

    // var filteredApSchemes = [];
    // for (let scheme of apSchemes) { // I tried for searching the type of scheme but didn't get right class
    //     let latestNav: LatestNAVClass | null = await LatestNAV.findOne({ schemeCode: scheme.schemeCode });
    //     latestNAV[scheme.schemeCode] = {
    //         "nav": latestNav?.nav,
    //         "date": latestNav?.date,
    //         "change": latestNav?.change,
    //         "gfNAV": scheme.gfNAV
    //     }
    //     filteredApSchemes.push({
    //         schemeCode: scheme.schemeCode,
    //         schemeName: scheme.schemeName,
    //         legend: scheme.legend,
    //         amfiBroad: scheme.amfiBroad,
    //         amfiSub: scheme.amfiSub,
    //         isin: scheme.isin || "N/A",
    //         lockOutPeriod: scheme.getLockOutPeriod,
    //         gfNAV: scheme.gfNAV
    //     })
    // };
    // // apSchemes = filteredApSchemes;
    // var data: any = pastOrders;
    // console.log("FilteredAPSchemes: ", filteredApSchemes)
    // data.navs = {};
    // let schemes = [];
    // for (let code of Object.keys(data.schemes)) {
    //     if (!data.schemes[code]) {
    //         console.log("Not found: ", code);
    //         throw "";
    //     }
    //     data.navs[code] = await LatestNAV.findOne({ schemeCode: code });
    //     data.navs[code] = {
    //         "nav": data.navs[code].nav,
    //         "date": data.navs[code].date,
    //         "change": data.navs[code].change,
    //         "gfNAV": data.navs[code].gfNAV,
    //     }
    //     let lockOutPeriod = data.schemes[code].lockOutPeriod;
    //     let schemeName = data.schemes[code].schemeName;
    //     console.log("dataSchemes: ", filteredApSchemes.find((e) => e.schemeCode == code))
    //     let legend = data.schemes[code].legend;
    //     let amfiBroad = data.schemes[code].amfiBroad;
    //     let amfiSub = data.schemes[code].amfiSub;
    //     let isin = data.schemes[code].isin;
    //     schemes.push({
    //         schemeCode: code,
    //         schemeName,
    //         lockOutPeriod,
    //         legend,
    //         isin,
    //         amfiBroad,
    //         amfiSub
    //     });
    // };
    // console.log("checking schemes: ", schemes);

    // data.orders.forEach((order: any) => {
    //     order.schemeName = data.navs[order.schemeCode].schemeName;
    //     order.amount = order.processingAmount;
    //     order.createdAt = order.processingDate;
    //     order.orderStatus = "VALID";
    //     order.isProcessed = true;
    //     order.bseOrderDate = yyyymmddtoddmmyy(order.processingDate);
    // });

    // var reqLambda = {
    //     type: withTrend ? "portfolio" : "portfolioOnly",
    //     data: {
    //         internal: [bseOrders, filteredApSchemes, latestNAV],
    //         external: [pastOrders.orders, schemes, data.navs]
    //     },
    //     gzip: true
    // }
    // console.log("Final Console: ", schemes)
    // return navHistoryHelper(reqLambda);

}



function yyyymmddtoddmmyy(str: string) {
    var splits = str.split("-");
    return splits[2] + "/" + splits[1] + "/" + splits[0];
}

// export default async function PortfolioHelper(userId: string, withTrend?: boolean, ignoreExternal?: boolean, ignoreInternal?: boolean): Promise<PortfolioClass> {
//     var bseOrders = ignoreInternal ? [] : await BSEOrdersModel.find({ userId, isProcessed: true }, {}, { lean: true });
//     var pastOrders = await fetchPastOrders(userId, ignoreExternal)


//     bseOrders = bseOrders.sort((a, b) => {
//         var aDate = a.bseOrderDate.split("/").reverse().join("-");
//         var bDate = b.bseOrderDate.split("/").reverse().join("-");
//         if (aDate < bDate) return -1;
//         if (aDate > bDate) return 1;

//         var multA = ["P", "DR", "SI"].includes(a.transactionType) ? 1 : -1;
//         var multB = ["P", "DR", "SI"].includes(b.transactionType) ? 1 : -1;

//         if (multA > multB) return -1;
//         if (multA < multB) return 1;

//         if (a.createdAt < b.createdAt) return -1;
//         if (a.createdAt > b.createdAt) return 1;
//         return 0;
//     });

//     var schemeCodeSet = new Set(bseOrders.map(order => order.schemeCode));

//     var latestNAV: any = {};
//     var allSchemes: any[] = await AllSchemes.find({ schemeCode: { $in: Array.from(schemeCodeSet) } });
//     var apSchemes: any[] = allSchemes;
//     apSchemes.filter(scheme => Array.from(schemeCodeSet).includes(scheme.schemeCode))

//     var filteredApSchemes = [];
//     for (let scheme of apSchemes) { // I tried for searching the type of scheme but didn't get right class
//         let latestNav: LatestNAVClass | null = await LatestNAV.findOne({ schemeCode: scheme.schemeCode });
//         latestNAV[scheme.schemeCode] = {
//             "nav": latestNav?.nav,
//             "date": latestNav?.date,
//             "change": latestNav?.change,
//             "gfNAV": scheme.gfNAV
//         }
//         filteredApSchemes.push({
//             schemeCode: scheme.schemeCode,
//             schemeName: scheme.schemeName,
//             legend: scheme.legend,
//             amfiBroad: scheme.amfiBroad,
//             amfiSub: scheme.amfiSub,
//             isin: scheme.isin || "N/A",
//             lockOutPeriod: scheme.getLockOutPeriod,
//             gfNAV: scheme.gfNAV
//         })
//     };
//     // apSchemes = filteredApSchemes;
//     var data: any = pastOrders;
//     console.log("FilteredAPSchemes: ", filteredApSchemes)
//     data.navs = {};
//     let schemes = [];
//     for (let code of Object.keys(data.schemes)) {
//         if (!data.schemes[code]) {
//             console.log("Not found: ", code);
//             throw "";
//         }
//         data.navs[code] = await LatestNAV.findOne({ schemeCode: code });
//         data.navs[code] = {
//             "nav": data.navs[code].nav,
//             "date": data.navs[code].date,
//             "change": data.navs[code].change,
//             "gfNAV": data.navs[code].gfNAV,
//         }
//         let lockOutPeriod = data.schemes[code].lockOutPeriod;
//         let schemeName = data.schemes[code].schemeName;
//         console.log("dataSchemes: ", filteredApSchemes.find((e) => e.schemeCode == code))
//         let legend = data.schemes[code].legend;
//         let amfiBroad = data.schemes[code].amfiBroad;
//         let amfiSub = data.schemes[code].amfiSub;
//         let isin = data.schemes[code].isin;
//         schemes.push({
//             schemeCode: code,
//             schemeName,
//             lockOutPeriod,
//             legend,
//             isin,
//             amfiBroad,
//             amfiSub
//         });
//     };
//     console.log("checking schemes: ", schemes);

//     data.orders.forEach((order: any) => {
//         order.schemeName = data.navs[order.schemeCode].schemeName;
//         order.amount = order.processingAmount;
//         order.createdAt = order.processingDate;
//         order.orderStatus = "VALID";
//         order.isProcessed = true;
//         order.bseOrderDate = yyyymmddtoddmmyy(order.processingDate);
//     });

//     var reqLambda = {
//         type: withTrend ? "portfolio" : "portfolioOnly",
//         data: {
//             internal: [bseOrders, filteredApSchemes, latestNAV],
//             external: [pastOrders.orders, schemes, data.navs]
//         },
//         gzip: true
//     }
//     console.log("Final Console: ", schemes)
//     return navHistoryHelper(reqLambda);

// }
