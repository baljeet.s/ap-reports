import { PortfolioClass } from "../../Classes/Portfolio";
import fetch from "./fetch";

function fatDate(input: string) {
    return [input.substring(0, 4), input.substring(4, 2), input.substring(6, 2)].join("-");
}

function thinDate(input: string) {
    return input.replace(/-/g, "");
}
function getDaysArray(start: Date, end: Date) {
    for (var arr = [], dt = start; dt <= end; dt.setDate(dt.getDate() + 1)) {
        arr.push(dt.toISOString().slice(0, 10));
    }
    return arr;
};

function getDateRange(startDate: string) {
    return getDaysArray(new Date(startDate), new Date());
}

export default async function TrendCalculator(portfolio: PortfolioClass): Promise<PortfolioClass> {
    var startDate: string | undefined;
    portfolio.holdings.forEach(holding => {
        holding.folios.forEach(folio => {
            if (!startDate || startDate > folio.orders[0].navDate) {
                startDate = folio.orders[0].navDate
            }
        });
    });

    console.time("Getting date range");
    portfolio.trend = getDateRange(startDate!).map(date => {
        return { date, amount: 0, value: 0 }
    });
    console.timeEnd("Getting date range");

    var tempHoldings = [];
    // portfolio.holdings.forEach(async holding => { 
    for (var holding of portfolio.holdings) {
        holding.folios.forEach(folio => {

            folio.trend = getDateRange(folio.orders[0].navDate).map(date => { return { date, units_10000: 0, amount: 0 } });

            var movingIndex = 0;
            folio.orders.forEach(order => {
                while (order.navDate > folio.trend![movingIndex].date) {
                    movingIndex++;
                }
                if (["P", "SI", "DR"].includes(order.transactionType)) {
                    folio.trend![movingIndex].units_10000! += Math.round(parseFloat(order.units) * 10000);
                    folio.trend![movingIndex].amount += parseFloat(order.units) * order.nav;
                } else if (["R", "SO"].includes(order.transactionType)) {
                    folio.trend![movingIndex].units_10000! -= Math.round(parseFloat(order.units) * 10000);
                    folio.trend![movingIndex].amount -= parseFloat(order.buyValue);
                } else if (["DP"].includes(order.transactionType)) {
                    folio.trend![movingIndex].amount -= parseFloat(order.amount);
                }
            })

            //Cumulate units and amounts;
            var prevItem: any = { units_10000: 0, amount: 0 };
            folio.trend.forEach(item => {
                item.units_10000! += prevItem.units_10000;
                item.amount += prevItem.amount;
                prevItem = item;
            });
            folio.trend = folio.trend.filter(item => item.units_10000);
        })
        var nonZeroTrends = holding.folios
            .map(folio => folio.trend)
            .filter(trend => trend!.length);

        if (nonZeroTrends.length == 0) {
            throw "";
        }

        let startDate: string | undefined
        let endDate: string;
        nonZeroTrends.forEach(trend => {
            if (!startDate || startDate > trend![0].date) {
                startDate = trend![0].date;
            }
            if (!endDate || endDate < trend![trend!.length - 1].date) {
                endDate = trend![trend!.length - 1].date;
            }
        });

        var history = await fetch({ startDate: thinDate(startDate!), endDate: thinDate(endDate!), schemeCode: holding.schemeCode.replace("DR", "") });
        holding.folios.forEach(folio => {
            //Merge NAV Trend with Folio Trend
            var movingIndex = 0;
            folio.trend?.forEach(trendItem => {
                while (history![movingIndex] && fatDate(history![movingIndex].d) < trendItem.date) {
                    movingIndex++;
                }
                movingIndex = Math.max(Math.min(movingIndex, history!.length - 1), 0);
                if (fatDate(history![movingIndex].d) > trendItem.date) {
                    movingIndex--;
                }
                movingIndex = Math.max(Math.min(movingIndex, history!.length - 1), 0);
                trendItem.navdate = history![movingIndex].d;
                trendItem.value = parseFloat(history![movingIndex].n) * trendItem.units_10000! / 10000.0;
                trendItem.amount = trendItem.amount;
                delete trendItem.units_10000;
            });

            //Merge Folio Trend with Portfolio Trend
            var movingIndex = 0;
            folio.trend.forEach(trendItem => {
                while (portfolio.trend[movingIndex].date != trendItem.date) {
                    movingIndex++;
                }
                portfolio.trend[movingIndex].amount += trendItem.amount;
                portfolio.trend[movingIndex].value += trendItem.value!;
            });
        })
        tempHoldings.push(holding);
    }
    portfolio.holdings = tempHoldings;
    return portfolio;
}