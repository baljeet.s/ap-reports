import { NavHistory } from "../../db/models";

function fatDate(input: string) {
    return [input.substring(0, 4), input.substring(4, 2), input.substring(6, 2)].join("-");
}

export default async function navHelper({ schemeCode, startDate, endDate, lookBackward = true }: { schemeCode: string, startDate: string, endDate?: string, lookBackward?: boolean }) {

    var lookBackward = !!lookBackward;
    if (!startDate) { throw "Start Date required" };
    if (startDate.length != 8) { throw "Start Date format is wrong. Should be YYYYMMDD" };
    if (endDate && endDate.length != 8) { throw "End Date format wrong" };

    schemeCode = schemeCode.replace("DR", "").replace("I", "");

    if (!endDate) {
        let item = null;
        if (lookBackward) {
            item = await NavHistory.findOne(
                { code: schemeCode, date: { $lte: fatDate(startDate) } },
                { nav: 1, date: 1, _id: 0 },
                { sort: { date: -1 }, lean: true }
            )
        } else {
            item = await NavHistory.findOne(
                { code: schemeCode, date: { $gte: fatDate(startDate) } },
                { nav: 1, date: 1, _id: 0 },
                { sort: { date: -1 }, lean: true }
            )
        }
        return { d: item?.date.split("-").join(""), n: item?.nav + "" }
    } else {
        var result = await NavHistory.find(
            { code: schemeCode, date: { $gte: fatDate(startDate), $lte: fatDate(endDate) } },
            { _id: 0, code: 0 },
            { sort: { date: 1 }, lean: true }
        )

        if (lookBackward) {
            if (result && result[0] && result[0].date != fatDate(startDate)) {
                let item = await NavHistory.findOne(
                    { code: schemeCode, date: { $lte: fatDate(startDate), } },
                    { _id: 0, code: 0 },
                    { sort: { date: -1 }, lean: true }
                )
                if (item) {
                    result.unshift(item);
                }
            }
        } else {
            if (result && result[result.length - 1] && result[result.length - 1].date != fatDate(endDate)) {
                var item = await NavHistory.findOne(
                    { code: schemeCode, date: { $gte: fatDate(endDate), } },
                    { _id: 0, code: 0 },
                    { sort: { date: 1 }, lean: true }
                )
                if (item) {
                    result.push(item);
                }
            }
        }

        return result.map(item => {
            return { d: item.date.split("-").join(""), n: item.nav + "" }
        });
    }
}