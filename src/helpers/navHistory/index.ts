import Portfolio from "../../Functions/Portfolio";
import TrendCalculator from "./TrendCalculator";

export default async function navHistoryHelper(event: any) {
    console.log("Came to navHistoryHelper");

    switch (event.type) {
        default:
        case "portfolioOnly": {
            var internalPortfolio = Portfolio.generate(event.data.internal[0], event.data.internal[1], event.data.internal[2], true);
            var externalPortfolio = Portfolio.generate(event.data.external[0], event.data.external[1], event.data.external[2], false);
            var combinedPortfolio = Portfolio.combine(internalPortfolio, externalPortfolio);
            return combinedPortfolio;
        }

        case "portfolio": {
            var internalPortfolio = Portfolio.generate(event.data.internal[0], event.data.internal[1], event.data.internal[2], true);
            var externalPortfolio = Portfolio.generate(event.data.external[0], event.data.external[1], event.data.external[2], false);
            var combinedPortfolio = Portfolio.combine(internalPortfolio, externalPortfolio);
            var internalPortfolioWithTrend = await TrendCalculator(internalPortfolio);
            var externalPortfolioWithTrend = await TrendCalculator(externalPortfolio);
            var combinedPortfolioWithTrend = await TrendCalculator(combinedPortfolio);
            combinedPortfolioWithTrend.internalTrend = internalPortfolioWithTrend.trend;
            combinedPortfolioWithTrend.externalTrend = externalPortfolioWithTrend.trend;

            return combinedPortfolio;
        }

    }
}