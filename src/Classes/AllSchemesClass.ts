import { prop } from "@typegoose/typegoose"

class Returns {
    @prop()
    yrs1!: string;
    @prop()
    yrs10!: string;
    @prop()
    yrs15!: string;
    @prop()
    yrs3!: string;
    @prop()
    yrs7!: string;
    @prop()
    yrs5!: string;
}
export default class AllSchemesClass {
    @prop({ index: true })
    schemeCode!: string
    @prop()
    schemeName!: string
    @prop()
    aumInLakhs!: number
    @prop()
    amfiBroad!: string
    @prop()
    amfiSub!: string
    @prop()
    isin!: string
    @prop()
    schemeType!: string
    @prop({ index: true })
    legend!: string;
    @prop()
    riskLevel!: number;
    @prop()
    horizon!: { min: number, max: number };
    @prop()
    loadMonths!: number;
    @prop()
    gfNAV!: string;
    @prop()
    gfDate!: string;
    @prop()
    isRecommended!: false;
    @prop()
    amcCode!: string;
    @prop()
    divReinvest!: string;
    @prop()
    purchaseAllowed!: Boolean;
    @prop()
    redemptionAllowed!: Boolean;
    @prop()
    sipAllowed!: Boolean;
    @prop({ type: Array })
    bseDetails!: [];
    @prop({ type: Returns })
    returns!: Returns;
}

