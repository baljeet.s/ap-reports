import { prop, mongoose, modelOptions, Severity } from "@typegoose/typegoose";

class StatusClass {
    @prop({ default: "EMPTY", match: /^EMPTY$|^SUBMITTED$|^ACCEPTED$|^REJECTED$/, required: true })
    status!: string

    @prop({ default: "EMPTY", match: /^EMPTY$|^NONE$|^OLD$|^EKYC$|^NEW$/ })
    kyc!: string

    @prop()
    rejectionReason?: string

    @prop()
    rejectedParams?: string[]

    @prop()
    rejectionCategories?: string[]
};
export class DeviceClass {
    @prop({ match: /^IOS$|^ANDROID$|^FLUTTER$/ })
    deviceType?: string //IOS or ANDROID
    @prop()
    fcmToken!: string //APNS or FCM token
    @prop()
    appVersionCode?: string
};
@modelOptions({ schemaOptions: { collection: 'users' }, options: { allowMixed: Severity.ALLOW } })
export default class UserClass {

    @prop({ type: mongoose.Schema.Types.ObjectId, required: true })
    id!: mongoose.Schema.Types.ObjectId

    @prop({ required: true })
    firstName!: string

    @prop({ default: "" })
    lastName?: string

    @prop({ unique: true, index: true, match: /^\d{10}$/, sparse: true })
    phoneNumber?: string

    @prop()
    email?: string

    @prop({ index: true })
    panNumber?: string

    @prop({ required: true })
    password!: string

    @prop({ unique: true })
    bseClientCode?: number

    @prop({ type: mongoose.Schema.Types.ObjectId, index: true })
    subAdvisorId?: mongoose.Schema.Types.ObjectId

    @prop({ type: mongoose.Schema.Types.ObjectId, index: true })
    assignedAdminId?: mongoose.Schema.Types.ObjectId

    @prop()
    // activationStatus?: StatusClass

    // Marked as mandatory as it is required in fcmhelper
    @prop()
    device!: DeviceClass

    @prop({ match: /^T15$|^B15/ })
    region?: string

    @prop({ match: /^NA$|^NONE$|^EKYC$|^FULL$/, default: "NA" })
    kycStatus?: string

    @prop()
    lastIssuedAt?: number

    @prop()
    chatbotVersion?: string

    @prop({ default: new Date(1970, 0, 1) })
    activationUpdated?: Date

    @prop({ type: mongoose.Schema.Types.ObjectId })
    refId?: mongoose.Schema.Types.ObjectId

    @prop()
    language?: string

    @prop()
    tracking?: number

    @prop()
    regularPc?: number

    @prop()
    equityPc?: number

    @prop()
    riskScore?: number

    @prop()
    assetAllocation?: number

    @prop()
    priority?: string

    @prop()
    status?: string

    @prop()
    lastSeen?: Date

    @prop()
    terLocation?: string

    @prop({ default: false })
    allowPortfolioAccess?: boolean

    @prop()
    city?: string

    @prop()
    state?: string

    @prop({ type: mongoose.Schema.Types.ObjectId, index: true })
    operatorId?: mongoose.Schema.Types.ObjectId

    @prop({ default: false })
    freezeNAV?: boolean

    @prop({ index: true })
    campaign?: string

}