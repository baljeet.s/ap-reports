import { plainToClass, Type } from "class-transformer";
import moment from "moment";
var xirr = require("xirr");
import { CashFlowItem, FolioModel } from "./Folio";
import { BSEOrder } from "./BSEOrder";
import { Holding } from "./Holding";
import { MFScheme } from "./MFScheme";
import { LatestNAVClass } from "./LatestNAV";
import { prop } from "@typegoose/typegoose";

class Trend {
    @prop()
    date!: string;
    @prop()
    amount!: number;
    @prop()
    value!: number;
}
export class PortfolioClass {

    @prop()
    totalDebtInvestment!: number;
    @prop()
    totalDebtValue!: number;
    @prop()
    debtGrowth!: number;
    @prop()
    totalEquityInvestment!: number;
    @prop()
    totalEquityValue!: number;
    @prop()
    equityGrowth!: number;
    @prop()
    xirrReturns!: number;
    @prop()
    totalUnits!: number;
    @prop()
    totalInvestment!: number;
    @prop()
    assetValue!: number;
    @prop()
    flowTotal!: number;
    @prop()
    growth!: number;
    @prop()
    unrealizedCg!: number;
    @prop()
    divPaid!: number;
    @prop()
    divReinvested!: number;
    @prop()
    dailyChange!: number;
    @prop()
    lockFreeUnits!: number;
    @prop()
    lockFreeValue!: number;
    @prop()
    unrealizedCG!: number;
    @prop()
    stcg!: number;
    @prop()
    stcgTax!: number;
    @prop()
    absoluteReturns!: number;
    @prop()
    debtRatio!: number;
    @prop()
    unprocessedPurchases!: number;
    @prop()
    unprocessedRedemptions!: number;
    @prop()
    unprocessedSwitches!: number;
    @prop({ type: Array<Holding> })
    holdings!: Holding[];
    @prop({ type: Array<CashFlowItem> })
    cashFlows!: CashFlowItem[];
    @prop()
    isXIRRMode!: boolean;
    @prop()
    unrealizedCGIndexed!: number;
    @prop()
    unrealizedSTCG!: number;
    @prop()
    unrealizedLTCG!: number;

    @prop({ type: Array<Trend> })
    trend!: Trend[];
    @prop({ type: Array<Trend> })
    internalTrend?: Trend[];
    @prop({ type: Array<Trend> })
    externalTrend?: Trend[];

    getAllocationRatio(): string {
        if (Object.is(this.debtRatio, NaN))
            return "- : -";
        else
            return `${this.debtRatio} : ${(100 - this.debtRatio)}`;
    }

    processHoldings() {

        this.totalDebtInvestment = this.holdings
            .filter(holding => !["Equity", "Balanced"].includes(holding.legend))
            .map(holding => holding.totalInvestment)
            .reduce((sum, current) => sum + current, 0);
        this.totalDebtValue = this.holdings
            .filter(holding => !["Equity", "Balanced"].includes(holding.legend))
            .map(holding => holding.assetValue)
            .reduce((sum, current) => sum + current, 0);
        this.debtGrowth = this.totalDebtValue - this.totalDebtInvestment;

        this.totalEquityInvestment = this.holdings
            .filter(holding => ["Equity", "Balanced"].includes(holding.legend))
            .map(holding => holding.totalInvestment)
            .reduce((sum, current) => sum + current, 0);
        this.totalEquityValue = this.holdings
            .filter(holding => ["Equity", "Balanced"].includes(holding.legend))
            .map(holding => holding.assetValue)
            .reduce((sum, current) => sum + current, 0);
        this.equityGrowth = this.totalEquityValue - this.totalEquityInvestment;

        this.cashFlows = this.holdings.flatMap(holding => holding.cashFlows)
            .sort((a, b) => a.when.getTime() - b.when.getTime());
        this.isXIRRMode = false;
        this.xirrReturns = 0;
        if (this.cashFlows.length > 0) {
            try {
                // this.xirrReturns = xirr(this.cashFlows, { maxIterations: 10000, tolerance: 0.0000001 });
                this.xirrReturns = xirr(this.cashFlows);
                this.isXIRRMode = moment(this.cashFlows[0].when).isBefore(moment().add(-1, "year"));
            }
            catch (ignored) { }
        }

        this.totalUnits = this.holdings.map(holding => holding.totalUnits).reduce((sum, current) => sum + current, 0);
        this.totalInvestment = this.holdings.map(holding => holding.totalInvestment).reduce((sum, current) => sum + current, 0);
        this.assetValue = this.holdings.map(holding => holding.assetValue).reduce((sum, current) => sum + current, 0);
        this.flowTotal = this.holdings.map(holding => holding.flowTotal).reduce((sum, current) => sum + current, 0);
        this.growth = this.holdings.map(holding => holding.growth).reduce((sum, current) => sum + current, 0);
        this.unrealizedCG = this.holdings.map(holding => holding.unrealizedCG).reduce((sum, current) => sum + current, 0);
        this.unrealizedSTCG = this.holdings.map(holding => holding.unrealizedSTCG).reduce((sum, current) => sum + current, 0);
        this.unrealizedLTCG = this.holdings.map(holding => holding.unrealizedLTCG).reduce((sum, current) => sum + current, 0);
        this.unrealizedCGIndexed = this.holdings.map(holding => holding.unrealizedCGIndexed).reduce((sum, current) => sum + current, 0);
        this.divPaid = this.holdings.map(holding => holding.divPaid).reduce((sum, current) => sum + current, 0);
        this.divReinvested = this.holdings.map(holding => holding.divReinvested).reduce((sum, current) => sum + current, 0);
        this.dailyChange = this.holdings.map(holding => holding.dailyChange).reduce((sum, current) => sum + current, 0);
        this.lockFreeUnits = this.holdings.map(holding => holding.lockFreeUnits).reduce((sum, current) => sum + current, 0);
        this.lockFreeValue = this.holdings.map(holding => holding.lockFreeValue).reduce((sum, current) => sum + current, 0);
        this.stcg = this.holdings.map(holding => holding.stcg).reduce((sum, current) => sum + current, 0);
        this.stcgTax = this.holdings.map(holding => holding.stcgTax).reduce((sum, current) => sum + current, 0);

        this.absoluteReturns = this.growth / this.totalInvestment;
        this.debtRatio = Math.round(100 * this.totalDebtValue / this.assetValue);
    }

    getNonZeroHoldings(): Holding[] {
        return this.holdings.filter(holding => holding.assetValue);
    }

    getCashInput() {
        var realizedGains = this.holdings.map(holding =>
            holding.folios.map(folio =>
                folio.sellQueue.map(item =>
                    item.buyQueue.map(item => item.gain)
                        .reduce((sum, current) => sum + current, 0)
                ).reduce((sum, current) => sum + current, 0)
            ).reduce((sum, current) => sum + current, 0)
        ).reduce((sum, current) => sum + current, 0);
        return this.totalInvestment - realizedGains;
    }
}
