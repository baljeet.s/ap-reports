import { prop } from "@typegoose/typegoose";

export class NavHistoryClass {
    @prop({ index: true })
    code!: string;

    @prop({ index: true })
    date!: string;

    @prop()
    nav!: Number;
}