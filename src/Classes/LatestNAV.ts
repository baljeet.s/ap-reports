import { prop } from "@typegoose/typegoose";

export class LatestNAVClass {
    @prop()
    nav!: string;
    @prop()
    date!: string;
    @prop()
    change!: number;
    @prop()
    isEquity!: boolean;
    @prop()
    legend!: string;
    @prop()
    schemeName!: string;
    @prop()
    schemeCode!: string;
    @prop()
    lockOutPeriod!: number;
    @prop()
    amfiBroad!: string;
    @prop()
    amfiSub!: string;
    @prop()
    gfNAV?: string;
    @prop()
    amcName!: string;

    @prop()
    isin?: string;
    // Used in MF screener
    @prop()
    changeColor?: string;
    @prop()
    changeSymbol?: string;

    getNAV() {
        return parseFloat(this.nav);
    }

    getChangePercentage() {
        return 100 * this.change / (parseFloat(this.nav) - this.change);
    }
}
