import AssetClass from './AssetClass';
var xirr = require("xirr");

export default class OtherAsset {
    name: string;
    type: string;
    totalInvestment: number;
    assetValue: number;
    growth: number;
    unrealizedCG: number;
    realizedCG: number;
    divReinvested: number;
    divPaid: number;
    cashFlows: { amount: number; when: Date; }[];
    xirrReturns: number;
    xirrYrs: number;
    constructor(asset: AssetClass) {
        // Object.assign(this, asset);
        this.name = asset.name;
        this.type = asset.type;

        this.totalInvestment = asset.purchaseAmount;
        this.assetValue = asset.currentValue;
        this.growth = asset.currentValue - asset.purchaseAmount;
        this.unrealizedCG = this.assetValue - this.totalInvestment
        this.realizedCG = 0
        this.divReinvested = 0;
        this.divPaid = asset.dividend ? asset.dividend : asset.dividends.map(div => div.dividend).reduce((sum, current) => sum + current, 0);

        this.cashFlows = [
            { amount: -asset.purchaseAmount, when: new Date(asset.purchaseDate) },
            { amount: asset.currentValue, when: new Date(asset.valuationDate) },
        ]
        if (asset.dividends) {
            this.cashFlows.push(
                ...asset.dividends.map(div => {
                    return { amount: parseFloat(div.dividend.toString()), when: new Date(div.date) };
                })
            )
        }
        this.xirrReturns = 0;
        this.xirrYrs = 0;
        if (this.cashFlows.length > 0) {
            try {
                this.xirrReturns = xirr(this.cashFlows, { maxIterations: 10000, tolerance: 0.0001 });
                this.xirrYrs = Math.log((this.assetValue + this.divPaid) / this.totalInvestment) / Math.log(1 + this.xirrReturns);
            }
            catch (ignored) { }
        }

    }


}