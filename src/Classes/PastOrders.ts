import { prop } from "@typegoose/typegoose";
import mongoose from "mongoose"

export class PastOrders {
    @prop({ type: mongoose.Schema.Types.ObjectId, required: true, index: true })
    userId!: { type: mongoose.Schema.Types.ObjectId, required: true, index: true };
    @prop()
    schemeCode!: string;
    @prop()
    orderId!: { type: string };
    @prop({ match: [/^P$|^R$|^DR$|^DP$|^SI$|^SO$/, "Invalid transaction type"] })
    transactionType!: string;
    @prop()
    folioNo!: string;
    @prop()
    processingDate!: string;
    @prop()
    processingNAV!: string;
    @prop()
    processingUnits!: string;
    @prop()
    processingAmount!: string;
    @prop()
    mode!: string;

    @prop()
    amount?: string;
    @prop()
    createdAt?: string;
    @prop()
    orderStatus?: string;
    @prop()
    isProcessed?: boolean;
    @prop()
    bseOrderDate?: string;
}