import { prop } from "@typegoose/typegoose";
import { Type } from "class-transformer";
import { LatestNAVClass } from "./LatestNAV";
import SchemeReturnsModel from "./SchemeReturnsModel";

class AmountModel {
    @prop()
    min!: number;
    @prop()
    max!: number;
    @prop()
    delta!: number;
}
class InstallmentsModel {
    min!: number;
    max!: number;
}

class SIPSModel {
    @prop({ type: Array<number> })
    dates!: Array<number>;
    @prop({ type: AmountModel, default: new AmountModel() })
    amount!: AmountModel;
    @prop({ type: InstallmentsModel, default: new InstallmentsModel() })
    installments!: InstallmentsModel;

    getInstallments(): SIPSDurationModel[] {
        var durations: SIPSDurationModel[] = [];
        const minInstallments = this.installments.min;
        let maxInstallments = this.installments.max;
        if (maxInstallments === 0 || maxInstallments > 300) maxInstallments = 300;
        for (var i = minInstallments; i <= maxInstallments; i++) {
            if ((i < 24 && i % 6 === 0) || (i >= 24 && i % 12 === 0)) {
                var count = i;
                var label = "";
                if (count < 24 || count % 12 !== 0) {
                    label = count.toString() + " months";
                } else {
                    label = Math.round(count / 12).toString() + " years";
                }
                durations.push(new SIPSDurationModel(count, label));
            }
        }
        return durations;
    }
}


class PurchaseAmountModel {
    @prop({ default: false })
    allowed!: boolean;
    @prop()
    freshMin!: number;
    @prop()
    additionalMin!: number;
    @prop()
    max!: number;
    @prop()
    delta!: number;

    validateDelta(input: number): boolean {
        var purchaseDelta = (this.delta * 1000);
        if (purchaseDelta < 1) purchaseDelta = 1;
        return ((input * 1000)) % purchaseDelta === 0;
    }
}

class RedemptionModel {
    @prop({ default: false })
    allowed!: boolean;
    @prop({ type: AmountModel, default: new AmountModel() })
    quantity!: AmountModel;
    @prop({ type: AmountModel, default: new AmountModel() })
    amount!: AmountModel;

    validateDelta(input: number): boolean {
        var redemptionDelta = (this.amount.delta * 1000);
        if (redemptionDelta < 1) redemptionDelta = 1;
        return ((input * 1000)) % redemptionDelta === 0;
    }
}

class FlagsModel {
    @prop({ default: false })
    sip!: boolean;
    @prop({ default: false })
    stp!: boolean;
    @prop({ default: false })
    swp!: boolean;
    // @J("switch")
    @prop({ default: false })
    switchFlag!: boolean;
}

class ExitLoadModel {
    @prop({ default: false })
    flag!: boolean;
    @prop()
    message!: string;
}

class LockInModel {
    @prop({ default: false })
    flag!: boolean;
    @prop()
    periodInMonths!: number;
}
class BseDetailModel {
    bseCode: string = '';
    @prop({ type: PurchaseAmountModel, default: new PurchaseAmountModel() })
    purchaseAmount!: PurchaseAmountModel;
    @prop({ type: RedemptionModel, default: new RedemptionModel() })
    redemption!: RedemptionModel;
    @prop({ type: FlagsModel, default: new FlagsModel() })
    flags!: FlagsModel;
    @prop({ type: ExitLoadModel, default: new ExitLoadModel() })
    exitLoad!: ExitLoadModel;
    @prop({ type: LockInModel, default: new LockInModel() })
    lockIn!: LockInModel;
    @Type(() => SIPSModel)
    SIPS!: SIPSModel;
}


export class SIPSDurationModel {
    @prop()
    count!: number;
    @prop()
    label!: string;

    constructor(count: number, label: string) {
        this.count = count;
        this.label = label;
    }
}




class HorizonModel {
    @prop()
    min!: number;
    @prop()
    max!: number;
}

class ReturnsModel {
    @prop()
    yrs1!: string;
    @prop()
    yrs3!: string;
    @prop()
    yrs5!: string;
    @prop()
    yrs7!: string;

    getYrs1(): number | null {
        if (this.yrs1 === null) {
            return null;
        }
        return parseFloat(this.yrs1);
    }

    getYrs3(): number | null {
        if (this.yrs3 === null) {
            return null;
        }
        return parseFloat(this.yrs3);
    }

    getYrs5(): number | null {
        if (this.yrs5 === null) {
            return null;
        }
        return parseFloat(this.yrs5);
    }

    getYrs7(): number | null {
        if (this.yrs7 === null) {
            return null;
        }
        return parseFloat(this.yrs5);
    }
}

export class MFScheme {

    @prop()
    schemeCode!: string;
    @prop()
    schemeName!: string;
    @prop()
    aumInLakhs!: number;
    @prop()
    ISIN!: string;
    @prop()
    amcCode!: string;
    @prop()
    amfiBroad!: string;
    @prop()
    amfiSub!: string;
    @prop()
    schemeType!: string;
    @prop()
    riskLevel: number = 0;
    @prop()
    legend!: string;
    @prop({ default: false })
    isRecommended!: boolean;
    @prop({ default: false })
    purchaseAllowed!: boolean;
    @prop({ default: false })
    sipAllowed!: boolean;
    @prop({ type: HorizonModel })
    horizon!: HorizonModel;
    @prop({ type: BseDetailModel })
    bseDetails!: BseDetailModel[];
    @prop({ type: ReturnsModel })
    returns!: ReturnsModel;
    @prop({ type: LatestNAVClass })
    latestNAV!: LatestNAVClass;
    @prop()
    lockOutPeriod?: number;
    @prop({ type: BseDetailModel })
    baseDetail?: BseDetailModel;
    @prop()
    sipDetail!: any;
    @prop()
    loadMonths?: number;
    @prop()
    gfNAV!: string;

    // For MF screener
    @prop()
    nav?: LatestNAVClass;
    @prop()
    yrs3Color?: string;
    @prop()
    yrs5Color?: string;

    // New Addition from api
    @prop()
    historicReturns?: SchemeReturnsModel;

    static schemeCache: { [schemeCode: string]: MFScheme | undefined } = {};

    @prop()
    bseCode?: string;

    getBasicBSEDetail = () => {
        if (!this.baseDetail) {
            var min = Number.MAX_VALUE;
            var minIndex = 0;
            for (var i = 0; i < this.bseDetails.length; i++) {
                var amount = this.bseDetails[i].purchaseAmount.freshMin;
                if (min > amount) {
                    min = amount;
                    minIndex = i;
                }
            }
            this.baseDetail = this.bseDetails[minIndex];
        }
        return this.baseDetail;
    }

    // private transient volatile SIPS sipDetail = null;
    getSIPDetails(): SIPSModel | undefined {
        if (!this.bseDetails || this.bseDetails.length < 1)
            return undefined;

        if (!this.sipDetail) {
            var min = Number.MAX_VALUE;
            for (var i = 0; i < this.bseDetails.length; i++) {
                if (this.bseDetails[i].flags.sip) {
                    if (min > this.bseDetails[i].SIPS.amount.min) {
                        min = this.bseDetails[i].SIPS.amount.min;
                        this.sipDetail = this.bseDetails[i].SIPS;
                    }
                }
            }
        }
        return this.sipDetail;
    }

    getSchemeTypeBroadName() {
        return this.amfiBroad;
    }

    isEquity() {
        return ["Equity", "Balanced"].includes(this.legend);
    }

    getLockOutPeriod = () => {

        if (this.loadMonths != null) {
            return this.loadMonths / 12;
        }
        if (this.schemeType == "ELSS") return 3;
        if (!this.getBasicBSEDetail().exitLoad) {
            console.log(this);
            return 0;
        }
        if (this.getBasicBSEDetail().exitLoad.flag) {
            return 1;
        } else {
            return 0;
        }
    }

    getSchemeGraphLength() {
        //        "1 mth", "6 mths", "1 yr", "3 yrs", "5 yrs", "All"
        switch (this.schemeType) {
            case "GST":
            case "DFR":
            case "LIQUID":
            case "DST":
            case "DUST":
                return "1 yr";
            case "ELSS":
            case "DA":
            case "BALANCED":
            case "FOF":
            case "GLT":
            case "DLT":
            case "DCO":
            case "MIP":
                return "3 yrs";
            case "EI":
            case "ELC":
            case "ESMC":
            case "ED":
            case "ES":
            case "ERGESS":
                return "5 yrs";
        }
        return "1 yr";
    }

    getSchemeHorizon() {
        return MFScheme.getSchemeTypeInvestmentHorizonShort(this.schemeType);
    }

    isFundInHorizon(key: string) {
        var horizon = this.getSchemeHorizon();
        switch (key) {
            case "<6":
                return horizon.first <= 0 && horizon.second >= 6;
            case "6-12":
                return horizon.first <= 6 && horizon.second >= 12;
            case "12-36":
                return horizon.first <= 12 && horizon.second >= 36;
            case "36-60":
                return horizon.first <= 36 && horizon.second >= 60;
            case "60-84":
                return horizon.first <= 60 && horizon.second >= 84;
            case ">84":
                return horizon.first <= 84 && horizon.second >= Number.MAX_VALUE;
        }
        return false;
    }
    getSchemeTypeInvestmentHorizon() {
        return MFScheme.getSchemeTypeInvestmentHorizon(this.schemeType);
    }

    getSchemeWarningMessage() {
        return MFScheme.getSchemeWarningMessage(this.schemeType);
    }

    getSchemeTypeRisk() {
        return MFScheme.getSchemeTypeRisk(this.schemeType);
    }

    getSchemeTypeRiskFullLabel() {
        return MFScheme.getRiskFullLabelForLevel(this.getSchemeTypeRiskLevel());
    }

    getSchemeTypeRiskLevel() {
        return MFScheme.getSchemeTypeRiskLevel(this.schemeType);
    }

    getSchemeRiskColor() {
        switch (this.getSchemeTypeRiskLevel()) {
            case 1: return "#00c221";
            case 2: return "#a2c200";
            case 3: return "#ffcc00";
            case 4: return "#ff8800";
            case 5: return "#ff5100";
            default: return "#ff5100";
        }
    }

    getAMCFullName() {
        return MFScheme.getAMCFullName(this.amcCode);
    }

    getSchemeTypeFullName() {
        return this.amfiBroad + " (" + this.amfiSub + ")";
    }

    getAUMString() {
        if (this.aumInLakhs < 9999) {
            return "₹" + this.aumInLakhs.toFixed(2) + "L";
        }
        else {
            return "₹" + (this.aumInLakhs / 100).toFixed(2) + "Cr";
        }
    }

    getReturns3() {
        return parseFloat(this.returns.yrs3 || "0");
    }

    getReturns5() {
        return parseFloat(this.returns.yrs5 || "0");
    }

    getReturns7() {
        return parseFloat(this.returns.yrs7 || "0");
    }

    getLockInString() {
        var basicDetails = this.getBasicBSEDetail();
        if (basicDetails) {
            if (basicDetails.lockIn.flag) {
                var lockInMonths = basicDetails.lockIn.periodInMonths;
                if (lockInMonths < 12)
                    return (basicDetails.lockIn.periodInMonths + " Mths");
                else
                    return ((basicDetails.lockIn.periodInMonths / 12) + " Yrs");
            }
            else
                return ("NIL");
        }
        else
            return "-"
    }


    static findSchemeByCode(schemeList: Array<MFScheme>, schemeCode: string) {
        // if (!MFScheme.schemeCache[schemeCode])
        //     MFScheme.schemeCache[schemeCode] = schemeList.find(scheme => scheme.schemeCode === schemeCode);
        // return MFScheme.schemeCache[schemeCode];
        return schemeList.find(e => e.schemeCode === schemeCode);
    }

    static getSchemeWarningMessage(schemeType: string) {
        switch (schemeType) {
            case "ELC":
                return "This fund has moderately high risk and is suitable if you are investing for at least 4 years. It is not recommended for short term as it can provide negative returns. It can even have a gain or loss of 3 - 4% in a week";
            case "ED":
                return "This fund has moderately high risk and is suitable if you are investing for at least 5 years. It is not recommended for short term as it can provide negative returns. It can even have a gain or loss of 5 - 6% in a week";
            case "ESMC":
                return "This funds carries high risk and is only recommended for more than 5 - 7 years of investment. It is not recommended for short term as it can provide negative returns. It can even have a gain or loss of 5 - 7% in a week";
            case "ES":
                return "This fund category has the highest risk among equity funds. If the sector does not perform well, it can provide up to 30-40% loss in one year. It is suitable only for an investment period of at least 7 - 8 years";
            case "ELSS":
                return "This fund has a lock in period of 3 years. It is meant for tax saving under Section 80C. Since it is an equity fund, its returns can fluctuate by a large extent in the short term. It can even have a gain or loss of 5 - 7% in a week";
            default:
                return null;
        }
    }

    static getSchemeTypeInvestmentHorizon(schemeType: string) {
        switch (schemeType) {
            case "ED":
                return "More than 5 years";
            case "DLT":
                return "More than 3 years";
            case "LIQUID":
                return "Less than 6 months";
            case "GLT":
                return "More than 3 years";
            case "ESMC":
                return "More than 5 years";
            case "BALANCED":
                return "More than 2.5 years";
            case "GST":
                return "1 - 3 years";
            case "ES":
                return "More than 7 years";
            case "ELC":
                return "More than 3 years";
            case "EI":
                return "More than 3 years";
            case "ELSS":
                return "More than 3 years";
            case "DCO":
                return "More than 3 years";
            case "MIP":
                return "More than 2 years";
            case "DUST":
                return "6 months to 1 year";
            case "DST":
                return "1 - 3 years";
            case "DFR":
                return "1 - 3 years";
            case "FOF":
                return "More than 5 years";
            case "DA":
                return "More than 1 year";
            case "ERGESS":
                return "More than 3 years";
        }
        return schemeType;
    }

    static getSchemeTypeInvestmentHorizonShort(schemeType: string) {
        switch (schemeType) {
            case "ED":
                return { first: 60, second: Number.MAX_VALUE };
            case "DLT":
                return { first: 36, second: 60 };
            case "LIQUID":
                return { first: 0, second: 6 };
            case "GLT":
                return { first: 36, second: 60 };
            case "ESMC":
                return { first: 60, second: Number.MAX_VALUE };
            case "BALANCED":
                return { first: 36, second: 60 };
            case "GST":
                return { first: 12, second: 36 };
            case "ES":
                return { first: 84, second: Number.MAX_VALUE };
            case "ELC":
                return { first: 36, second: 60 };
            case "EI":
                return { first: 36, second: 60 };
            case "ELSS":
                return { first: 36, second: Number.MAX_VALUE };
            case "DCO":
                return { first: 36, second: 60 };
            case "MIP":
                return { first: 12, second: 36 };
            case "DUST":
                return { first: 6, second: 12 };
            case "DST":
                return { first: 12, second: 36 };
            case "DFR":
                return { first: 6, second: 12 };
            case "FOF":
                return { first: 60, second: Number.MAX_VALUE };
            case "DA":
                return { first: 12, second: 36 };
        }
        return { first: 60, second: Number.MAX_VALUE };
    }

    static getSchemeTypeRiskLevel(schemeType: string) {
        switch (schemeType) {
            case "LIQUID":
                return 1;
            case "GST":
            case "DFR":
            case "DA":
            case "DUST":
                return 2;
            case "GLT":
            case "DST":
            case "BALANCED":
            case "DCO":
            case "MIP":
            case "DLT":
                return 3;
            case "EI":
            case "ELC":
            case "ED":
            case "ELSS":
                return 4;
            case "FOF":
            case "ES":
            case "ESMC":
            case "ERGESS":
                return 5;
        }
        return 5;
    }

    static getRiskLabelForLevel(level: number) {
        switch (level) {
            case 1:
                return "Low";
            case 2:
                return "Mod. Low";
            case 3:
                return "Moderate";
            case 4:
                return "Mod. High";
            case 5:
                return "High";
        }
        return "High";
    }

    static getRiskFullLabelForLevel(level: number) {
        switch (level) {
            case 1:
                return "Low";
            case 2:
                return "Moderately Low";
            case 3:
                return "Moderate";
            case 4:
                return "Moderately High";
            case 5:
                return "High";
        }
        return "High";
    }

    static getSchemeTypeRisk(schemeType: string) {
        return MFScheme.getRiskLabelForLevel(MFScheme.getSchemeTypeRiskLevel(schemeType));
    }

    static getAMCFullName(amcCode: string) {
        switch (amcCode) {
            case "FRANKLINTEMPLETON":
                return "Franklin Templeton";
            case "BirlaSunLifeMutualFund_MF":
                return "Aditya Birla Sun Life";
            case "SBIMutualFund_MF":
                return "SBI";
            case "ICICIPrudentialMutualFund_MF":
                return "ICICI Prudential";
            case "UTIMUTUALFUND_MF":
                return "UTI";
            case "RelianceMutualFund_MF":
                return "Nippon India";
            case "HDFCMutualFund_MF":
                return "HDFC";
            case "KOTAKMAHINDRAMF":
                return "Kotak Mahindra";
            case "MOTILALOSWAL_MF":
                return "Motilal Oswal";
            case "PRINCIPALMUTUALFUND_MF":
                return "Principal";
            case "EDELWEISSMUTUALFUND_MF":
                return "Edelweiss";
            case "BNPSUNDARAMPARIBAS_MF":
                return "Sundaram";
            case "BARODAPIONEERMUTUALFUND_MF":
                return "Baroda Pioneer";
            case "CANARAROBECOMUTUALFUND_MF":
                return "Canara Robeco";
            case "TATAMutualFund_MF":
                return "TATA";
            case "RELIGAREMUTUALFUND_MF":
                return "Invesco";
            case "IDBIMUTUALFUND_MF":
                return "IDBI";
            case "BNPPARIBAS_MF":
                return "BNP Paribas";
            case "PPFAS_MF":
                return "Parag Parikh";
            case "DSPBLACKROCK":
                return "DSP";
            case "PEERLESSMUTUALFUND_MF":
                return "Essel";
            case "AXISMUTUALFUND_MF":
                return "Axis";
            case "IIFLMUTUALFUND_MF":
                return "IIFL";
            case "INDIABULLSMUTUALFUND_MF":
                return "India Bulls";
            case "BHARTIAXAMUTUALFUND_MF":
                return "BOI AXA";
            case "IDFCMUTUALFUND_MF":
                return "IDFC";
            case "TAURUSMUTUALFUND_MF":
                return "Tauras";
            case "L&TMUTUALFUND_MF":
                return "L&T";
            case "MIRAEASSET":
                return "Mirae Asset";
            case "HSBCMUTUALFUND_MF":
                return "HSBC";
            case "DHFLPRAMERICAMUTUALFUND_MF":
                return "PGIM";
            case "MAHINDRA MUTUAL FUND_MF":
                return "Mahindra";
            case "QUANTUMMUTUALFUND_MF":
                return "Quantum";
            case "QUANTMUTUALFUND_MF":
                return "Quant";
            case "LICMUTUALFUND_MF":
                return "LIC";
            case "JM FINANCIAL MUTUAL FUND_MF":
                return "JM Financial";
            case "UNIONMUTUALFUND_MF":
                return "Union";
            case "ITI MUTUAL FUND_MF":
                return "ITI";
        }
        return amcCode;
    }

}

MFScheme.prototype