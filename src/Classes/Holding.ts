import { prop } from "@typegoose/typegoose";
import { Type } from "class-transformer";
import AlarmModel from "./AlarmModel";
import { CashFlowItem, FolioModel } from "./Folio";

/**
 * Created by awanish on 10/10/16.
 */
var xirr = require("xirr");

export class Holding {

    @prop()
    isAssetPlus!: boolean;
    @prop()
    schemeCode!: string;
    @prop()
    isin!: string;
    @prop()
    schemeName!: string;
    @prop()
    nav!: number;
    @prop()
    navDate!: string;
    @prop()
    navChange!: number;
    @prop()
    lockInPeriod!: number;
    @prop()
    amfiBroad!: string;
    @prop()
    amfiSub!: string;
    @prop()
    legend!: string;
    @prop()
    amcCode!: string;
    @prop({ type: Array<FolioModel> })
    folios!: FolioModel[];
    @prop()
    totalUnits!: number;
    @prop()
    totalInvestment!: number;
    @prop()
    assetValue!: number;
    @prop()
    flowTotal!: number;
    @prop()
    growth!: number;
    @prop()
    unrealizedCG!: number;
    @prop()
    unrealizedCGIndexed!: number;
    @prop()
    divPaid!: number;
    @prop()
    divReinvested!: number;
    @prop()
    unrealizedSTCG!: number;
    @prop()
    unrealizedLTCG!: number;
    @prop()
    dailyChange!: number;
    @prop()
    lockFreeUnits!: number;
    @prop()
    lockFreeValue!: number;
    @prop()
    stcg!: number;
    @prop()
    stcgTax!: number;
    @prop({ type: Array<CashFlowItem> })
    cashFlows!: CashFlowItem[];
    @prop()
    xirrReturns!: number;
    @prop()
    absoluteReturns!: number;

    @prop()
    alarms?: AlarmModel[];

    processFolios() {
        this.totalUnits = this.folios.map(folio => folio.totalUnits).reduce((sum, current) => sum + current, 0);
        this.totalInvestment = this.folios.map(folio => folio.totalInvestment).reduce((sum, current) => sum + current, 0);
        this.assetValue = this.folios.map(folio => folio.assetValue).reduce((sum, current) => sum + current, 0);
        this.flowTotal = this.folios.map(folio => folio.flowTotal).reduce((sum, current) => sum + current, 0);
        this.growth = this.folios.map(folio => folio.growth).reduce((sum, current) => sum + current, 0);
        this.unrealizedCG = this.folios.map(folio => folio.unrealizedCG).reduce((sum, current) => sum + current, 0);
        this.unrealizedCGIndexed = this.folios.map(folio => folio.unrealizedCGIndexed).reduce((sum, current) => sum + current, 0);
        this.unrealizedSTCG = this.folios.map(folio => folio.unrealizedSTCG).reduce((sum, current) => sum + current, 0);
        this.unrealizedLTCG = this.folios.map(folio => folio.unrealizedLTCG).reduce((sum, current) => sum + current, 0);
        this.divPaid = this.folios.map(folio => folio.divPaid).reduce((sum, current) => sum + current, 0);
        this.divReinvested = this.folios.map(folio => folio.divReinvested).reduce((sum, current) => sum + current, 0);
        this.dailyChange = this.folios.map(folio => folio.dailyChange).reduce((sum, current) => sum + current, 0);
        this.lockFreeUnits = this.folios.map(folio => folio.lockFreeUnits).reduce((sum, current) => sum + current, 0);
        this.lockFreeValue = this.folios.map(folio => folio.lockFreeValue).reduce((sum, current) => sum + current, 0);
        this.stcg = this.folios.map(folio => folio.stcg).reduce((sum, current) => sum + current, 0);
        this.stcgTax = this.folios.map(folio => folio.stcgTax).reduce((sum, current) => sum + current, 0);

        this.absoluteReturns = (this.growth / this.totalInvestment);


        this.cashFlows = [];
        this.folios.forEach(folio => {
            if (folio.totalInvestment > 0) this.cashFlows.push(...folio.cashFlows);
        });

        // this.cashFlows = this.folios.flatMap(folio => folio.cashFlows)
        //     .sort((a, b) => +a.when - +b.when);

        this.xirrReturns = 0;
        if (this.cashFlows.length > 0) {
            try {
                this.xirrReturns = xirr(this.cashFlows, { maxIterations: 10000, tolerance: 0.0000001 });
            }
            catch (ignored) {
            }
        }
    }
}

export type RedemptionAPIRequest = {
    userId: string;
    schemeCode: string;
    folioNo: string;
    amount: string;
    allUnits: boolean;
    units: string | null
}

export type ExternalRedemptionAPIRequest = {
    userId: string,
    bseSchemeCode: string,
    schemeName?: string,
    amount: string,
    allUnits: boolean,
    folioNo: string,
    units: string | null
}

export type ExternalSwitchAPIRequest = {
    userId: string,
    fromBseSchemeCode: string,
    fromSchemeName: string,
    toSchemeCode: string,
    amount: string,
    units: string | null,
    allUnits: boolean,
    isAdditional: boolean,
    folioNo: string
}