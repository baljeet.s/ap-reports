import { prop } from "@typegoose/typegoose";

export default class SchemeReturnsModel {
    @prop()
    days1?: string;
    @prop()
    mths1?: string;
    @prop()
    mths6?: string;
    @prop()
    yrs1?: string;
    @prop()
    yrs2?: string;
    @prop()
    yrs3?: string;
    @prop()
    yrs5?: string;
    @prop()
    yrs7?: string;
    @prop()
    yrs10?: string;
}