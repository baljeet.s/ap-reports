import { mongoose, prop } from "@typegoose/typegoose"

class Config {
    @prop({ type: Boolean, default: true })
    drawer_tax_saving!: Boolean;
    @prop({ type: Boolean, default: true })
    drawer_top_mfs!: Boolean;
    @prop({ type: Boolean, default: true })
    drawer_all_mfs!: Boolean;
    @prop({ type: Boolean, default: true })
    drawer_tracker!: Boolean;
    @prop({ type: Boolean, default: true })
    drawer_compare!: Boolean;
    @prop({ type: Boolean, default: true })
    drawer_learn!: Boolean;
    @prop({ type: Boolean, default: true })
    drawer_calculator!: Boolean;
    @prop({ type: Boolean, default: true })
    drawer_watchlist!: Boolean;
    @prop({ type: Boolean, default: true })
    drawer_social!: Boolean;
    @prop({ type: Boolean, default: false })
    notify_client_sip_due!: Boolean;
    @prop({ type: Boolean, default: false })
    notify_payment_link!: Boolean;
    @prop({ type: Boolean, default: true })
    dashboard_daily_gains!: Boolean;
    @prop({ type: Boolean, default: true })
    holdings_switch!: Boolean;
    @prop({ type: Boolean, default: true })
    holdings_redeem!: Boolean;
    @prop({ type: Boolean, default: true })
    sips_modify!: Boolean;
    @prop({ type: Boolean, default: true })
    support_learn!: Boolean;
    @prop({ type: Boolean, default: true })
    support_videos!: Boolean;
    @prop({ type: Boolean, default: false })
    activation_force!: Boolean;
    @prop({ type: Boolean, default: false })
    mandates_paper_only!: Boolean;
    @prop({ type: Boolean, default: true })
    allow_upi!: Boolean;
    @prop({ type: String, default: "25000" })
    mandates_limit!: string;
    @prop({ type: Boolean, default: false })
    trigger_signup!: Boolean;
    @prop({ type: Boolean, default: true })
    trigger_activation!: Boolean;
    @prop({ type: Boolean, default: false })
    trigger_first_investment!: Boolean;
    @prop({ type: Boolean, default: false })
    trigger_portfolio_upload!: Boolean;
    @prop({ type: Boolean, default: true })
    trigger_mandate_approved!: Boolean;
    @prop({ type: Boolean, default: true })
    trigger_mandate_rejected!: Boolean;
    @prop({ type: Boolean, default: true })
    trigger_kyc_online_rejection!: Boolean;
    @prop({ type: Boolean, default: true })
    trigger_lumpsum_failure!: Boolean;
    @prop({ type: Boolean, default: true })
    trigger_sip_failure!: Boolean;
    @prop({ type: Boolean, default: true })
    trigger_birthday!: Boolean;
    @prop({ type: Boolean, default: true })
    product_fd!: Boolean;

}

export default class PartnerClass {
    @prop({ type: mongoose.Schema.Types.ObjectId, required: true, index: true })
    adminId!: mongoose.Schema.Types.ObjectId;
    @prop()
    title!: String;
    @prop()
    subtitle!: String;
    @prop()
    description!: String;
    @prop()
    image_splash!: String;
    @prop()
    image_banner!: String;
    @prop()
    image_logo!: String;
    @prop({ type: Config })
    config!: Config;
}

