import { prop } from "@typegoose/typegoose";

export default class AlarmModel {
    @prop()
    condition!: string;
    @prop()
    current!: number;
    @prop()
    isInternal!: boolean;
    @prop()
    parameter!: string;
    @prop()
    schemeCode!: string;
    @prop()
    schemeName!: string;
    @prop()
    value!: number;
    @prop()
    _id!: string;

    static parameters = [
        { value: "gains", label: "Gains" },
        { value: "loadFree", label: "Load Free" },
        { value: "xirrReturns", label: "XIRR Returns" },
        { value: "absoluteReturns", label: "Absolute Returns" },
    ];

    static conditions = [
        { value: "goesAbove", label: "Goes above" },
        { value: "goesBelow", label: "Goes below" }
    ]
}

export class AlarmAPIAddModel {
    @prop()
    schemeCode!: string;
    @prop()
    schemeName!: string;
    @prop()
    condition!: string;
    @prop()
    parameter!: string;
    @prop()
    current!: number;
    @prop()
    value!: number;
    @prop()
    isInternal!: boolean;
}