import { mongoose, prop } from "@typegoose/typegoose"

export default class AssetClass {
    @prop({ type: mongoose.Schema.Types.ObjectId, required: true, index: true })
    userId!: mongoose.Schema.Types.ObjectId;
    @prop({ type: String, required: true })
    name!: string;
    @prop({ type: String, required: true })
    type!: string;
    @prop()
    purchaseAmount!: number;
    @prop()
    purchaseDate!: Date;
    @prop()
    currentValue!: number;
    @prop()
    valuationDate!: Date;
    @prop()
    dividend?: number;
    @prop({ type: Array<Dividend> })
    dividends!: Dividend[];

    @prop()
    xirr!: number;
}

export class Dividend {
    @prop()
    dividend!: number;
    @prop()
    date!: Date;
}