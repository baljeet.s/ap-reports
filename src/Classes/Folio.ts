import { prop } from "@typegoose/typegoose";
import { plainToClass, Type } from "class-transformer";
import moment from "moment";
var xirr = require("xirr");
import "reflect-metadata";

var ciiMap: any = {
    "2001": 100,
    "2002": 105,
    "2003": 109,
    "2004": 113,
    "2005": 117,
    "2006": 122,
    "2007": 129,
    "2008": 137,
    "2009": 148,
    "2010": 167,
    "2011": 184,
    "2012": 200,
    "2013": 220,
    "2014": 240,
    "2015": 254,
    "2016": 264,
    "2017": 272,
    "2018": 280,
    "2019": 289,
    "2020": 301,
    "2021": 317,
    "2022": 331,
}


function getFY(date: string): number {
    var dateMoment = moment(date, "YYYY-MM-DD");
    var thresholdMoment = moment(dateMoment).set({ date: 1, month: 3 });
    var fyYear;
    if (dateMoment.isSameOrAfter(thresholdMoment, "days")) {
        fyYear = dateMoment.format("YYYY");
    } else {
        fyYear = dateMoment.add(-1, "years").format("YYYY");
    }
    return parseInt(fyYear);
}

export class OrderModel {

    @prop()
    transactionType!: string;
    @prop()
    navDate!: string;
    @prop()
    nav!: number;
    @prop()
    units!: string;
    @prop()
    amount!: string;
    @prop()
    folioNo!: string;

    @prop()
    buyValue!: string;
    @prop()
    schemeName?: string;

    getTransactionTypeLabel(): string {
        switch (this.transactionType) {
            case "P":
                return "Purchase";
            case "R":
                return "Redemption";
            case "SI":
                return "Switch In";
            case "SO":
                return "Switch Out";
            case "DR":
                return "Dividend Reinvested";
            case "DP":
                return "Dividend Payout";
        }
        return this.transactionType;
    }

}

class UnitsQueueItemModel {
    @prop()
    units_10000!: number;
    @prop()
    nav!: number;
    @prop()
    date!: string;
    @prop()
    type!: string;
}

class SellQueueItemModel {
    @prop()
    sellAmount!: string;
    @prop()
    sellDate!: string;
    @prop()
    sellUnits!: number;
    @prop()
    sellNav!: number;
    @prop({ type: Array<BuyQueueItemModel> })
    buyQueue!: BuyQueueItemModel[];
}

class BuyQueueItemModel {
    @prop()
    sellAmount!: string;
    @prop()
    sellDate!: string;
    @prop()
    sellUnits!: number;
    @prop()
    sellNav!: number;
    @prop()
    buyNav!: number;
    @prop()
    buyDate!: string;
    @prop()
    originalNav!: number;
    @prop()
    age!: number;
    @prop()
    gain!: number;
    @prop()
    stcg!: number;
    @prop()
    ltcg!: number;
    @prop()
    taxation!: string;
}

export class CashFlowItem {

    @prop()
    amount!: number;
    // @Type(() => Date)
    // when = new Date();
    @prop({ type: Date, default: new Date() })
    when!: Date;


    // static _convertToDate(str) {
    //     return parseDate(str, "yyyy-MM-dd");
    // }

}

class Trend {
    @prop()
    date!: string;
    @prop()
    amount!: number;
    @prop()
    value?: number;
    @prop()
    navdate?: string;
    @prop()
    units_10000?: number;
}

export class FolioModel {

    @prop()
    folioNo!: string;
    @prop()
    schemeCode!: string;
    @prop()
    schemeName!: string;
    @prop()
    nav!: string;
    @prop()
    navDate!: string;
    @prop()
    navChange!: number;
    @prop()
    lockInPeriod!: number;
    @prop()
    amfiBroad!: string;
    @prop()
    amfiSub!: string;
    @prop()
    legend!: string;
    @prop({ type: Array<OrderModel> })
    orders!: OrderModel[];
    @prop({ type: Array<UnitsQueueItemModel> })
    unitsQueue!: UnitsQueueItemModel[];
    @prop({ type: Array<SellQueueItemModel> })
    sellQueue!: SellQueueItemModel[];
    @prop({ type: Array<CashFlowItem> })
    cashFlows!: CashFlowItem[];
    @prop()
    xirr!: number;
    @prop()
    divPaid!: number;
    @prop()
    totalUnits!: number;
    @prop()
    totalInvestment!: number;
    @prop()
    divReinvested!: number;
    @prop()
    assetValue!: number;
    @prop()
    unrealizedCG!: number;
    @prop()
    unrealizedSTCG!: number;
    @prop()
    unrealizedLTCG!: number;
    @prop()
    growth!: number;
    @prop()
    flowTotal!: number;
    @prop()
    dailyChange!: number;
    @prop()
    lockFreeUnits!: number;
    @prop()
    lockFreeValue!: number;
    @prop()
    unrealizedCGIndexed!: number;
    @prop()
    stcg!: number;
    @prop()
    stcgTax!: number;
    @prop()
    gfNAV!: number;

    @prop()
    xirrReturns!: number;
    @prop()
    absoluteReturns!: number;
    @prop({ type: Array<Trend> })
    trend!: Trend[];

    static getNavReferenceDate() {
        var currentMoment = moment();
        var thresholdTime = moment().set({ hours: 21, minutes: 0, seconds: 0 });
        if (currentMoment.isBefore(thresholdTime)) {
            currentMoment.add(-1, "day");
        }
        return currentMoment.format("YYYY-MM-DD");
    }

    processOrders() {
        this.unitsQueue = [];
        this.sellQueue = [];
        this.cashFlows = [];
        this.xirr = 0;

        this.divPaid = 0;
        this.divReinvested = 0;

        this.orders.forEach(order => {

            if (["P", "SI", "DR"].includes(order.transactionType)) {
                this.unitsQueue.push({
                    units_10000: Math.round(parseFloat(order.units) * 10000),
                    nav: order.nav,
                    date: order.navDate,
                    type: order.transactionType
                });
            }
            // if (["DR"].includes(order.transactionType)) {
            //     this.divReinvested += parseFloat(order.amount);
            // }
            if (["DP"].includes(order.transactionType)) {
                this.divPaid += parseFloat(order.amount);
            }
            if (["R", "SO"].includes(order.transactionType)) {
                var soldUnits_10000 = Math.round(parseFloat(order.units) * 10000);
                var sellQueueItem = plainToClass(SellQueueItemModel, {
                    sellAmount: order.amount,
                    sellDate: order.navDate,
                    sellUnits: soldUnits_10000 / 10000,
                    sellNav: order.nav,
                    buyQueue: []
                });
                while (soldUnits_10000 !== 0) {
                    var head = this.unitsQueue[0];
                    if (!head) {
                        //Units have gone negative
                        break;
                    }
                    if (head.units_10000 <= soldUnits_10000) {
                        sellQueueItem.buyQueue.push({
                            sellAmount: order.amount,
                            sellDate: order.navDate,
                            sellUnits: head.units_10000 / 10000,
                            sellNav: order.nav,
                            buyNav: head.nav,
                            buyDate: head.date,
                            originalNav: head.nav,
                            age: 0,
                            gain: 0,
                            stcg: 0,
                            ltcg: 0,
                            taxation: ''
                        });
                        soldUnits_10000 -= head.units_10000;
                        this.unitsQueue.shift();
                    } else {
                        sellQueueItem.buyQueue.push({
                            sellAmount: order.amount,
                            sellDate: order.navDate,
                            sellUnits: soldUnits_10000 / 10000,
                            sellNav: order.nav,
                            buyNav: head.nav,
                            buyDate: head.date,
                            originalNav: head.nav,
                            age: 0,
                            gain: 0,
                            stcg: 0,
                            ltcg: 0,
                            taxation: ''
                        });
                        head.units_10000 -= soldUnits_10000;
                        soldUnits_10000 = 0;
                    }
                }

                this.sellQueue.push(sellQueueItem);
            }

            if (["P", "SI"].includes(order.transactionType)) {
                this.cashFlows.push({
                    amount: -parseFloat(order.amount),
                    when: new Date(order.navDate)
                })
            }
            if (["R", "SO", "DP"].includes(order.transactionType)) {
                this.cashFlows.push({
                    amount: parseFloat(order.amount),
                    when: new Date(order.navDate)
                })
            }
            if (this.unitsQueue.length === 0) {
                this.cashFlows = [];
            }
        })

        this.totalUnits = this.unitsQueue.map(item => item.units_10000).reduce((sum, current) => sum + current, 0) / 10000;
        this.totalInvestment = this.unitsQueue
            .filter(item => ["P", "SI"].includes(item.type))
            .map(item => (item.units_10000 / 10000) * item.nav)
            .reduce((sum, current) => sum + current, 0);

        this.divReinvested = this.unitsQueue
            .filter(item => ["DR"].includes(item.type))
            .map(item => (item.units_10000 / 10000) * item.nav)
            .reduce((sum, current) => sum + current, 0);

        this.assetValue = this.totalUnits * parseFloat(this.nav);


        /**
         * Capital Gains Report
         */
        this.sellQueue.forEach(sellParentItem => {
            sellParentItem.buyQueue.forEach(sellItem => {
                sellItem.age = moment(sellItem.sellDate, "YYYY-MM-DD").diff(moment(sellItem.buyDate, "YYYY-MM-DD"), "years", true);
                sellItem.gain = (sellItem.sellNav - sellItem.buyNav) * sellItem.sellUnits;

                sellItem.stcg = 0;
                sellItem.ltcg = 0;
                sellItem.taxation = "";

                if (["Equity", "Balanced"].includes(this.legend)) {

                    //Equity Funds
                    sellItem.taxation = "EQUITY";

                    if (sellItem.age < 1) {
                        //Short Term Gains
                        sellItem.stcg = sellItem.gain;
                    } else {
                        //Long Term Gains
                        if (sellItem.sellDate < "2018-04-01") {
                            //Sold before 1st April 2018
                            sellItem.ltcg = 0;
                        } else {
                            //Sold after 1st April 2018
                            if (sellItem.buyDate < "2018-01-31") {
                                //Bought before 1st Feb 2018
                                sellItem.buyNav = Math.max(sellItem.buyNav, Math.min(this.gfNAV, sellItem.sellNav));
                            }
                            sellItem.ltcg = (sellItem.sellNav - sellItem.buyNav) * sellItem.sellUnits;
                        }
                    }
                } else {
                    //Debt Funds
                    sellItem.taxation = "DEBT";

                    if (sellItem.age < 3) {
                        sellItem.stcg = sellItem.gain;
                    } else {

                        var sellFY = getFY(sellItem.sellDate);
                        var buyFY = Math.max(getFY(sellItem.buyDate), 2001);

                        sellItem.buyNav = sellItem.buyNav * ciiMap[`${sellFY}`] / ciiMap[`${buyFY}`];

                        sellItem.ltcg = (sellItem.sellNav - sellItem.buyNav) * sellItem.sellUnits;

                    }
                }
            });
        });

        var taxation = "";

        /**
         * Unrealized capital gains with grandfathering
         */
        this.unrealizedCGIndexed = 0;

        this.unrealizedSTCG = 0;
        this.unrealizedLTCG = 0;

        var nowDate = moment().format("YYYY-MM-DD");
        plainToClass(UnitsQueueItemModel, JSON.parse(JSON.stringify(this.unitsQueue)) as []).forEach((buyItem: UnitsQueueItemModel) => {
            var age = moment().diff(moment(buyItem.date, "YYYY-MM-DD"), "years", true);
            var gain = (parseFloat(this.nav) - buyItem.nav) * buyItem.units_10000 / 10000.0;

            var stcg = 0;
            var ltcg = 0;

            if (["Equity", "Balanced"].includes(this.legend)) {

                //Equity Funds
                taxation = "EQUITY";

                if (age < 1) {
                    //Short Term Gains
                    stcg = gain;
                } else {
                    //Long Term Gains
                    if (nowDate < "2018-04-01") {
                        //Sold before 1st April 2018
                        ltcg = 0;
                    } else {
                        //Sold after 1st April 2018
                        if (buyItem.date < "2018-01-31") {
                            //Bought before 1st Feb 2018
                            buyItem.nav = Math.max(buyItem.nav, Math.min(this.gfNAV, parseFloat(this.nav)));
                        }
                        ltcg = (parseFloat(this.nav) - buyItem.nav) * buyItem.units_10000 / 10000.0;
                    }
                }
            } else {
                //Debt Funds
                taxation = "DEBT";

                if (age < 3) {
                    stcg = gain;
                } else {

                    var sellFY = getFY(nowDate);
                    var buyFY = Math.max(getFY(buyItem.date), 2001);

                    buyItem.nav = buyItem.nav * ciiMap["" + sellFY] / ciiMap["" + buyFY];

                    ltcg = (parseFloat(this.nav) - buyItem.nav) * buyItem.units_10000 / 10000.0;
                }
            }

            this.unrealizedSTCG += stcg;
            this.unrealizedLTCG += ltcg;

            this.unrealizedCGIndexed += stcg + ltcg;
        });


        this.unrealizedCG = this.assetValue - this.totalInvestment - this.divReinvested;
        this.growth = this.unrealizedCG + this.divReinvested;

        //XIRR Calculation
        this.cashFlows.push({
            amount: this.assetValue,
            when: new Date(this.navDate)
        });
        this.flowTotal = this.cashFlows.map(item => item.amount).reduce((sum, current) => sum + current, 0);

        this.dailyChange = 0;

        if (this.navDate >= FolioModel.getNavReferenceDate()) {
            var dailyChangeUnits = this.unitsQueue
                .filter(item => item.date < this.navDate)
                .map(item => item.units_10000)
                .reduce((sum, current) => sum + current, 0) / 10000;
            this.dailyChange = this.navChange * dailyChangeUnits;
        }

        var lockInThreshold = moment().add(-this.lockInPeriod, "years");
        this.lockFreeUnits = this.unitsQueue
            .filter(item => moment(item.date, "YYYY-MM-DD").isBefore(lockInThreshold))
            .map(item => item.units_10000)
            .reduce((sum, current) => sum + current, 0) / 10000;

        this.lockFreeValue = this.lockFreeUnits * parseFloat(this.nav);

        var shortTermDuration = ["Equity", "Balanced"].includes(this.legend) ? 1 : 3;
        var shortTermThreshold = moment().add(-shortTermDuration, "years");
        this.stcg = this.unitsQueue.filter(item => moment(item.date, "YYYY-MM-DD").isSameOrAfter(shortTermThreshold))
            .map(item => item.units_10000 / 10000 * (parseFloat(this.nav) - item.nav))
            .reduce((sum, current) => sum + current, 0)


        if (taxation === "DEBT") {
            this.stcgTax = (0.30 * this.stcg);
        }
        if (taxation === "EQUITY") {
            this.stcgTax = (0.15 * this.stcg);
        }

        // this.averageDuration = this.unitsQueue.map(item => (item.units_10000 / 10000) * item.nav * moment().diff(moment(item.date, "YYYY-MM-DD"), "days")).reduce((sum, current) => sum + current, 0) / this.totalInvestment;
        this.absoluteReturns = (this.growth / this.totalInvestment);
        this.xirrReturns = 0;
        if (this.cashFlows.length > 0) {
            try {
                // this.xirrReturns = xirr(this.cashFlows, { maxIterations: 10000, tolerance: 0.0000001 });
                this.xirrReturns = xirr(this.cashFlows);
            }
            catch (ignored) {
            }
        }
    }

}