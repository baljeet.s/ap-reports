import { prop } from "@typegoose/typegoose";
import { Type } from "class-transformer";
import moment from "moment";
import mongoose from "mongoose";
import User from "./User";


export class BSEOrder {

    @prop({ type: mongoose.Schema.Types.ObjectId, required: true })
    _id!: mongoose.Schema.Types.ObjectId;
    @prop({ type: mongoose.Schema.Types.ObjectId, required: true })
    userId!: mongoose.Schema.Types.ObjectId;
    @prop()
    schemeCode!: string;
    @prop()
    amcCode!: string;
    @prop()
    bseOrderId!: string;
    @prop()
    bseOrderDate!: string;
    @prop()
    payoutDate!: string;
    @prop()
    schemeName!: string;
    @prop()
    bseSchemeCode?: string;
    @prop()
    transactionType!: string;
    @prop()
    bseTransactionId?: string;
    @prop()
    amount!: string;
    @prop()
    units?: string;
    @prop()
    folioNo!: string;
    @prop()
    orderStatus!: string;
    @prop()
    orderRemark!: string;
    @prop()
    isProcessed!: boolean;
    @prop()
    processingDate?: string;
    @prop()
    processingNAV!: string;
    @prop()
    processingUnits!: string;
    @prop()
    processingAmount!: string;
    @prop()
    processingRemarks?: string;
    @prop()
    createdAt!: string;
    @prop()
    modifiedAt?: string;
    @prop()
    updatedAt!: string;
    @prop()
    isSIP?: boolean;
    @prop()
    sipId?: string;
    @prop()
    paymentSuccess!: boolean;
    @prop({ type: User })
    user!: User;


    // For displaying in CSV
    @prop()
    orderId!: string;

    getFolioNo(): string {
        if (this.folioNo != null) {
            return this.folioNo;
        } else {
            return "N/A";
        }
    }

    getTransactionTypeCode(): string {
        return this.transactionType;
    }

    getOrderDate_ddmmyyyy(): string {
        return this.bseOrderDate;
    }

    getOrderDate_iso(): string {
        return this.bseOrderDate.split("/").reverse().join("-");
    }

    getProcessingNAV(): string {
        return this.processingNAV;
    }

    getProcessingUnits(): string {
        return this.processingUnits;
    }

    getOrderStatus(): string {
        return this.getOrderStatusLabel();
    }

    getOrderType(): string {
        if (this.transactionType == "P" && !this.isSIP)
            return "lumpsum";
        if (this.transactionType == "P" && this.isSIP)
            return 'sip';
        if (this.transactionType == "R")
            return 'redemption';
        if (["SI", "SO"].includes(this.transactionType))
            return 'switch';
        if (["DP", "DR"].includes(this.transactionType))
            return 'dividend';
        return 'lumpsum';
    }

    getPaymentStatus(): string {

        var lexDate = this.payoutDate ? moment(this.payoutDate, "DD/MM/YYYY").isBefore(moment()) : false;

        if (this.transactionType == "R")
            return "redemption";
        if (this.transactionType == "P" && !this.paymentSuccess && this.orderStatus != "INVALID")
            return "awaiting";

        //(Awanish) - Should successful not include isProcessed or orderStatus
        if (this.transactionType != "P" || this.paymentSuccess)
            return "successful"
        if ((this.transactionType != "P" || this.paymentSuccess) && !this.isProcessed && lexDate && this.isOrderValid())
            return "pending";
        if (!this.isOrderValid())
            return "failed"
        if ((this.orderStatus != "INVALID") && !this.isProcessed)
            return "under_process"
        if (this.isSIP && this.transactionType != "P" || this.paymentSuccess)
            return "sip_orders"
        if (!this.isSIP && this.transactionType != "P" || this.paymentSuccess)
            return "non_sip_orders"
        if (["SO", "SI"].includes(this.transactionType))
            return "switches"
        if (["DR", "DP"].includes(this.transactionType))
            return "dividends"

        return 'pending';
    }

    getOrderStatusLabel(): string {
        if (!this.isOrderValid()) {
            return "Failed";
        }
        if (/incomplete/gi.test(this.orderRemark)) {
            return "Incomplete";
        }
        if (!this.isProcessed) {
            return "Under Process";
        }
        return "Completed";
    }

    getStatusColor(): string {
        if (!this.isOrderValid()) {
            return "#d91c00";
        }

        if (/incomplete/gi.test(this.orderRemark)) {
            return "#ff8800";
        }

        if (!this.isProcessed) {
            return "#00ADFF";
        }

        return "#12ab00";
    }


    getProcessingAmountRoundedWithSign(): number {
        return (this.isPurchase() ? 1 : -1) * (parseFloat(this.processingAmount) || (parseFloat(this.processingNAV) * parseFloat(this.processingUnits)))
    }

    getProcessingAmount(): string {
        return this.processingAmount;
    }

    getOrderAmount(): number {
        return parseFloat(this.amount);
    }

    getProcessingAmountWithSign(): number {
        let amount: number = parseFloat(this.processingUnits) * parseFloat(this.processingNAV);
        if (this.processingAmount !== null && parseFloat(this.processingAmount) !== 0) {
            amount = parseFloat(this.processingAmount);
        }
        return (this.isPurchase() ? 1 : -1) * amount;
    }

    getNAVDate(): string {
        if (this.bseOrderDate != null && this.bseOrderDate.includes("/")) {
            return moment(this.getOrderDate()).format("d-MMM-yyyy");
        } else {
            return "-";
        }
    }

    isAmountCredited(): boolean {
        if (this.transactionType === "R") {
            return moment().isAfter(moment(this.payoutDate));
        } else {
            return true;
        }
    }

    getCompletionDate(): string {
        if (this.payoutDate != null && this.payoutDate.includes("/")) {
            return moment(this.payoutDate, "dd/MM/yyyy").format("d MMM");
        }
        return "-";
    }

    getProcessingDate(): Date {
        return moment(this.processingDate, "YYYY-MM-DD").toDate();
    }

    getOrderDate(): Date {
        return moment(this.bseOrderDate, "DD/MM/YYYY").toDate();
    }

    isPurchase(): boolean {
        return ["P", "SI", "DR"].includes(this.transactionType);
    }

    isRedemption(): boolean {
        return ["R", "DP", "SO"].includes(this.transactionType);
    }

    getOrderAmountWithSign(): number {
        if (this.isPurchase()) {
            return parseFloat(this.amount);
        } else {
            return -parseFloat(this.amount);
        }
    }

    getProcessingUnitsWithSign(): string {
        if (this.isPurchase()) {
            return "" + this.processingUnits;
        } else {
            return "-" + this.processingUnits;
        }
    }

    isAssetPlus(): boolean {
        return true;
    }

    getTransactionType(): string {
        switch (this.transactionType) {
            case "P":
                if (this.isSIP !== undefined && this.isSIP)
                    return "SIP";
                else
                    return "Lumpsum";
            case "R":
                return "Redemption";
            case "SI":
                return "Switch In";
            case "SO":
                return "Switch Out";
            case "DR":
                return "Div Reinv";
            case "DP":
                return "Div Pay";
        }
        return "Txn";
    }

    isOrderValid(): boolean {
        return this.orderStatus === undefined || this.orderStatus === "VALID";
    }

    isOrderUnprocessed(): boolean {
        return this.isOrderValid() && !this.isProcessed && (this.orderRemark === undefined || !this.orderRemark.toLowerCase().includes("incomplete"));
    }

    isOrderProcessed(): boolean {
        return this.isOrderValid() && this.isProcessed;
    }

    getSchemeCode(): string {
        return this.schemeCode;
    }

    static isInvested(response: BSEOrder[], schemeCode: string): boolean {
        return response.find(
            (order) => order.isOrderValid() && order.schemeCode === schemeCode) !== undefined;
    }

}