import { getModelForClass, Severity } from "@typegoose/typegoose";
import User from "../Classes/User";
import Mongoose from "mongoose";
let database: Mongoose.Connection;
import dotenv from "dotenv";
import { BSEOrder } from "../Classes/BSEOrder";
import { PastOrders } from "../Classes/PastOrders";
import { NavHistoryClass } from "../Classes/NavHistoryClass";
import AllSchemesClass from "../Classes/AllSchemesClass";
import { LatestNAVClass } from "../Classes/LatestNAV";
import PartnerClass from "../Classes/PartnelClass";
import AssetClass from "../Classes/AssetClass";
dotenv.config({ path: "../../.env" });

export const connect = async () => {
    if (database) {
        return;
    }

    Mongoose.connect(process.env.MONGO_URI!, {
    });
    database = Mongoose.connection;
    database.once("open", async () => {
        console.log("Connected to database");
    });
    database.on("error", () => {
        console.log("Error connecting to database");
    });
};

export const disconnect = () => {
    if (!database) {
        return;
    }
    Mongoose.disconnect();
};
connect();
export var UserModel = getModelForClass(User, { schemaOptions: { timestamps: true, collection: 'users' } });
export var PartnerModel = getModelForClass(PartnerClass, { schemaOptions: { timestamps: true, collection: 'partners' } });
export var BSEOrdersModel = getModelForClass(BSEOrder, { schemaOptions: { timestamps: true, collection: 'bseorders' } });
export var PastOrdersModel = getModelForClass(PastOrders, { schemaOptions: { timestamps: true, collection: 'pastorders' } });
export var AssetModel = getModelForClass(AssetClass, { schemaOptions: { timestamps: true, collection: 'assets' } });
export var LatestNAV = getModelForClass(LatestNAVClass, { schemaOptions: { timestamps: true, collection: 'currentnavs' } });
export var NavHistory = getModelForClass(NavHistoryClass, { schemaOptions: { timestamps: true, collection: 'navhistories', versionKey: false } });
export var AllSchemes = getModelForClass(AllSchemesClass, { schemaOptions: { timestamps: true, collection: 'allschemes' } });
